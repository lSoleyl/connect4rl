package arch2.ai

import arch2.data.experience._
import arch2.base._
import arch2.data._
import arch2.algorithm.QLearning
import arch2.base.predicates._
import arch2.config.RewardConfig

/** This class implements the AI for DCQLearingAI with a full state description (ExtendedPredicateState2)
 *  Therefore every column is bindable for this agent in every state.
 */
case class DynamicCQLearningAI2(val experience:CARCASSExperience with HeaderV1, l:AILoader) extends AbstractAgentAI(new QLearning(experience),l)(TypeMapping.simplePredicateState) with ExtendedPredicateState2  with PredicateStateUnifier[Int,Double] {
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone) = selectAction(state, 0, actions)
  
  override def newGame(config:Seq[Any]) {
    config.foreach(_ match {
      case r:RewardConfig => {
        //Set threshold for abstraction
        experience.setTDEThreshold((r.won - r.lost) * 10)
      }
      case _ =>
    })
    
    super.newGame(config)
  }
  
  val defaultVariable = Variable('?')
  val variableFactory = Variable.get _
  
  override val notifyExperience = experience.notifyExperience _
}

object DynamicCQLearningAI2 {
  val Loader = GenericAILoader("DCQLearningAI2", new CARCASSExperience with HeaderV1, DynamicCQLearningAI2.apply, GenericAILoader.CANNOT_PLAY_AGAINST_ITSELF)
}