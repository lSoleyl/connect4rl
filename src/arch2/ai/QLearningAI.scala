package arch2.ai

import arch2.base._
import arch2.data._
import arch2.algorithm.QLearning
import arch2.data.experience.StringExperience
import arch2.data.experience.HeaderV1
import arch2.data.experience.FileSerializable

case class QLearningAI(experience:StringExperience with HeaderV1, l:AILoader) extends AgentAI(new QLearning(experience), l) with StringState 


object QLearningAI {
  val Loader = GenericAILoader("QLearningAI", new StringExperience with HeaderV1, QLearningAI.apply)
}