package arch2.ai

import arch2.base._
import arch2.config._
import utils.Log
import arch2.data.AILoader

/** This class implements the player trait if an algorithm (Agent) is provided.
 *  This class calls all agent methods in the correct moment and initializes them properly.
 *  The class is marked abstract, because for most implementations S isn't FieldState and thus
 *  the Player trait isn't fully implemented.
 *  
 *  To instantiate this class additional mixins like 'StringState' or 'PredicateState' may be necessary.
 */
abstract class AgentAI[S,A,V](agent:Agent[S,A,V], l:AILoader) extends AbstractAgentAI(agent, l)() {  
  override def turn(state:S, possibleActions:List[A], color:Stone):A = agent.selectAction(state, 0, possibleActions)
}