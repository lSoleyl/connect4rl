package arch2.ai

import arch2.base._
import arch2.data._
import arch2.algorithm.MonteCarlo
import arch2.data.experience.HeaderV1
import arch2.data.experience.MCStringExperience

case class MCLearningAI(experience:MCStringExperience with HeaderV1, l:AILoader) extends AgentAI(new MonteCarlo(experience), l) with StringState


object MCLearningAI {
  val Loader = GenericAILoader("MCLearningAI", new MCStringExperience with HeaderV1, MCLearningAI.apply)
}