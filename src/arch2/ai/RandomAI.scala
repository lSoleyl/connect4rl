package arch2.ai

import arch2.field._
import arch2.base.Player
import scala.util.Random
import arch2.data.AILoader

/** This AI chooses a random action out of the possible actions
 */
class RandomAI(override val loader:AILoader) extends Player {
  override def turn(state:FieldState, actions:List[Int], color:Stone):Int = actions(Random.nextInt(actions.length))
}