package arch2.base.predicates

import utils.Options._

class Variable[T](val id:Char) {
  /** This method just checks whether a value could be bound to this variable.
   *  This should be useful for filtering bindable predicate candidates from the predicate states.
   *  
   * @param bindValue the value which should be bound
   * 
   * @return true if a bind(bindValue) would return true
   */
  def isBindableTo(bindValue:T):Boolean = {
    return !value || value.get == bindValue
  }
  
  /** This method tries to bind the variable to a given value.
   *  It returns true if the binding was successful.
   *  The binding is successful if:
   *   * the variable isn't bound
   *   or
   *   * the bound value is equal to the given binding value
   *   
   *   Every successful call to this method increases the bind count. 
   *   
   * @param bindValue the value to bind
   * 
   * @return true if binding was successful, false otherwise 
   */
  def bind(bindValue:T):Boolean = {
    if (!value) {
      value = Some(bindValue)
      bindCount = 1
      return true
    } else {
      if (bindValue == value.get) {
        bindCount += 1
        return true
      } else 
        return false
    }
  }
  
  
  /** This method should be called once every time, a successful 
   *  bind() was called to release the bound variable and make it bindable again.
   */
  def release() {
    if (bindCount > 0)
      bindCount -= 1
    
    if (bindCount == 0)
      value = None 
  }
  
  /** Wraps the variable into a BoundVariable iff the variable is bound to a value.
   *  
   * @returns The BoundVariable if this variable is bound, otherwise None
   */
  def bound:Option[BoundVariable[T]] = if (value) Some(BoundVariable(this, value.get)) else None
  
  override def equals(o:Any) = {
    if (!o.isInstanceOf[AnyRef])
      false
    else {
      o.asInstanceOf[AnyRef] eq this
    }
  }
  
  override def hashCode = id.hashCode  
  
  override def toString = id.toString 
    
  private var bindCount:Int = 0
  private var value:Option[T] = None
}



object Variable {  
  import scala.collection.mutable.Map
  val map:Map[Char, Variable[Int]] = Map()
  
  def get(i:Int):Variable[Int] = apply(i)
  
  def apply(i:Int):Variable[Int] = apply(('A' + i).asInstanceOf[Char])
  
  def apply(c:Char):Variable[Int] = {
    if (!map.contains(c)) {
      map(c) = new Variable[Int](c)
    }
    
    map(c)
  }
}