package arch2.base.predicates

import arch2.base.Agent
import utils.Log

/** This trait maps bound states to unbound states and bound variables to be able to use Q-Learning together with a
 *  predicate state description. 
 */
trait PredicateStateMatcher[A,V] extends Log {
  import utils.Options._
  
  implicit val variableFactory:Int => Variable[A]
  val defaultVariable:Variable[A]
  
  val agent:Agent[UnboundPredicateState[A],Variable[A],V]
  
  
  /** This method gets a raw predicate state, and matches the state against unbound states in the 
   *  experience's state list. (Assuming a PredicateListExperience)
   * 
   */
  def selectAction(state:BoundPredicateState[A], reward:Double, possibleActions:List[A]):A = {
    import scala.util.Random
    import utils.Tap._
    
    val (unboundState,boundVariables) = bindStateToVariables(state)
    val variables = boundVariables.map(_.variable) ::: List(defaultVariable)

    val variable = agent.selectAction(unboundState, reward, variables) //TODO would be nice, if QLearning would use optimized lookups for predicate lists
    
    val boundVariable = boundVariables.find(_.variable == variable)
    if (boundVariable)
      return boundVariable.get.value.tap(x => logTrace("Selected: " + x))
    else { //it was the random_action()-variable
      val boundActions = boundVariables.map(_.value)
      val randomActions = possibleActions diff boundActions
      if (!randomActions.isEmpty)
        return randomActions(Random.nextInt(randomActions.size)).tap(x => logTrace("Selected Randomly1: " + x))
      else
        return possibleActions(Random.nextInt(possibleActions.size)).tap(x => logTrace("Selected Randomly2: " + x))
    }
  }
  
  /** This method actually searches the expercience for the abstract state by calling extractFirstMatch() with _.matchWith().
   *  If a match is found, it returns the abstract state and the variable matching.
   *  
   *  If the concrete state can't be matched to any abstract state, then it will return a new abstract state, which is 
   *  derived from the passed concrete state. Before returning the new state, it will be entered into the experience.
   *  
   *  @param state the concrete state to look for
   *  
   *  @return A pair of the unbound prdedicate state and a list of variable bindings to 
   */
  private def bindStateToVariables(state:BoundPredicateState[A]):(UnboundPredicateState[A],List[BoundVariable[A]]) = {
    import utils.Tap._
    
    val eMatch = agent.experience.extractFirstMatch(_.matchWith(state))
    if (eMatch)
      return (eMatch.get.state,eMatch.get.matchResult)
    else
      UnboundPredicateState.deriveState(state).tap(s => agent.experience.enterState(s._1, s._2.map(_.variable)))
  }
}