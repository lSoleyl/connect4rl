package arch2.data.experience.base

import arch2.base.predicates._
import scala.annotation.tailrec
import utils.generic.MA._
import utils.Options._
import scala.collection.mutable.Map
import scala.collection.immutable.Queue
import arch2.data.Serializer

trait PredicateTreeExperience[A,V] extends Experience[UnboundPredicateState[A],Variable[A],V] {
  
  val defaultVariable:Variable[A] //Variable for random action '?'
  val defaultVariableStartValue:V
    
  protected var experience:TreeNode //Abstract, initial state must be defined by derived class
  
  /** Hook method to instantiate a new tree node. This can be overwritten to create nodes of derived classes.
   *  
   *  @param state the state, which the node should represent
   *  @param actionMap an action map (action->qvalue)
   *  @param children the node's child nodes (optional) 
   */
  protected def createNode(state:UnboundPredicateState[A], actionMap:Map[Variable[A],V], children:Queue[TreeNode] = Queue()):TreeNode = new TreeNode(state, actionMap, children)
  
  override def extractFirstMatch[X](matcher:State=>Option[X]):Option[ExtractorMatch[X]] = extractMatch(matcher, experience)
  
  /** This method first tries to match the current node and if this was successful, it will continue it's search
   *  at the first matching child. If no such child is found, the current node is the match result.
   *  If the current node didn't match, it returns None
   */
  private def extractMatch[X](matcher:State=>Option[X], node:TreeNode):Option[ExtractorMatch[X]] = {
    val matchRes = matcher(node.state)
    if (matchRes) {
      node.children.findFirst(extractMatch(matcher, _)) or Some(ExtractorMatch(matchRes.get, node.state)) 
    } else {
      None
    }
  } 
  
  
  //FIXME Accessing the tree structure, using a state is not optimal!
  override def bestActionEntry(state:State, allowedActions:List[Action], isBetter:(Value,Value)=>Boolean):Entry = {
    val entry = experience.find(state)
    
    if (!entry)
      return Entry(state, allowedActions.head, default)
      
    val (action,value) = entry.get.actionMap.reduce((av1,av2) => if (isBetter(av1._2,av2._2)) av1 else av2)
    Entry(state, action, value)
  }
  
  
  //FIXME Accessing the tree structure, using a state is not optimal!
  override def apply(key:SA):Value = experience.find(key.s).mapOr(default)(_.actionMap.getOrElse(key.a, default))
  
  //FIXME Accessing the tree structure, using a state is not optimal!
  override def containsState(state:State) = experience.find(state)
  
  //FIXME Accessing the tree structure, using a state is not optimal!
  override def update(key:SA, value:Value):Unit = {
    //This will throw an exception if the algorithm tries to update a not existing state
    experience.find(key.s).get.actionMap(key.a) = value
  }
  
  override def enterState(state:State, actions:List[Action]) = throw new Exception("Cannot simply enter a new state somewhere into the tree")
  
  //FIXME Accessing the tree structure, using a state is not optimal!
  /** Tries to insert a new state into the tree structure as child of "derivedFrom"
   *  If the state already exists, then this method just returns false
   *  
   *  @param state the state to insert
   *  @param derivedFrom the state it should be derived from
   */
  def enterState(state:State, derivedFrom:State):Boolean = {
    val parent = experience.find(derivedFrom).get
    if (parent.find(state)) { //state already exists
      false 
    } else { //enter new state
      val actionMap = state.getActionMap(default)
      actionMap(defaultVariable) = defaultVariableStartValue
    
      parent.children = parent.children.enqueue(createNode(state, actionMap)) //Append new node to children queue
      true
    }
  }
  
  class TreeNode(val state:UnboundPredicateState[A], val actionMap:Map[Variable[A],V], var children:Queue[TreeNode] = Queue()) {
    def find(s:UnboundPredicateState[A]):Option[TreeNode] = {
      if (state == s)
        Some(this)
      else
        children.findFirst(_.find(s))
    }
    
    def foreach(action:TreeNode => Unit) {
      action(this)
      children.foreach(child => child.foreach(action))
    }
    
    def foldLeft[T](zero:T)(op:(T,TreeNode) => T):T = {
      val head = op(zero,this)
      children.foldLeft(head)((acc:T, node:TreeNode) => node.foldLeft(acc)(op))
    }
    
    def serialize(implicit writer:utils.io.ByteWriter, ss:Serializer[UnboundPredicateState[A]], as:Serializer[List[(Variable[A],V)]]) {
      Serializer.serialize(state)
      Serializer.serialize(actionMap.toList)
    }
  }
  
  override def stateCount:Option[Int] = Some(experience.foldLeft(0)((acc,n) => acc+1))
}