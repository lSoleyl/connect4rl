package arch2.gui

import scala.swing._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import utils.Log
import scala.util.{Success, Failure}

/** This button provides a mechanism of coupling one button press to one function execution.
 *  The exectued function is passsed as parameter 'onClick' and will be called inside a 
 *  Future to free GUI thread.
 *  The Button disables himself before the function starts and disables himself after the function
 *  has finished.
 */
class ActionButton(label:String, onClick: =>Unit, toolTip:String = null) extends Button with Log {
  action = new Action(label) {
    def apply = {
      enabled = false
      
      Future {
        onClick
      } onComplete {
        case Success(_) => Swing.onEDT { enabled = true }        
        case Failure(ex) => {          
          log.error(s"Actionbutton: $label caught an exception during action execution", ex)
          Swing.onEDT { enabled = true }
        }
      }
    }
  }
  text = label
  tooltip = toolTip
}