package utils

import org.apache.log4j.Logger

import Convert._

/** A small helper trait for logging.
 *  After mixing this trait into your class you can simply call log.debug, log.trace, ...
 *  Companion objects and classes share the same logger for readability
 */
trait Log {
  val log = Logger.getLogger(this.getClass().getName().split('$')(0))
  log.trace("Created logger")
  
  /** Since string interpolation is expensive and traces are deactivated
   *  most of the time, this method takes the parameter by name to prevent 
   *  premature evaluation
   * 
   * @param msg the message to print
   */
  def logTrace(msg: =>String) {
    if(log.isTraceEnabled())
      log.trace(msg)
  }
  
  /** Since string interpolation is expensive and debugs are deactivated
   *  most of the time, this method takes the parameter by name to prevent 
   *  premature evaluation
   * 
   * @param msg the message to print
   */
  def logDebug(msg: =>String) {
    if(log.isDebugEnabled())
      log.debug(msg)
  }
}