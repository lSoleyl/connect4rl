package arch2.data.experience

import java.io._
import utils.Log
import arch2.data.ProgressInfo
import base.MapExperience
import utils.io.ByteWriter
import utils.io.ByteReader

/** This class represents string based experience where states are represented by strings 
 *  and are mapped with their int actions onto the Q values.
 *  
 *  (string,int) -> double
 *  
 *  This is the old serialization format which uses object streams to load and write experience.
 *  The data starts with an int, which represents the amount of entries in the map
 *  it is then followed by the flattened map data.
 *  eg.
 *  [count:Int][state1:String][action1:Int][q1:Double][state2:String][action2:Int][q2:Double]...
 */
class StringExperience extends StreamSerializable with MapExperience[String,Int,Double] with Log {
    override val default = 0d
  
    /** This method loads the experience map from the given input stream
     *  according to the old serialization method. Like this the old learn data should
     *  stay compatible even though the architecture changed.
     */
    override def loadFromStream(in:InputStream, progress:ProgressInfo) {
      experience.clear
      val reader = new ByteReader(in)
      
      //An int for compability reasons... and if we had the necessity to use a long, then our file would be many gigabytes big.
      //Additionally Map.size only returns an int
      val size = reader.readInt
      if (size < 0) {
        log.error("Size seems to have an overflow")
        throw new Exception("Size seems to have an overflow")
      }
      
      var i = 0
      progress.setTotal(size)
      
      log.info(s"Reading $size entries")
      while(i < size) {
        val state = reader.readString
        val action = reader.readInt
        val value = reader.readDouble
        
        experience(SA(state,action)) = value
        progress.next
        i += 1
      }
    }
    
    /** Writes the current experience from the experience map into the output stream.
     *  This output is compatible to the first serialization.
     */
    override def writeToStream(out:OutputStream, progress:ProgressInfo) {
      val writer = new ByteWriter(out)
      val entries = experience.size
      progress.setTotal(entries)
      writer.writeInt(entries)
      log.info(s"Writing $entries entries")      
      
      for((sa, value) <- experience) {
        writer.writeString(sa.s) //is being written as object so that we don't have to handle the string length
        writer.writeInt(sa.a)
        writer.writeDouble(value)
        progress.next
      }
    }

    override def stateCount:Option[Int] = Some(experience.keySet.map(_.s).toSet.size)   
}