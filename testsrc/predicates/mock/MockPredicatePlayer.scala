package predicates.mock

import arch2.base._
import arch2.base.predicates._
import arch2.field.Stone

class MockPredicatePlayer extends Player with PredicateState {
  case class State(state:BoundPredicateState[Int], actions:List[Int], color:Stone)
  
  var lastState:State = null
  
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone):Int = {
    lastState = State(state,actions,color)
    actions.head //Simply return first action
  }
  
  override val loader = null
}