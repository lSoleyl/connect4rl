package arch2.base.predicates

import arch2.base.Order

trait Predicate1[T] {
  def argument:T
  
  val id:String //Name of predicate without it's arguments 
  
  override def toString = s"$id($argument)"
  
  /** Create a new Predicate with identical properties, but with
   *  other argument.
   *  
   *  @param arg the new argument
   */
  def replaceArgument[A](arg:A)(implicit order:Order[A]):Predicate1[A]
}