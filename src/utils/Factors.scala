package utils

object Factors {
  implicit class UnitInt(i:Int) {
    def M = i*1000000
    def k = i*1000
    
    def short:String = {
      val (number,factor) = tuple
      "" + number + factor
    }
    
    def tuple:(Int,Factor) = {
      if (i >= (1 M))
        (i/(1 M), Factor(1 M, "M"))
      else if (i >= (1 k))
        (i/(1 k), Factor(1 k, "k"))
      else 
        (i, Factor(1, ""))
    }
  }

  implicit class UnitLong(i:Long) {
    def M = i*1000000
    def k = i*1000
    
    def short:String = {
      if (i >= (1 M)) {
        "" + (i / (1 M)) + "M"
      } else if (i >= (1 k)) {
        "" + (i / (1 k)) + "k"
      } else 
        i.toString
    }
  }
  
  def shortFormatToLong(fmt:String):Long = {
    if (fmt.endsWith("M"))
      Integer.parseInt(fmt.dropRight(1)).asInstanceOf[Long] M
    else if (fmt.endsWith("k"))
      Integer.parseInt(fmt.dropRight(1)).asInstanceOf[Long] k
    else
      Integer.parseInt(fmt).asInstanceOf[Long]
  }
  
  def shortFormatToInt(fmt:String):Int = {
     if (fmt.endsWith("M"))
      Integer.parseInt(fmt.dropRight(1)) M
    else if (fmt.endsWith("k"))
      Integer.parseInt(fmt.dropRight(1)) k
    else
      Integer.parseInt(fmt)
  }
  
  val FACTOR_LIST = List(Factor(1, ""), Factor(1 k, "k"), Factor(1 M, "M"))
  val FACTOR_MAP = FACTOR_LIST.map(factor => factor.stringRepresentation -> factor).toMap
  
  case class Factor(numericValue:Int, stringRepresentation:String) {
    override def toString = stringRepresentation 
  }
}
