package arch2.ai

import arch2.base._
import arch2.config._
import utils.Log
import arch2.data.AILoader

/** An abstract base class which has been put on top of AgentAI to also support an Agent which learns
 *  States which have a different structure than the States, the environment passes to him.
 */
abstract class AbstractAgentAI[CS,CA,AS,AA,V](val agent:Agent[AS,AA,V], override val loader:AILoader)(t:TypeMapping[CS,CA,AS,AA,V] = TypeMapping.identity(agent)) extends Player with Log {
  type Stone = arch2.field.Stone
  var rewards:RewardConfig = null
  
  def turn(state:CS, possibleActions:List[CA], color:Stone):CA
  
  override def newGame(config:Seq[Any]) {
    config.foreach(_ match {
      case params:LearningParameters => agent.initialize(params)
      case r:RewardConfig => rewards = r
      case _ =>
    })
    
    if (rewards == null)
      log.warn("Agent can't learn without reward values! Please provide a RewardConfig.")
  }
  
  /** Hook method to react onto enemy rewards 
   */
  def enemyFinalReward(reward:Double) {}
  
  override def won  { agent.finalReward(rewards.won); enemyFinalReward(rewards.lost) }
  override def lost { agent.finalReward(rewards.lost); enemyFinalReward(rewards.won) }
  override def tied { agent.finalReward(rewards.tied); enemyFinalReward(rewards.tied) }
}