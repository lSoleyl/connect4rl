package predicates

import org.scalatest._
import arch2.base.predicates._
import utils.Options._
import data.Predicates
import data.Variables
import helper.Binding

class PredicateMatching extends FlatSpec with Matchers {
  import Variables._
  import Predicates._
  
  //Define some abstract states
  object ub {
    val EMPTY_STATE = new UnboundPredicateState[Int](Nil)
    val ONLY_A = new UnboundPredicateState(A(X) :: Nil)
    val ABC_WITH_X = new UnboundPredicateState(A(X) :: B(X) :: C(X) :: Nil)
    val ABC_WITH_XYZ = new UnboundPredicateState(A(X) :: B(Y) :: C(Z) :: Nil)
    val ABC_WITH_XYX = new UnboundPredicateState(A(X) :: B(Y) :: C(X) :: Nil)
    val AAB_WITH_XYX = new UnboundPredicateState(A(X) :: A(Y) :: B(X) :: Nil)
  }
  //Define some concrete states
  object b {
    val EMPTY_STATE = Nil
    val ONLY_A = A(3) :: Nil
    val ONLY_C = C(1) :: Nil
    
    val ABC_555 = A(5) :: B(5) :: C(5) :: Nil
    val ABC_123 = A(1) :: B(2) :: C(3) :: Nil
    val ABC_321 = A(3) :: B(2) :: C(1) :: Nil
    val ABC_323 = A(3) :: B(2) :: C(3) :: Nil
    
    val AAB_777 = A(7) :: A(7) :: B(7) :: Nil
    val AAB_767 = A(7) :: A(6) :: B(7) :: Nil
    val AAB_677 = A(6) :: A(7) :: B(7) :: Nil //This state enforces backtracking
  }
  
  
  //Precondition for most tests
  "All variables" should "be in an unbound state" in {
    List(X,Y,Z).foreach(_.bound should be (None))
  }
  
  
  "An empty state" should "only match empty state" in {
    import ub._
    EMPTY_STATE.matchWith(b.EMPTY_STATE) should be (Some(Nil))
    EMPTY_STATE.matchWith(b.ONLY_A) should be (None)
    EMPTY_STATE.matchWith(b.ONLY_C) should be (None)
    EMPTY_STATE.matchWith(b.ABC_555) should be (None)
    EMPTY_STATE.matchWith(b.ABC_123) should be (None)
  }
  
  
  "A(X)" should "match A(3)" in {
    Binding(ub.ONLY_A.matchWith(b.ONLY_A)).possible should be (true)    
  }
  
  it should "return a list with one bound variable" in {
    Binding(ub.ONLY_A.matchWith(b.ONLY_A)).length should be (1)
  }
  
  it should "assign X = 3" in {
    val bvar = Binding(ub.ONLY_A.matchWith(b.ONLY_A)).boundVars.head
    
    bvar.variable.id should be ('X')
    bvar.variable should be (X)
    bvar.value should be (3)
  }
  
  it should "have a bindable variable after matching" in {
    ub.ONLY_A.matchWith(b.ONLY_A)
    ub.ONLY_A.predicates.head.argument.isBindableTo(13) should be (true)
    ub.ONLY_A.predicates.head.argument.bound should be (None)
  }
  
  it should "not match to C(1)" in {
    ub.ONLY_A.matchWith(b.ONLY_C) should be (None)
  }
  
  it should "not match to empty state" in {
    ub.ONLY_A.matchWith(b.EMPTY_STATE) should be (None)
  }
  
  it should "not match with other states" in {
    ub.ONLY_A.matchWith(b.ABC_323) should be (None)
    ub.ONLY_A.matchWith(b.ABC_555) should be (None)
    ub.ONLY_A.matchWith(b.ABC_123) should be (None)
  }
  
  "matching A(X) B(X) C(X) with A(5) B(5) C(5)" should "be possible" in {
    Binding(ub.ABC_WITH_X.matchWith(b.ABC_555)).possible should be (true)
  }
  
  it should "bind exactly one variable" in {
    Binding(ub.ABC_WITH_X.matchWith(b.ABC_555)).length should be (1)
  }
  
  it should "bind X = 5" in {
    val bnd = Binding(ub.ABC_WITH_X.matchWith(b.ABC_555))
    
    bnd.boundVars.head should be (BoundVariable(X,5))
    bnd(X) should be (Some(5))
  }
  
  "A(X) B(X) C(X)" should "not match with A(1) B(2) C(3)" in {
    ub.ABC_WITH_X.matchWith(b.ABC_123) should be (None)
  }
  
  it should "not match with A(7) A(7) B(7)" in {
    ub.ABC_WITH_X.matchWith(b.AAB_777) should be (None)
  }
  
  it should "not match with anything else" in {
    ub.ABC_WITH_X.matchWith(b.EMPTY_STATE) should be (None)
    ub.ABC_WITH_X.matchWith(b.ABC_321) should be (None)
    ub.ABC_WITH_X.matchWith(b.ONLY_A) should be (None)
    ub.ABC_WITH_X.matchWith(b.AAB_677) should be (None)
  }
  
  "matching A(X) B(Y) C(Z) with A(5) B(5) C(5)" should "be possible" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_555)).possible should be (true)
  } 
  
  it should "bind exactly three variables" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_555)).length should be (3)
  }
  
  it should "bind X = Y = Z = 5" in {
    val binding = Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_555))
    binding(X) should be (Some(5))
    binding(Y) should be (Some(5))
    binding(Z) should be (Some(5))
  }
  
  it should "leave no variables in a bound state" in {
    ub.ABC_WITH_XYZ.matchWith(b.ABC_555)
    ub.ABC_WITH_XYZ.variables.foreach(_.bound should be (None))
  }
  
  
  "matching A(X) B(Y) C(Z) with A(1) B(2) C(3)" should "be possible" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_123)).possible should be (true)
  }
  
  it should "bind exactly three variables" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_123)).length should be (3)
  }
  
  it should "bind X = 1, Y = 2, Z = 3" in {
    val binding = Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_123))
    
    binding(X) should be (Some(1))
    binding(Y) should be (Some(2))
    binding(Z) should be (Some(3))
  }
  
  
  "matching A(X) B(Y) C(Z) with A(3) B(2) C(1)" should "be possible" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_321)).possible should be (true)
  }
  
  it should "bind exactly three variables" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_321)).length should be (3)
  }
  
  it should "bind X = 3, Y = 2, Z = 1" in {
    val binding = Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_321))
    
    binding(X) should be (Some(3))
    binding(Y) should be (Some(2))
    binding(Z) should be (Some(1))
  }
  
  
  "matching A(X) B(Y) C(Z) with A(3) B(2) C(3)" should "be possible" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_323)).possible should be (true)
  }
  
  it should "bind exactly three variables" in {
    Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_323)).length should be (3)
  }
  
  it should "bind X = 3, Y = 2, Z = 3" in {
    val binding = Binding(ub.ABC_WITH_XYZ.matchWith(b.ABC_323))
    
    binding(X) should be (Some(3))
    binding(Y) should be (Some(2))
    binding(Z) should be (Some(3))
  }
  
  "A(X) B(Y) C(Z)" should "not match with empty state" in {
    ub.ABC_WITH_XYZ.matchWith(b.EMPTY_STATE) should be (None)
  }
  
  it should "not match with any A(?) A(?) B(?) state" in {
    ub.ABC_WITH_XYZ.matchWith(b.AAB_677) should be (None)
    ub.ABC_WITH_XYZ.matchWith(b.AAB_767) should be (None)
    ub.ABC_WITH_XYZ.matchWith(b.AAB_777) should be (None) 
  }
  
  it should "not match with A(3) or C(1)" in {
    ub.ABC_WITH_XYZ.matchWith(b.ONLY_A) should be (None)
    ub.ABC_WITH_XYZ.matchWith(b.ONLY_C) should be (None)
  }
  
  
  "matching A(X) B(Y) C(X) with A(5) B(5) C(5)" should "be possible" in {
    Binding(ub.ABC_WITH_XYX.matchWith(b.ABC_555)).possible should be (true)
  }
  
  it should "bind exactly two variables" in {
    Binding(ub.ABC_WITH_XYX.matchWith(b.ABC_555)).length should be (2)
  }
  
  it should "bind X = Y = 5" in {
    val binding = Binding(ub.ABC_WITH_XYX.matchWith(b.ABC_555))
    
    binding(X) should be (Some(5))
    binding(Y) should be (Some(5))
  }
  
  "matching A(X) B(Y) C(X) with A(3) B(2) C(3)" should "be possible" in {
    Binding(ub.ABC_WITH_XYX.matchWith(b.ABC_323)).possible should be (true)
  }
  
  it should "bind exactly two variables" in {
    Binding(ub.ABC_WITH_XYX.matchWith(b.ABC_323)).length should be (2)
  }
  
  it should "bind X = 3, Y = 2" in {
    val binding = Binding(ub.ABC_WITH_XYX.matchWith(b.ABC_323))
    
    binding(X) should be (Some(3))
    binding(Y) should be (Some(2))
  }
  
  
  "A(X) B(Y) C(X)" should "not match with anything else" in {
    ub.ABC_WITH_XYX.matchWith(b.ABC_123) should be (None)
    ub.ABC_WITH_XYX.matchWith(b.ABC_321) should be (None)
    ub.ABC_WITH_XYX.matchWith(b.AAB_677) should be (None)
    ub.ABC_WITH_XYX.matchWith(b.AAB_767) should be (None)
    ub.ABC_WITH_XYX.matchWith(b.AAB_777) should be (None)
  }
  
  //Here Backtracking will be required to correctly match this, since X will be bound to 6 at first
  "matching A(X) A(Y) B(X) with A(6) A(7) B(7)" should "be possible" in {
    Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_677)).possible should be (true)
  }
  
  it should "bind exactly two variables" in {
    Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_677)).length should be (2)
  }
  
  it should "bind X = 7, Y = 6" in {
    val binding = Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_677))
    
    binding(X) should be (Some(7))
    binding(Y) should be (Some(6))
  }
  
  "matching A(X) A(Y) B(X) with A(7) A(6) B(7)" should "be possible" in {
    Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_767)).possible should be (true)
  } 
  
  it should "bind exactly two variables" in {
    Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_767)).length should be (2)
  }
  
  it should "bind X = 7, Y = 6" in {
    val binding = Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_767))
    
    binding(X) should be (Some(7))
    binding(Y) should be (Some(6))
  }
  
  "matching A(X) A(Y) B(X) with A(7) A(7) B(7)" should "be possible" in {
    Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_777)).possible should be (true)
  }
  
  
  it should "bind exactly two variables" in {
    Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_777)).length should be (2)
  }
  
  it should "bind X = Y = 7" in {
    val binding = Binding(ub.AAB_WITH_XYX.matchWith(b.AAB_777))
    
    binding(X) should be (Some(7))
    binding(Y) should be (Some(7))
  }
  
  
  
  //Last test to ensure valid global state for next tests
  "All used variables" should "be in an unbound state" in {
    List(X,Y,Z).foreach(_.bound should be (None))
  } 
}