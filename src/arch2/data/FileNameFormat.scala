package arch2.data

import utils.Log
import utils.Options._
import java.io._
import utils.Factors._
import experience.HeaderV1
import experience.StreamSerializable
import experience.FileSerializable

object FileNameFormat {
  val FILE_EXTENSION = ".exp"
  
  def unapply(fName:String):Option[(String, Option[String])] = {
    import utils.Factors._ 
    
    if (!fName.endsWith(FILE_EXTENSION))
      return None
      
    val fNameShort = new File(fName).getName()
    
    val parts = fNameShort.dropRight(4).split("-")
    if (parts.size < 2 || parts.size > 3)
      return None
    
    val Number = "^([0-9]+[Mk]?)$".r
    parts(1) match {
      case Number(_) => Some((parts(0), Some(fNameShort.dropRight(4).replace('-', ' '))))
      case _ => None      
    }
  }
}


object FileFormat2 extends Log {
  val FILE_EXTENSION = ".exp2"
    
  /** This method can be used to check whether the given file matches the new file format
   *  Then the agent's description and loadertype is encoded inside the file's header.
   *  
   *  @param fName the filename to match
   * 
   *  @return either (type, description) or None if it doesn't match
   */
  def unapply(fName:String):Option[(String, Option[String])] = {
    
    if (!fName.endsWith(FILE_EXTENSION))
      return None
    
    log.info(s"Trying to read header of $fName to determine agenttype and description")
    val headerReader = new HeaderReader with HeaderV1 with FileSerializable
    
    headerReader.loadFromFile(new File(fName), null)
    
    if (!headerReader.agentType) {
      log.error("Invalid Fileformat2 because of missing agentType")
      None
    } else {
      log.info(s"Extracted: (${headerReader.agentType.get},${headerReader.episodes},${headerReader.agentdescription}) from $fName")
      Some((headerReader.agentType.get, headerReader.agentdescription.map(_ + " (" + headerReader.episodes.short + ")")))
    }    
  }
  
  
  class HeaderReader extends StreamSerializable {
    override def loadFromStream(in:InputStream, progress:ProgressInfo) {}
    override def writeToStream(out:OutputStream, progress:ProgressInfo) {}
  } 
}