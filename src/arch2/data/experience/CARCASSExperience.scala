package arch2.data.experience

import arch2.base.predicates._
import arch2.data.experience.base.PredicateListExperience
import arch2.data.ProgressInfo
import scala.collection.mutable.{Map,Set}
import utils.io._
import utils.Times._
import scala.swing.Dialog

class CARCASSExperience(hasExpertKnowledge:Boolean = true) extends PredicateListExperience[Int,Double] with DynamicStateAbstraction with StreamSerializable {
  val default = 0d
  val defaultVariable = Variable('?')
  val defaultVariableStartValue = -1d
  type A = Int    //Maybe we could parameterize this type, but it would make everything a bit more complicated
                               //v- MA.partMap() doesn't work anymore if we use BoundPredicateState[A] here
  
  //This variable holds the partially entered trace entry which gets completed on the next update
  private var nextTraceEntry:Double=>TEntry = null
  
  
  if (experience.isEmpty) {
    //Add default state at bottom of experience: true -> ?
    enterState(new UnboundPredicateState(Nil))
    
    if (hasExpertKnowledge) {
      // Expert-Knowledge (first two states)
      enterState(new UnboundPredicateState(List(BlockPredicate(4, Variable('X')))))
      enterState(new UnboundPredicateState(List(ChainingPredicate(4, Variable('X')))))
    }
  }
  
  def setTDEThreshold(threshold:Double) {
    tdeThreshold = threshold
  }
  
  /** Inserts a new state into the experience before a given state
   *  
   *  @param state the state to enter into the experience
   *  @param before the state to insert the new state before (must exist in the experience!)
   */
  def insertStateBefore(state:ListEntry, before:State) {
    //Recursive helper function for fast list insertion
    def insertHelper(s:ListEntry, before:State, list:List[ListEntry]):List[ListEntry] = {
      if (before == list.head.state) {
        s :: list
      } else {
        list.head :: insertHelper(s, before, list.tail)
      }
    }
    
    experience = insertHelper(state, before, experience)
  }
  
  /** This method should get called by PredicateStateUnifier in selectAction()
   *  to notify the experience about the used abstraction.
   *  This information is needed to derive new abstract states.
   *  
   *  @param cs the concrete state
   *  @param as the derived abstract state
   *  @param aa the chosen abstract action
   *  @param ca the concrete action
   */
  def notifyExperience(cs:BoundPredicateState[A], as:UnboundPredicateState[A], aa:Variable[A], ca:A) {
    nextTraceEntry = TraceEntry(cs,as,aa,ca)
  }
  
  
  /** This class overrides the update method to insert the logging of trace entries.
   *  To do this, it needs the current Q-Value of the State,Action pair to calculate the
   *  TDE, which will entered into the trace entry.
   *  
   *  @throws MissingStateException if the state doesn't exist in the experience
   */
  override def update(key:SA, value:Value):Unit = {
    val entry = experience.find(_.state == key.s)
    if (entry.isEmpty)
      throw new MissingStateException(key.s)
    
    val actionMap = entry.get.actionMap
    val tde = value - actionMap(key.a)  //calculate TDE*alpha
    actionMap(key.a) = value            //update Q value

    //nextTraceEntry can't be zero if a value gets an update, since QLearning doesn't call update on the first state    
    addTrace(nextTraceEntry(tde)) //This might cause a new state to be derived
  }
  
  /** Interface method from DynamicStateAbstraction, which is needed to implement the state abstraction
   */
  override def insertState(newState:AS, derivedFrom:AS):Boolean = {
    if (containsState(newState)) {
      false
    } else {
      insertStateBefore(makeEntry(newState), derivedFrom)
      true
    } 
  }
  
  
  
  override def customAction {
    import utils.Percentage._
    println("\nStatecount: " + experience.length)
    println("States: \n" + experience.map(_.state).mkString(" - ", "\n - ", "\n"))
    println("Deleting temporary data to reduce size of experience data (traces and tdeMap)")
    
    clearTempData
  }
  
 
  override def writeToStream(out:OutputStream, progress:ProgressInfo) {
    implicit val writer = new ByteWriter(out)
    import writer._
    import arch2.data.Serializer._
    
    
    val length = experience.length
    val tempData = tempDataSize
    import tempData._
    log.info(s"Writing $length states")
    writeInt(length)
    writeInt(traceLength)
    writeInt(tdeMapSize)
    
    progress.setTotal(length + traceLength + tdeMapSize)
    for(entry <- experience) {
      serialize(entry.state)      
      serialize(entry.actionMap.toList)
      
      progress.next
    }   
    
    log.info(s"Writing $traceLength trace entries and $tdeMapSize tdeMap entries")
    tempToStream(progress)
    
    log.info("done.")
  }
  
  override def loadFromStream(in:InputStream, progress:ProgressInfo) { 
    log.info(s"CARCASSExperience.loadFromStream")
    implicit val reader = new ByteReader(in)
    import reader._
    import arch2.data.Serializer._
    
    
    val length = readInt
    val traceLength = readInt
    val mapSize = readInt
    log.info(s"Loading $length states")
    
    progress.setTotal(length+traceLength+mapSize)
    experience = (0 until length).map(i => {
      val state = load[UnboundPredicateState[Int]]
      val actionMap = Map(load[List[(Variable[Int],Double)]]: _*)
      
      progress.next
      ListEntry(state, actionMap)
    }).toList
    
    log.info(s"Loading $traceLength traces and $mapSize tdeMap entries")
    tempFromStream(traceLength, mapSize, progress)
  }
  
  override def stateCount:Option[Int] = Some(experience.length)
}

