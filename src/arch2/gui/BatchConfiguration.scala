package arch2.gui

import utils.Log
import utils.Tap._
import utils.JSON
import utils.Options._
import spray.json._
import scala.util.{Try,Failure, Success}

object BatchConfiguration extends Log {
  sealed abstract class Step(name:String) { 
    override def toString = name 
    /** This method will be called to get an execution plan.
     *  The Step type should correctly calculate the episode values for the single batch runs
     *  
     *  @param initialStep the initial step size
     *  @param totalEpisodes the amount of episodes which should be simulated
     *  
     *  @return A list of episode counts for the batch run and an Int which returns how many
     *          episodes are actually simulated (might be less than totalEpisodes, but never more)
     */
    def getEpisodeList(initialStep:Int, totalEpisodes:Int):(List[Int],Int)  
  }
  
  object Step {
    object LINEAR extends Step("linear") {
      def getEpisodeList(initialStep:Int, totalEpisodes:Int):(List[Int],Int) = {
        if (initialStep == 0) //Prevent division by 0
          (Nil,0)
        else {
          val batchRuns = totalEpisodes / initialStep //default round down
          (List.fill(batchRuns)(initialStep), batchRuns*initialStep)      
        }
      }
    }
    
    object LOG10 extends Step("log10") {
      def getEpisodeList(initialStep:Int, totalEpisodes:Int):(List[Int],Int) = {
        if (initialStep == 0) //The whole calculation makes no sense for initialStep = 0
          return (Nil,0)
          
        var stepSize = Math.pow(10, Math.log10(initialStep).floor).toInt
        var episodes = 0
        var steps:List[Int] = Nil
        
        if (stepSize <= totalEpisodes) { //Special treatment for first entry
          steps = stepSize :: steps
          episodes = stepSize
        } 
        
        while(episodes + stepSize <= totalEpisodes) {
          steps = stepSize :: steps
          episodes += stepSize
          
          if ((steps.length-1) % 9 == 0)
            stepSize *= 10 //increment step size every 10 batch runs by factor 10
        }
        
        (steps.reverse, episodes)
      }
    }
  }
  
  val STEPS = Map(
      "linear" -> Step.LINEAR,
      "log10"  -> Step.LOG10)
  
  
  val CONFIG_FILE = "batch_configuration.json" 
  
  //Returns the default configuration
  def defaultConfig = BatchConfiguration(MultiBatchConfiguration(), SimpleBatchConfiguration(), false)
 
  def load:BatchConfiguration = {
    JSON.load(CONFIG_FILE) match {
      //File not found
      case None => {
        log.info("No " + CONFIG_FILE + " found, using default batch config")
        defaultConfig
      }
      
      //File found
      case Some(json) => fromJSON(json) match {
          case Success(config) => config
          case Failure(ex) => { 
            log.error("Loading batch configuration failed! Using default configuraion", ex)
            defaultConfig
          }
        }
    }
  }
  
  def save(config:BatchConfiguration) {    
    Try {
      JSON.save(config.toJSON, CONFIG_FILE)
    } match {
      case Failure(ex) => log.error("Failed to write batch config file. No write access to ./" + CONFIG_FILE, ex)
      case Success(_) => log.info("Successfully saved batch configuration") 
    }
  }
  
  def fromJSON(json:JsValue):Try[BatchConfiguration] = {
    Try {
      val jsObj = json.asJsObject
      
      val simple = SimpleBatchConfiguration.fromJSON(jsObj.fields("simple"))
      val multi = MultiBatchConfiguration.fromJSON(jsObj.fields("multi"))
      val useMulti = jsObj.fields.get("useMultiBatch").as[JsBoolean].mapOr(false)(_.value)
      
      BatchConfiguration(multi, simple, useMulti)      
    }
  } 
  
  import arch2.data.Serializer
  implicit object ConfigSerializer extends Serializer[BatchConfiguration] {
    override def write(c:Type)(implicit writer:ByteWriter) {
      Serializer.serialize(c.multi)
      Serializer.serialize(c.simple)
      writer.writeBool(c.useMultiBatch)
    }
    
    override def load()(implicit reader:ByteReader) = {
      import reader._
      BatchConfiguration(Serializer.load[MultiBatchConfiguration], Serializer.load[SimpleBatchConfiguration], readBool)
    }
  }
}


case class BatchConfiguration(multi:MultiBatchConfiguration, simple:SimpleBatchConfiguration, var useMultiBatch:Boolean) {
  def toJSON:JsValue = JsObject(Map("multi" -> multi.toJSON, "simple" -> simple.toJSON, "useMultiBatch" -> JsBoolean(useMultiBatch)))
}

case class MultiBatchConfiguration(var outputDir:String = ".", var stepType:BatchConfiguration.Step = BatchConfiguration.Step.LOG10, var initialStep:Int = 100, var totalEpisodes:Int = 1000) {
  def toJSON:JsValue = JsObject(Map(
    "outputDir" -> JsString(outputDir),
    "initialStep" -> JsNumber(initialStep),
    "totalEpisodes" -> JsNumber(totalEpisodes),
    "stepType" -> JsString(stepType.toString)
  ))
}

object MultiBatchConfiguration {
  def fromJSON(json:JsValue) = {
    val config = MultiBatchConfiguration()
    val obj = json.asJsObject
    val member = (x:String) => obj.fields.get(x)
    
    member("outputDir").as[JsString].map(_.value).iff { config.outputDir = _ }
    member("initialStep").as[JsNumber].map(_.value.toInt).iff { config.initialStep = _ }
    member("totalEpisodes").as[JsNumber].map(_.value.toInt).iff { config.totalEpisodes = _ }
    member("stepType").as[JsString].map(_.value).flatMap(BatchConfiguration.STEPS.get(_)).iff { config.stepType = _ }
    
    config
  }
  
  import arch2.data.Serializer
  implicit val MultiSerializer:Serializer[MultiBatchConfiguration] = new Serializer[MultiBatchConfiguration] {
    override def write(cfg:Type)(implicit writer:ByteWriter) {
      import writer._
      import cfg._
      
      writeString(outputDir)
      writeString(stepType.toString)
      writeInt(initialStep)
      writeInt(totalEpisodes)
    }
    
    override def load()(implicit reader:ByteReader) = {
      import reader._
      readString //<-- Ignore the actual outputDir and use the local one (This makes the .resume files usable on other machines)
      MultiBatchConfiguration(GameGUI.batchConfig.multi.outputDir, BatchConfiguration.STEPS(readString), readInt, readInt)
    }
  }
}



case class SimpleBatchConfiguration(var episodes:Int = 1000) {
  def toJSON:JsValue = JsObject(Map("episodes" -> JsNumber(episodes)))
}

object SimpleBatchConfiguration {
  def fromJSON(json:JsValue) = {
    val config = SimpleBatchConfiguration()
    val obj = json.asJsObject
    val member = (x:String) => obj.fields.get(x)
    
    member("episodes").as[JsNumber].map(_.value.toInt).iff { config.episodes = _ }
    
    config
  }
  
  import arch2.data.Serializer
  implicit val SimpleSerializer:Serializer[SimpleBatchConfiguration] = new Serializer[SimpleBatchConfiguration] {
    override def write(cfg:Type)(implicit writer:ByteWriter) {
      writer.writeInt(cfg.episodes)
    } 
    
    override def load()(implicit reader:ByteReader) = SimpleBatchConfiguration(reader.readInt)
  }
}
