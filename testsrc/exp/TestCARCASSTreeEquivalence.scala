package exp

import org.scalatest._
import arch2.data.experience._
import arch2.base.predicates.PredicateStateUnifier
import arch2.algorithm.QLearning
import arch2.data.experience.base.Experience
import arch2.base.predicates._
import arch2.math.IncrementingGenerator
import arch2.math.RandomGenerator
import arch2.config.LearningParameters

abstract class MockPlayer(exp:Experience[UnboundPredicateState[Int],Variable[Int],Double] with HeaderV1) extends PredicateStateUnifier[Int,Double] {
  val agent = new QLearning(exp)
  val defaultVariable:Variable[Int] = Variable('?')
  val variableFactory = Variable.get _
  
  override val random:RandomGenerator = new IncrementingGenerator
}

object StateHelper {
  /** This method expects a state description in the form "2k(3) 3b(1) 2k(7)" and will 
   *  generate the state description from that.
   */
  def convertState(stateDesc:String):BoundPredicateState[Int] = {
    stateDesc.split(" ").toList.map(predStr => {
      val chainLength = predStr.substring(0,1).toInt
      val column = predStr.substring(3,4).toInt
      if (predStr.substring(1,2) == "k")
        ChainingPredicate(chainLength, column)
      else
        BlockPredicate(chainLength, column)      
    })
  }
}

/** This test should ensure that CARCASSExperience and CARCASSTreeExperience are both 
 *  equivalent in terms of decision making and deriving new states.
 *  To test this, two mock agents with no exploration and deterministic RandomGenerator are presented with
 *  the same sequence of states and have to make the same decisions and derive new states after the same number of steps
 */
class TestCARCASSTreeEquivalence extends FlatSpec with Matchers {
  val exp1 = new CARCASSExperience(true) with HeaderV1
  val exp2 = new CARCASSTreeExperience(true) with HeaderV1
  
  exp1.setTDEThreshold(100)
  exp2.setTDEThreshold(100)
  val params = LearningParameters(0.5, 0.8, 0.0, 0.0, false, 0) //with alpha = 0 the agent won't derive new states
  
  val cplayer = new MockPlayer(exp1) { val notifyExperience = exp1.notifyExperience _ }
  val tplayer = new MockPlayer(exp2) { val notifyExperience = exp2.notifyExperience _ }
  
  cplayer.agent.initialize(params)
  tplayer.agent.initialize(params)
  
  
  val possibleActions = List(0,1,2,3,4,5,6) //All actions are always possible, as field doesn't fill up
  
  //This will be looped over and over to simulate a game
  val stateRewardSequence = List(
      ("2k(3) 2b(1)",17),
      ("4k(4)",5),
      ("4k(3) 4b(1)",3),
      ("2k(1) 2b(6)",-2),
      ("4k(0)",-10),
      ("2k(0) 2k(1) 3k(1)",2),
      ("2k(6) 3b(1) 3b(0)",7),
      ("2k(0) 2b(3)",3)).map(pair => (StateHelper.convertState(pair._1), pair._2.toDouble))
      
  
  
  //Both experiences have only the states: true, 4k(X) and 4b(X) for now
  
  "TreeCARCASS and CARCASS" should "both select the same actions and learn states at the same rate" in {
    for(i <- (0 until 1000)) { //Simulate 1000 steps
      val (state,reward) = stateRewardSequence(i%stateRewardSequence.length)
      val caction = cplayer.selectAction(state, reward, possibleActions)
      val taction = tplayer.selectAction(state, reward, possibleActions)
    
      caction should be (taction)
      exp1.stateCount should be (exp2.stateCount)
    }  
  }    
  
  it should "both have more than the 3 initial states" in {
    exp1.stateCount.get should be > 3
    exp2.stateCount.get should be > 3
  }
  
  it should "both have learned 8 states" in {
    exp1.stateCount.get should be (8)
    exp2.stateCount.get should be (8)
  }
  
}