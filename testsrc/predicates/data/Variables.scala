package predicates.data

import arch2.base.predicates.Variable

object Variables {
  //Provide some variables for testing
  val V = Variable('V')
  val W = Variable('W')
  val X = Variable('X')
  val Y = Variable('Y')
  val Z = Variable('Z')
  val Q = Variable('?')
}