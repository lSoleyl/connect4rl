package arch2.data

import arch2.base._
import java.io.File
import org.apache.log4j.Logger
import utils.Log
import utils.Options._
import java.util.concurrent.Semaphore
import utils.SyncObject
import java.io.InputStream
import java.io.OutputStream
import java.io.FileInputStream
import arch2.data.experience.HeaderV1
import java.io.FileOutputStream
import utils.Tap._

/** This class wraps a loading routine for an agent together with an optional description. This class ensures that only one
 *  instance of the learndata is present. Additionally this class provides a mechanism to prevent saving and playing the same learn data 
 *  simultaneously. 
 *  
 *  @param loadable the Object which describes how to load the agent and instantiates new Agents of this type
 *  @param description is used in the toString function if not None otherwise the loadable's toString is used
 */
abstract class AILoader(val loaderName:String) extends Log with Ordered[AILoader] with SyncObject {
  private val semaphore = new Semaphore(1)
  
  var description:Option[String] = None
  var players:List[Player] = Nil //The loader now holds a list of one or two players.
  var source:Option[File] = None
  
  /** This function is used to display the loader with a name in the loader list
   */
  def displayName:String = loaderName
  
  
  def loadFromFile(in:File, status:ProgressInfo) {
    loadFrom(Some(new FileInputStream(in)), status)
    source = Some(in)    
  }
  
  /** This method provides a method to allow agents to play against themselves.
   *  It takes care of loading the required agents and returns exactly two different player instances.
   *  The loader locks should be acquired before calling this method to ensure that the agent's experience won't be changed 
   *  in between.
   *  
   *  @param l2 the other loader to instantiate (may be eq to this)
   *  @param loadFn a function which takes an AILoader and calls it's loading routing with the apropriate arguments. (and displays a progress bar) 
   *  
   *  @return the pair (p1,p2)
   */
  def loadPlayers(l2:AILoader)(loadFn:AILoader=>Unit):(Player,Player) = {
    //Load all loaders, which aren't yet loaded
    List(this,l2).foreach { l => if (!l.isLoaded) loadFn(l) }
    
    if (compare(l2) == 0) { //l1 == l2 (this must provide both players)
      if (!canPlayAgainstItself)
        throw new Exception(s"$this cannot play against itself!")
      else {
        val List(p1,p2) = players
        (p1,p2)
      } 
    } else { //l1 != l2 (take first players from both loaders)
      val List(p1,p2) = List(this,l2).map(_.getPlayer)
      (p1,p2)
    }
  }
  
  def loadFrom(in:Option[InputStream], status:ProgressInfo)
  def isLoaded:Boolean = players != Nil
  
  def getPlayer:Player = getPlayers.head
  
  def getPlayers:List[Player] = {
    if (players.isEmpty) {
      log.error(s"Requested a player from $this before loading experience")
      throw new Exception("Requested player before initialization has finished")
    } else 
      players
  }
  
  def saveToFile(f:File, status:ProgressInfo) {
    save(Some(new FileOutputStream(f)), status)
    source = Some(f) //change source
  }
  
  def save(out:Option[OutputStream], status:ProgressInfo)
  
  /** This method should return a new AILoader, which will always load from the specified file
   */
  def bindToFile(f:File, desc:String):AILoader
  
  def hasSource = !source.isEmpty
  
  def acquire = semaphore.acquire()
  def release = semaphore.release()
  
  val canPlayAgainstItself = true
  
  override def toString = description default displayName
  
  val isWritable:Boolean
  
  def v1Header:Option[HeaderV1] = None
  def stateCount:Option[Int] = None
  def customAction {  }
  
  //This class must be comparable to use lock ordering for Deadlock prevention
  def compare(that:AILoader):Int = if (this eq that) 0 else toString.compare(that.toString)
}

object AILoader extends Log {  
  
  def searchForData(path:String):List[AILoader] = {
    import utils.Factors._
    import utils.Tap._
    log.info("Searching '" + path + "' for loadable agents")
   
    val files = new File(path).listFiles()
    if (files != null)
      files.flatMap(file =>
       if (!file.isFile())
         None
       else { 
         file.getAbsolutePath() match {
           
           case FileNameFormat(agentClass, description) => loaders.get(agentClass) match {
             case None => log.warn("No loader found for file: '" + file.getName() + "'"); None
             case Some(creator) => Some(creator.bindToFile(file, description default file.getName()))
           }
           
           case FileFormat2(agentClass, description) => loaders.get(agentClass) match {
             case None => log.warn(s"No loader found for file ${file.getName()} containing an agent of type $agentClass"); None
             case Some(creator) => Some(creator.bindToFile(file, description default file.getName()))
           }
           
           case name:String => log.info("file '" + name + "' not loaded, because it doesn't match the file name pattern"); None 
         }
       }
     ).toList.tap(l => log.info("Found " + l.length + " agents"))
     else {
       log.warn(s"Expected learnData at '$path', but nothing found. Can't load any saved experience")
       Nil
     }       
  } 
  
  def commentString(comment:Option[String]):String = if (comment.isEmpty) "" else " (" + comment.get + ")"

  import arch2.ai._

  val loaders:Map[String, AILoader] = Map(
    QLearningAI.Loader,
    MCLearningAI.Loader,
    MMQLearningAI.Loader,
    MMMCLearningAI.Loader,
    MMMCDLearningAI.Loader,
    PredicateQLearningAI.Loader,
    DynamicCQLearningAI.Loader,
    DynamicCQLearningAI2.Loader,
    SDynamicCQLearningAI.Loader,
    SimpleDynamicTreeQLearningAI.Loader,
    "DTQLearningAI" -> SimpleDynamicTreeQLearningAI.Loader._2, //Alias for loading old files
    DynamicTreeQLearningAI.Loader.ttap2(_.displayName("DTQLearningAI")), //Displayname differs from actual agent name
    
    //AIs added to resume runs
    "RandomAI" -> GenericAILoader("RandomAI", new RandomAI(_)),
    "SimpleAI" -> GenericAILoader("SimpleAI", new SimpleAI(_)),
    "GreedyPredicateAI" -> GenericAILoader("GreedyPredicateAI", new GreedyPredicateAI(_))
  )
  
  
  private var loaderList:Option[List[AILoader]] = None
  
  def loaderList(aiPath:String):List[AILoader] = {
    if (loaderList.isEmpty) {
      loaderList = Some((List(
        GenericAILoader("Player", new HumanPlayer(_)),
        GenericAILoader("PredicatePlayer", new PredicateHumanPlayer(_)),
        GenericAILoader("StringPlayer", new StringHumanPlayer(_))) ++
        loaders.values.toSet.toList ++ //Ensure no duplicate loaders are displayed in the list
        searchForData(aiPath)).sortBy(_.toString)
      )            
    }
  
    loaderList.get
  } 
}