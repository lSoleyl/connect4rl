package arch2.field

//Read-Only field traits
trait Field1DR {
  def apply(index:Int):Stone  
}

trait Field2DR {
  def apply(column:Int, row:Int):Stone
  
   /** This method determines the row in which a 
   *  stone would land when thrown into column "col"
   *  
   *  @param col to column to throw the stone into
   *  
   *  @return the row
   *  @throws an exception if the column is full
   */
  def getLandingPos(col:Int):Int = {
    for(row <- 0 until rows) {
      if (this(col,row) == Stone.EMPTY)
        return row
    }
    throw new Exception("Column " + col + " is full!")      
  }
  
  val rows:Int
  val columns:Int
}

/** This trait adds a list of rows to the field
 */
trait RowList extends Field2DR {
  import sequences.Row
  val row:Array[Row] = (0 until rows) map (Row(this, _)) toArray
}


trait ColumnList extends Field2DR {
  import sequences.Column
  val column:Array[Column] = (0 until columns) map (Column(this, _)) toArray
}

trait Field1DRW extends Field1DR {
  def update(index:Int, value:Stone)
} 

trait Field2DRW extends Field2DR {
  def update(column:Int, row:Int, value:Stone)
  def clear
}

/** This trait does absolutely no bounds checking, etc.
 *  All it does is map a 1D-Access to a 2D-Access
 *  
 *  All it needs to do this is the amount of columns the field has
 */
trait FieldIndexMapper extends Field1DRW with Field2DRW {
  def apply(x:Int, y:Int) = super.apply(x + y*columns)
  def update(x:Int, y:Int, v:Stone) = super.update(x + y*columns, v)
  
  //Source methods
  abstract override def apply(x:Int) = super.apply(x)
  abstract override def update(x:Int, v:Stone) = super.update(x,v)
  val columns:Int
}

/** This trait allows to access the field, using indicies starting from 1
 *  instead of 0
 *  
 *  @remark very experimental...
 */
trait Base1Index extends Field2DRW {
  abstract override def apply(x:Int, y:Int) = super.apply(x-1, y-1)
  abstract override def update(x:Int, y:Int, v:Stone) = super.update(x-1, y-1, v)
}