package utils.io

import java.io.BufferedReader

object ReadLines {
  implicit class LineReader(reader:BufferedReader) {
    
    def lineStream:Stream[String] = {
      val line = reader.readLine()
      if (line != null)
        line #:: lineStream
      else
        Stream.empty
    }
    
  }

}