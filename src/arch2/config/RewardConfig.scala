package arch2.config

case class RewardConfig(won:Double, tied:Double, lost:Double)

object RewardConfig {
  import arch2.data.Serializer
  
  implicit object RewardSerializer extends Serializer[RewardConfig] {
    override def write(c:Type)(implicit writer:ByteWriter) {
      writer.writeDouble(c.won)
      writer.writeDouble(c.tied)
      writer.writeDouble(c.lost)
    }
    
    override def load()(implicit reader:ByteReader) = {
      import reader._
      RewardConfig(readDouble, readDouble, readDouble)
    }
  }
  
}