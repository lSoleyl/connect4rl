package arch2.gui

import scala.swing._
import scala.swing.ListView.IntervalMode
import utils.ui.PanelWrapper._
import utils.TryWithResources
import java.io._
import arch2.game.MultiBatchRunner
import scala.util.{Failure, Success}
import utils.Log
import utils.TimeMeasureMS
import utils.Tap._
import utils.Options._ 

class ResumeWindow(files:List[String], callback: =>Unit) extends Frame with Log {
  title = "Select batch run to resume"
  
  var resumed = false
    
  val fileSelect = new ListView(files) {
    selection.intervalMode = IntervalMode.Single
  }
  
  val resumeButton = new ActionButton("resume", resume)
  
  fileSelect.selectIndices(files.length - 1)
    
  contents = new BoxPanel(Orientation.Vertical) {
    contents += fileSelect.flow
    contents += new FlowPanel {
      contents += resumeButton
      contents += new ActionButton("export results", exportResults)
    }
  }
  
  def resume {
    resumed = true
    val fileName = fileSelect.selection.items.head
    val selectedFile = new File(fileName)
    val measure = new TimeMeasureMS
    
    TryWithResources(new FileInputStream(selectedFile)) { source => measure { MultiBatchRunner.resume(source) } } match {
      case Failure(ex) => log.error("Resumed batch run failed", ex)
      case Success(res) => {
        //Don't delete file for now, for safety reasons, but rename old to hide it in future resume runs
        selectedFile.renameTo(new File(fileName + ".old"))
            
        new ResultWindow(res)
        log.info(s"Batch run took ${measure.result} ms")
        log.info(f"Overall simuation speed was ${res.totalEpisodes / (measure.result/1000.0)}%.2f ep/sec")
      }        
    }
    
    Swing.onEDT { this.close }
    callback
  }
  
  //Export the game statistics, stored inside a .resume file
  def exportResults {
    val selectedFile = new File(fileSelect.selection.items.head)
    
    TryWithResources(new FileInputStream(selectedFile)) { source =>
      
      MultiBatchRunner.withResumeData(source) { data =>
        import data._
        import utils.Factors._
        val fileName = s"$p1-vs-$p2-${totalEpisodes.short}.partial.txt"
        MultiBatchRunner.exportStats(p1, p2, results, fileName, batchConfig.multi.outputDir)
      }     
      
    } match {
      case Failure(ex) => log.error(s"Failed to export game statistics from $selectedFile", ex)
      case _ => log.info(s"Successfully exported resume file stats")
    }
  }
  
  override def closeOperation {
    if (!resumed) { //Unlock buttons if no run has been resumed and window just got closed
      callback
    }
  }
}


