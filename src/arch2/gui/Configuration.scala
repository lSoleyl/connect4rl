package arch2.gui

import arch2.ai._
import arch2.field.Stone._
import utils.Factors._
import arch2.data.AILoader
import spray.json._
import java.io._
import utils.JSON
import scala.util.{Try, Failure, Success}
import org.apache.log4j.Logger
import utils.Log
import arch2.config.RewardConfig

object Configuration extends Log {      
  val CONFIG_FILE = "gui_configuration.json" //configuration file
    
  /** Loads the current configuration from the default config file.
   *  If the file doesn't exist then a default configuraiton will be provided
   *  
   *  @return the configData object which contains all configuration information
   */
  def load:ConfigurationData = {
    val jsonConfig = JSON.load(CONFIG_FILE) match {
      //File does not exist
      case None => {
        log.info("No " + CONFIG_FILE + " found, using default config")
        JSONConfigurationData()
      } 
      case Some(json) => {
        import ConfigProtocol._
        configFormat.read(json)
      } 
    }
    
    jsonConfig.convert
  }
  
  /** Saves the given configuration data into the default save file.
   *  The old data will be overwritten 
   * 
   * @param config the configuration to save
   */
  def save(config:ConfigurationData) = {
    import ConfigProtocol._
    Try {
      JSON.save(configFormat.write(config.convert), CONFIG_FILE)
    } match {
      case Failure(ex) => log.error("Failed to write config file. No write access to ./" + CONFIG_FILE, ex)
      case Success(_) => log.info("Successfully saved configuration") 
    }
  }
}


/** This class represents the configuration data needed for the GUI
 *  The default values are used in case when no configuration file is available
 *  
 *  lambda = the effective lambda value (gamma * lambda)
 *           0.8 equals a factor of:
 *             * 0.1 after 10 episodes
 *             * 0.01 after 20 episodes
 */
case class ConfigurationData(batchRun:Boolean, alpha:Double, dynamicAlpha:Boolean, e_2:Int, gamma:Double, epsilon:Double, lambda:Double, rewards:RewardConfig, agentDir:String) {
  def convert = JSONConfigurationData(Some(batchRun), Some(alpha), Some(dynamicAlpha), Some(e_2), Some(gamma), Some(epsilon), Some(lambda), Some(rewards), Some(agentDir))
  
  def toJSON = {
    import ConfigProtocol._
    configFormat.write(convert)
  }
}

object ConfigurationData {
  def fromJSON(json:JsValue) = {
    import ConfigProtocol._
    configFormat.read(json).convert
  }
  
  import arch2.data.Serializer
  implicit object ConfigSerializer extends Serializer[ConfigurationData] {
    override def write(c:ConfigurationData)(implicit writer:ByteWriter) {
      import writer._
      import c._
      
      writeBool(batchRun)
      writeDouble(alpha)
      writeBool(dynamicAlpha)
      writeInt(e_2)
      writeDouble(gamma)
      writeDouble(epsilon)
      writeDouble(lambda)
      
      Serializer.serialize(rewards)
      
      writeString(agentDir)      
    }
    
    override def load()(implicit reader:ByteReader) = {
      import reader._
      val result = ConfigurationData(readBool, readDouble, readBool, readInt, readDouble, readDouble, readDouble, Serializer.load[RewardConfig], GameGUI.config.agentDir)
      readString //<-- ignore the actual agentDir and use the local one. This makes the .resume files usable across different machines
      result
    }
  }
}

/** Same as above but with options for spray-json to prevent error messages if some entries are missing
 */
case class JSONConfigurationData(batchRun:Option[Boolean] = None, 
                                 alpha:Option[Double] = None, dynamicAlpha:Option[Boolean] = None, e_2:Option[Int] = None, 
                                 gamma:Option[Double] = None, epsilon:Option[Double] = None, lambda:Option[Double] = None,
                                 rewards:Option[RewardConfig] = None, agentDir:Option[String] = None) {
  import utils.Options._                                                                                                                                                                      //v- Default: ./agents
  def convert = ConfigurationData(batchRun default false, alpha default 0.2, dynamicAlpha default false, e_2 default 1000000,gamma default 0.8, epsilon default 0.1, lambda default 0.8, rewards default RewardConfig(100, 0, -100), agentDir default ("." + java.io.File.separator + "agents"))  
}

object ConfigProtocol extends DefaultJsonProtocol {
  implicit val rewardConfigFormat = jsonFormat3(RewardConfig.apply)  
  implicit val configFormat = jsonFormat9(JSONConfigurationData.apply)
}