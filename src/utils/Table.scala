package utils
/** Table has the following format:
 *  [ <- Vertical direction (CONTAINS rows and IS A table) 
 *     [ , , , ] <- Horizontal direction (CONTAINS column values and IS A row)
 *     [ , , , ]
 *     [ , , , ]
 *  ]
 */
object Table {
  implicit class ListTable[T](table:List[List[T]]) {
    //Execute the reduce operator on each row
    def reduceInner(op:(T,T) => T):List[T] = table.map(row => row.reduce(op))
    
    //Execute the reduce operator on each column (outer, beacuse the operation ist executed between the rows)
    def reduceOuter(op:(T,T) => T):List[T] = transpose.reduceInner(op)
    
    
    def foldLeftInner[A](z:A)(f:(A,T) => A) = table.map(row => row.foldLeft(z)(f))
    def foldLeftOuter[A](z:A)(f:(A,T) => A) = transpose.foldLeftInner(z)(f)
    
    
    
    def transpose:List[List[T]] = {
      def innerTranspose(t:List[List[T]]):List[List[T]] = { //Not tail recursive and not optimal but short
        if (t.head != Nil) {
          val row = t.map(_.head)
          row :: innerTranspose(t.map(_.tail))    
        } else {
          Nil
        }
      }
      
      innerTranspose(table)
    }
  }
}