package arch2.data

import arch2.base.Player
import utils.Log
import utils.Times._
import experience._
import experience.base.ExperienceBase
import arch2.data.experience.StreamSerializable
import java.io._

/** This method implements a very generic AILoadable to be used as base class.
 *  This implementation is complete, so a derving class normally isn't required to implement anything and it can even be used directly as loader.
 *  The companion object implements a useful apply() method for that case.
 *  
 *  Also the generic arguments are infered through the provided arguments
 *  
 *  @param loaderName the name of the loader, which will be entered into the HeaderV1 part as agentType
 *  @param newExperience a by-name experience instantiator, which will be called at most once
 *  @param agentCreator a function which creates an Player from the loaded/created experience
 *  
 *  @param file the file parameter which will be passed to the loader
 */
class AgentAILoader[E <: ExperienceBase with StreamSerializable with HeaderV1, A <: Player](loaderName:String, newExperience: =>E, agentCreator: (E,AILoader)=>A, override val canPlayAgainstItself:Boolean) extends AILoader(loaderName) with Log {
  import utils.Options._
  
  var exp:Option[E] = None
  
  
  /** Allow the user to define a custom display name for the loader
   */
  private var displayNameValue:String = loaderName
  override def displayName:String = displayNameValue
  
  /** Create set the display name and return the loader itself
   */
  def displayName(name:String):AgentAILoader[E,A] = {
    displayNameValue = name
    this
  }
  
  
  /** This method will always generate a new agent with either newly loaded experience or empty experience
   */
  def loadFrom(in:Option[InputStream], progress:ProgressInfo) = {
    exp = Some(newExperience)
    exp.get.agentType = Some(loaderName)
    
    val stream = in or source.map(new FileInputStream(_))
    
    if (stream) {
      log.info(s"Loading $loaderName...")
      exp.get.loadFromStream(stream.get, progress)
    } else {
      log.info(s"Creating new $loaderName without experience, because no file was specified for loadFrom()")
    }
    
    players = List(agentCreator(exp.get, this))
    if (canPlayAgainstItself) //Instantiate second player from same experience if possible
      players = agentCreator(exp.get, this) :: players
  }
  
  override def save(out:Option[OutputStream], progress:ProgressInfo) {
    if (isLoaded) {
      val stream = out or source.map(new FileOutputStream(_))
      if (stream) {
        exp.get.writeToStream(stream.get, progress)
      } else {
        log.error(s"Can't save $this, because it wasn't loaded from a file and no file was provided to save it's progress")
      }
    }
  }
  
  override def bindToFile(f:File, desc:String):AILoader = {
    val loader = new AgentAILoader(loaderName, newExperience, agentCreator, canPlayAgainstItself)
    loader.source = Some(f)
    loader.description = Some(desc)
    loader
  }
  
  override def v1Header:Option[HeaderV1] = exp
  
  override def stateCount:Option[Int] = exp.flatMap(_.stateCount)
  
  override def customAction { exp.iff(_.customAction) }
  
  override val isWritable = true
}


class SimpleAILoader[A <: Player](loaderName:String, agentCreator: AILoader=>A) extends AILoader(loaderName) with Log {
  /** This method will always generate a new agent with either newly loaded experience or empty experience
   */
  def loadFrom(in:Option[InputStream], progress:ProgressInfo) = {
    players = 2.collect { agentCreator(this) } //Always instantiate two players
  }
    
  override def save(out:Option[OutputStream], progress:ProgressInfo) { }
  
  override def bindToFile(f:File, desc:String) = throw new UnsupportedOperationException(s"Can't bind $loaderName to a file")
  
  override val canPlayAgainstItself = true
  override val isWritable = false
}


/** Companion object with a convinience apply method to create a AILoadable.Creator
 *  for parameter description see constructor of GenericAILoadable
 */
object GenericAILoader {
  def apply[E <: ExperienceBase with StreamSerializable with HeaderV1, A <: Player](loaderName:String, newExperience: =>E, agentCreator: (E,AILoader)=>A, canPlayAgainstItself:Boolean = CAN_PLAY_AGAINST_ITSELF) = (loaderName, new AgentAILoader(loaderName, newExperience, agentCreator, canPlayAgainstItself))
  def apply[A <: Player](loaderName:String, agentCreator: AILoader=>A) = new SimpleAILoader(loaderName, agentCreator)
    
  
  val CANNOT_PLAY_AGAINST_ITSELF = false
  val CAN_PLAY_AGAINST_ITSELF = true
}