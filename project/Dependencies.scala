import sbt._

object Dependencies {
  val sprayJSON  = "io.spray" %%  "spray-json" % "1.3.2"
  val log4j      = Seq(
    "org.apache.logging.log4j" % "log4j" % "2.2"
  )
  val scalaSwing = "org.scala-lang" % "scala-swing" % "2.10.5"
  val scalaTest = "org.scalatest" %% "scalatest" % "2.2.0" % "test"
}