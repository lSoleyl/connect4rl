package arch2.gui

import scala.swing._
import java.awt.RenderingHints
import java.awt.Color

import arch2.field._

class FieldPanel(val field:FieldState) extends Panel {
  val width = FieldPanel.calcSize(field.columns)
  val height = FieldPanel.calcSize(field.rows)
  
  background = Color.WHITE
  preferredSize = new Dimension(width, height)
  
  override def paint(g: Graphics2D) {
    implicit val gr = g
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    paintField
    paintFieldState
  }
  
  private def paintFieldState(implicit g:Graphics2D) {
    import FieldPanel._

    for {
      row <- 0 until field.rows
      col <- 0 until field.columns
    } if (field(col,row) != Stone.EMPTY) drawTile(col, row, field(col,row))
  }
  
  //col,row = zero-based
  private def drawTile(col:Int, row:Int, t:Stone)(implicit g:Graphics2D) {
    import FieldPanel._
    
    val x = col * (fieldSize + fieldSpace)
    val y = ((field.rows - 1)-row) * (fieldSize + fieldSpace)
    
    g.setColor(stoneColor(t))
    g.fillOval(x, y, fieldSize, fieldSize)
    g.setColor(Color.BLACK)
    g.drawOval(x, y, fieldSize, fieldSize)
  }
  
  private def paintField(implicit g:Graphics2D) {
    g.setColor(Color.WHITE) //Background
    g.fillRect(0, 0, width, height)
    
    g.setColor(Color.BLACK)
    g.drawRect(0, 0, width, height)
    
    for (i <- 1 to (field.columns - 1)) {
      val x = FieldPanel.calcPos(i)
      g.drawLine(x, 0, x, height)
    }
    
    for (i <- 1 to (field.rows - 1)) {
      val y = FieldPanel.calcPos(i)
      g.drawLine(0, y, width, y)
    }
  }
}

object FieldPanel {
  //GUI constants
  val fieldSize = 40//px
  val fieldSpace = 10//px
  
  def calcSize(elements:Int) = elements * fieldSize + (elements-1)*fieldSpace
  def calcPos(i:Int) = i * fieldSize + (i-1)*fieldSpace + fieldSpace/2
  
  val stoneColor = Map(
      Stone.YELLOW -> Color.YELLOW,
      Stone.RED -> Color.RED
  )
}