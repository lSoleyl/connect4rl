package arch2.ai

import arch2.base._
import arch2.field._
import arch2.data.AILoader

/** This AI simply chooses the first available action every time
 */
class SimpleAI(override val loader:AILoader) extends Player {
  override def turn(state:FieldState, actions:List[Int], color:Stone):Int = actions.head
}