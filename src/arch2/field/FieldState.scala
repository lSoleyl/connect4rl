package arch2.field

import Stone._
import scala.annotation.tailrec

/** This trait offers a read-only interface for the player to help determine the next move.
 */
trait FieldState extends Field2DR with RowList with ColumnList {
  /** Method to access the field value. This method performs
   *  additional bounds checking and returns Stone.INVALID in case
   *  the given position is outside the field
   *  
   *  @param col the column to access
   *  @param row the row to access
   *  
   *  @return the stone type
   */
  abstract override def apply(col:Int, row:Int):Stone = {
    if (col < 0 || row < 0) 
      INVALID
    else if (col >= columns || row >= rows) 
      INVALID
    else 
      super.apply(col,row)
  }
  
  /** This method returns a list of lines starting from the
   *  given position in the field. The field is treated as if it
   *  had the color given as parameter.
   *  With this method an agent can reason about "what-if" states.  
   *  
   *  @param col the selected column
   *  @param row the selected row
   *  @param color the color to be assumed for the field. If the color is passed as
   *               EMPTY then lines in opposite directions but same color won't get
   *               connected to one line and the lines will be one field shorter
   *  
   *  @return a List of Line elements, which contain information about
   *          the color of the line and it's length
   */
  def getLines(col:Int, row:Int, color:Stone):List[Line] = {
    if (color == INVALID)
      Nil //Empty List if invalid field is selected
    
    val lines = PAIRED_VECTOR_CLOCK.map((d) => 
      (getLine(col+d._1.x, row+d._1.y, d._1, color, if (color != EMPTY) 1 else 0),
       getLine(col+d._2.x, row+d._2.y, d._2, color, if (color != EMPTY) 1 else 0)))
     
    if (color != EMPTY) 
      lines.flatMap(ll => {
        if (ll._1.color == color && ll._2.color == color)
          List(Line(color, ll._1.length + ll._2.length - 1))
        else
          filterNonEmptyLines(ll)
      })
    else
      lines.flatMap(ll => filterNonEmptyLines(ll))
  }
  
  /** Determines all rows which are starting from the specified position
   *  In contrast to the above method the actual field color is used and
   *  not a hypothetical one.
   *  
   *  @param col the column of the pos
   *  @param row the row of the pos
   *  
   *  @return a list of Line objects
   */
  def getLines(col:Int, row:Int):List[Line] = getLines(col:Int, row:Int, apply(col,row))
  
  /** Same as above but automatically determines the 
   *  lowest EMPTY position in the given column.
   *  This method actually checks what lines are currently pointing
   *  to the position where the next playstone would land. 
   * 
   * @param col the column
   * 
   * @return a list of lines
   */
  def getLines(col:Int, color:Stone):List[Line] = getLines(col, getLandingPos(col), color)

  
  /** Returns a list of columns, which are free to throw a stone into
   */
  def getFreeColumns:List[Int] = COLUMN_LIST.flatMap(col => if (this(col, rows-1) == EMPTY) Some(col) else None)
  
  
  
  private def filterNonEmptyLines(ll:(Line,Line)):List[Line] = (if (ll._1.length > 0) List(ll._1) else List()) ++ (if (ll._2.length > 0) List(ll._2) else List())
  
  /** This helper method determines the length of a line
   *  starting from pos (col,row) recursively
   *  
   */
  @tailrec
  private def getLine(col:Int, row:Int, d:Vector, color:Stone, length:Int = 0):Line = {
    val current = this(col, row)
    
    if (current == EMPTY || current == INVALID) 
      Line(color, length)
    else if ((color == EMPTY && current != INVALID) || (color == current))
      getLine(col+d.x, row+d.y, d, current, length+1)
    else
      Line(color,length)
  }
  
  
  

  //abstract members to implement
  val columns:Int
  val rows:Int
}


