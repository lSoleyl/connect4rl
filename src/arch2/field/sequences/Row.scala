package arch2.field.sequences

import arch2.field.Field2DR
import arch2.field.Stone

/** Row like view onto the field, used for string prepresentation of 
 *  the field's state. This class is inspired by vg.base.sequences.Row[A], but 
 *  is tailored towards the new field classes
 */
case class Row(field:Field2DR, index:Int) extends Seq[Stone] {
  if (index >= field.rows || index < 0)
    throw new IllegalArgumentException(s"Can't instantiate a row at index $index for a field with ${field.rows} rows!")
  
  def apply(i:Int):Stone = field(i,index)
  def iterator:Iterator[Stone] = new RowIterator(this)
  def length = field.columns
  
  override def toString = "Row("+ mkString(",") + ")"
}

class RowIterator(row:Row) extends Iterator[Stone] {
   var i = 0
   def hasNext = i < row.length
   def next = {
     i += 1
     row(i-1)
   }
}