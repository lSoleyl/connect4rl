package arch2.field

/** This field is a two dimensional implementation of a game field.
 *  This seems to be more performant than using the one dimensional PlainField.
 */
class ArrayField2D(val columns:Int, val rows:Int) extends Field2DRW {
  val data = Array.fill[Stone](columns, rows) { Stone.EMPTY }
  
  override def apply(x:Int,y:Int) = data(x)(y)
  override def update(x:Int, y:Int, c:Stone) { data(x)(y) = c }
  
  override def clear {
    for{ 
      i <- 0 until columns
      j <- 0 until rows
    } data(i)(j) = Stone.EMPTY
  }
}