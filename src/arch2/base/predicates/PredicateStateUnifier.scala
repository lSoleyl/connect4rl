package arch2.base.predicates

import utils.Log
import arch2.base.Agent
import arch2.math.RandomGenerator

trait PredicateStateUnifier[A,V] extends Log {
  import utils.Options._
  
  implicit val variableFactory:Int => Variable[A]
  val defaultVariable:Variable[A]
  
  type CA = A
  type CS = BoundPredicateState[A]
  type AS = UnboundPredicateState[A]
  type AA = Variable[A]
  
  val agent:Agent[AS,AA,V]
  
  /** Notification hook for CARCASSExperience to generate trace data
   */
  val notifyExperience:(CS,AS,AA,CA)=>Unit
  
  /** This object is used to randomly select an action if the agent has chosen '?'
   *  This can be overwritten to use a different generator 
   */
  val random:RandomGenerator = RandomGenerator.defaultGenerator
  
  /** This method gets a raw predicate state, and matches the state against unbound states in the 
   *  experience's state list. (Assuming a PredicateListExperience)
   * 
   */
  def selectAction(state:CS, reward:Double, possibleActions:List[CA]):CA = {
    import utils.Tap._
    
    val (unboundState,boundVariables) = bindStateToVariables(state)
    val variables = boundVariables.map(_.variable) ::: List(defaultVariable)

    val variable = agent.selectAction(unboundState, reward, variables) //TODO would be nice, if QLearning would use optimized lookups for predicate lists
    
    val boundVariable = boundVariables.find(_.variable == variable)
    if (boundVariable)
      return boundVariable.get.tap(v => notifyExperience(state, unboundState, v.variable, v.value)).value
    else { //it was the random_action()-variable
      val boundActions = boundVariables.map(_.value)
      val randomActions = possibleActions diff boundActions
      val poolActions = if (!randomActions.isEmpty) randomActions else possibleActions
        
      poolActions(random.nextInt(poolActions.size)).tap(ca => notifyExperience(state, unboundState, defaultVariable, ca))
    }
  }
  
  
  /** This method actually searches the expercience for the abstract state by calling extractFirstMatch() with _.unifyWith().
   *  If a match is found, it returns the abstract state and the variable binding.
   *  
   *  If the concrete state can't be unified to any abstract state, then it will throw an exception because the state list is expected 
   *  to have an abstract state which unifies with every state at it's end.
   *  
   *  @param state the concrete state to look for
   *  
   *  @return A pair of the unbound prdedicate state and a list of variable bindings to 
   */
  private def bindStateToVariables(state:BoundPredicateState[A]):(UnboundPredicateState[A],List[BoundVariable[A]]) = {
    import utils.Tap._
    
    val eMatch = agent.experience.extractFirstMatch(_.unifyWith(state))
    if (eMatch)
      return (eMatch.get.state,eMatch.get.matchResult)
    else
      throw new NoUnifiableStateException
  }
  
  class NoUnifiableStateException extends Exception
}