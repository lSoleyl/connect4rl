package arch2.gui

import scala.swing._
import arch2.game.GameStatistics
import arch2.base._
import javax.swing.table.DefaultTableCellRenderer
import javax.swing.SwingConstants
import utils.ui.PanelWrapper._
import utils.Log

class ResultWindow(val stats:GameStatistics) extends Frame with Log {
  title = "Results of last batch run"
  
  preferredSize = new Dimension(800,180)
  
  /** Helper function to retrieve all required information from the statistics object
   *  for the given player.
   *  
   *  @param p the player to get the information array
   *  @param useLoaderName if both players have the same name (Loader-Classname) then this can be set to true to use the actually set loader name
   *  
   *  @return a string array containing the information, which should be displayed
   */
  private def getPlayer(p:Player, useLoaderName:Boolean):Array[String] = {
    val pstats = stats.getFor(p)
    
    val name = if (useLoaderName) p.loader.loaderName else p.toString
    
    import pstats._
    import utils.Percentage._
    Array(
      name,
      f"$won%10d after $wonAvgTurns%2.2f turns",
      f"$draws%10d after $drawAvgTurns%2.2f turns",
      f"$lost%10d after $lostAvgTurns%2.2f turns",
      (won of totalEpisodes) +   "  (" + (wonAsStarter of won) +    " as starting player)",
      (draws of totalEpisodes) + "  (" + (drawAsStarter of draws) + " as starting player)",
      (lost of totalEpisodes) +  "  (" + (lostAsStarter of lost) +  " as starting player)"
    )
  }
  
  private val descriptions = Array(
      "vs.",
      "won games",
      "tied games",
      "lost games",
      "won",
      "tied",
      "lost"      
  )
    

  private def statTable:Component = {
    val center = new DefaultTableCellRenderer
    center.setHorizontalAlignment(SwingConstants.CENTER)
    
    //Use loader names if player names are equal
    val useLoaderName = (stats.p1.toString == stats.p2.toString)
    val List(p1,p2) = List(stats.p1, stats.p2).map { getPlayer(_, useLoaderName) }
    
    val tbl = new Table(p1.size, 3)
    
    tbl.peer.setDefaultRenderer(classOf[String], center)
    
    for (row <- (0 until p1.size)) {
      tbl(row, 0) = p1(row)
      tbl(row, 1) = descriptions(row)
      tbl(row, 2) = p2(row)
    }
    
    tbl
  }
  
  
  contents = new BoxPanel(Orientation.Vertical) {
    contents += new Label("Results:").flow
    contents += new BoxPanel(Orientation.Horizontal) {
      contents += statTable
    }
  }
  
  log.info("Displaying game results")
  
  Swing.onEDT { 
    this.open
    this.centerOnScreen
  }
}