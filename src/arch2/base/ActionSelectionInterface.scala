package arch2.base

/** This trait is used by the human player class to interact with the actual
 *  human either via console or GUI.
 */
trait ActionSelectionInterface {
  def selectAction(actions:List[Int]):Int
  def require
}