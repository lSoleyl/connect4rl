package utils.generic.functions

trait Partition[M[_]] {
  def apply[A](ma:M[A], predicate:A=>Boolean):(M[A],M[A])
}

object Partition {
  implicit object ListPartition extends Partition[List] {
    override def apply[A](l:List[A], predicate:A=>Boolean) = l.partition(predicate)
  }
}