package arch2.data.experience.base

import arch2.base.predicates._
import utils.Log
import scala.collection.mutable.Map
import utils.Options._


/** Basically the PredicateListExperience implementation with a map instead of a list.
 */
trait PredicateMapExperience[A,V] extends Experience[UnboundPredicateState[A],Variable[A],V] with Log {
  import scala.collection.mutable.Map
  import utils.Options._
  
  type ActionMap = Map[Variable[A],V]
  type BoundVariables = List[BoundVariable[A]]
  type ListEntry = ActionMap
  
  val defaultVariable:Variable[A] //Variable for random action '?'
  val defaultVariableStartValue:V
 
  
  protected var experience:Map[State, Map[Action, V]] = Map()
  
  override def containsState(state:State) = experience.contains(state)
  
  /** With this method the caller can add a new entry into the list and gets a ListEntry returned, which 
   *  he then can manipulate without matching the state again.
   *  
   * @param state the state to add
   * 
   * @return the newly created listEntry
   */
  def enterState(state:State):ActionMap = {
    val actionMap:ActionMap = Map()
    state.getBoundActions.foreach(actionMap(_) = default) //Populate Entry
    actionMap(defaultVariable) = default
    
    experience(state) = actionMap
    actionMap
  }
  
  
  override def bestActionEntry(state:State, allowedActions:List[Action], isBetter:(Value,Value)=>Boolean):Entry = {
    val entry = experience.get(state)
    
    if (!entry)
      return Entry(state, allowedActions.head, default)
      
    logDebug("StateEntry: " + entry.get)
      
    val (action,value) = entry.get.reduce((av1,av2) => if (isBetter(av1._2,av2._2)) av1 else av2)
    Entry(state,action,value)
  }
  
  
  override def apply(key:SA):Value = experience.getOrElse(key.s, Map()).getOrElse(key.a, default)
  
  
  override def update(key:SA, value:Value) {
    val actionMap = experience.getOrElse(key.s, enterState(key.s))    
    actionMap(key.a) = value
  }
  
  override def enterState(state:State, actions:List[Action]) {
    val actionMap:ActionMap = Map()
    actions.foreach(actionMap(_) = default)
    actionMap(defaultVariable) = defaultVariableStartValue
    
    experience(state) = actionMap
  }
  
  
  override def toString = experience.toString
}