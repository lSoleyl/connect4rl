package arch2.game

import arch2.base.Player

case class PlayerStatistics(player:Player, won:Int, draws:Int, lost:Int, wonAsStarter:Int, drawAsStarter:Int, lostAsStarter:Int, wonAvgTurns:Double, drawAvgTurns:Double, lostAvgTurns:Double, totalEpisodes:Int, states:Option[Int]) {
  def winRate = won.toDouble / totalEpisodes
}