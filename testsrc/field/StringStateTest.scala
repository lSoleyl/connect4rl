package field

import org.scalatest._
import arch2.field.Stone._
import arch2.base.StringState
import arch2.game.Field
import arch2.field.Stone
import arch2.field.Stone._
import arch2.field.FieldState
import arch2.data.AILoader


class StringStateTest extends FlatSpec with Matchers {
  def f = Field
  
  //This function is used to retrieved the string state
  def convert(field:FieldState):String = {
    val state = new StringState() { 
      var state:String = null //Holds the last state
      override def turn(state:String, actions:List[Int], color:Stone):Int = {  
        this.state = state
        42 //Result is irrelevant
      }
      override val loader:AILoader = null //Needs to be implemented
    }
    
    //Actions and color are irrelevant
    state.turn(field, Nil, RED)
    state.state //return the state, stored in the converter
  }
  
  //Build a field according to a string state description
  def buildField(state:String):Field = {
    val field = f
    var color = YELLOW
    for(ch <- state) {
      val column = ch.toInt - '0'.toInt
      field.putIntoSlot(column, color)
      color = Stone.switch(color) //Switch turns
    }
    field //return complete field
  }
  
  "StringState conversion" should "return an empty string for an empty field" in {
    val field = f    
    convert(field) should be ("")
  }
  
  val states = List("1", "12", "021", "01234", "0123456", "012345", "434534"/*433544*/, "01621", "10261", "43434", "434343",
      "343465664565536", "31313123", "0333132", "313321311", "2113133", "43443")
  
  for (state <- states) {
    it should s"be symmetric for state '${state}'" in {
      convert(buildField(state)) should be (state)
    }
  }
  
  "Applying convert twice" should "work for an empty state" in {
    convert(buildField(convert(buildField("")))) should be ("")
  }
  
  //These states don't need to be ordered, because the first coonversion should order them
  val asymStates = states ::: List("120", "353533564244", "6543210", "0332213", "1101", "03133230", "1223303")
    
  for (state <- asymStates) {
    it should s"work for each state including '${state}'" in {
      val stateID = convert(buildField(state))
      convert(buildField(stateID)) should be (stateID)
    }
    
    it should s"build the same gamefield for '${state}'" in {
      val f1 = buildField(state)
      val f2 = buildField(convert(f1))
      
      for(row <- 0 until f1.rows) {
        f2.row(row).toString should be (f1.row(row).toString)
      }
    }
  }
   
}