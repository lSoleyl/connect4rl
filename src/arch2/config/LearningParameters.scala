package arch2.config

/** Most parameters are self explanatory if the formulas are known.
 *  
 *  lambda is the effective lambda value which is (lambda*gamma) to save some computation overhead
 */
case class LearningParameters(alpha:Double, gamma:Double, epsilon:Double, lambda:Double, dynamicAlpha:Boolean, e_2:Int)