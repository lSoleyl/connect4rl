package arch2.ai

import arch2.field._
import utils.Log
import arch2.base._
import arch2.config._
import arch2.base.predicates.BoundPredicateState
import arch2.data.AILoader

class HumanPlayer(override val loader:AILoader) extends Player with Log {
  var actionInterface:ActionSelectionInterface = null
  
  
  override def newGame(config:Seq[Any]) {
    config.foreach(_ match {
      case InterfaceConfiguration(interface) => actionInterface = interface
      case _ => 
    })
    
    if (actionInterface == null) {//This must be supplied to a human player
      log.error("Human player must be provided with an interface selection parameter. Provided config: " + config)
      throw new MissingInterfaceConfigurationException("Human player: " + this + " needs an interface to select an action")      
    } else {
      actionInterface.require
    }
  }

  /** Delegates the choice of the action to the human.
   *  The drawing of the gamefield won't be handled by the player, but instead by the game.
   */
  override def turn(field:FieldState, actions:List[Int], color:Stone):Int = actionInterface.selectAction(actions)
}


/** This class is mostly implemented for debugging purposes.
 *  It will print out all valid predicates each turn, using it's logger
 */
class PredicateHumanPlayer(l:AILoader) extends HumanPlayer(l) with PredicateState {
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone):Int = {
    log.info("state: " + state.mkString("  "))
    actionInterface.selectAction(actions)
  }
}

/** Another debugging player class
 */
class StringHumanPlayer(l:AILoader) extends HumanPlayer(l) with StringState {
  def turn(state:String, actions:List[Int], color:Stone):Int = {
    log.info("state: " + state)
    actionInterface.selectAction(actions)
  }  
}


class MissingInterfaceConfigurationException(msg:String) extends Exception(msg)