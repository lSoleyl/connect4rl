package arch2

package object math {
  //Default Vector
  type Vector = IntVector2D
  
  import MathTypes._
  implicit def tupleToIntVector2D(tuple:(Int,Int)) = IntVector2D(tuple._1, tuple._2)
  implicit def tupleToVector2D[T](tuple:(T,T))(implicit math:Ring[T]) = Vector2D[T](tuple._1, tuple._2)
}