package utils.generic.functions

import scala.collection.immutable.Queue

trait ForEach[M[_]] {
  def apply[A](ma:M[A], f:A=>Unit)
}

object ForEach {
  implicit object ListForEach extends ForEach[List] {
    override def apply[A](list:List[A], f:A=>Unit) {
      list.foreach(f)
    }
  }
  
  implicit object QueueForEach extends ForEach[Queue] {
    override def apply[A](queue:Queue[A], f:A=>Unit) {
      queue.foreach(f)
    }
  }
}