lazy val Connect4RL = (project in file("."))
  .settings(
    version := "1.1.0",
    scalaVersion := "2.10.4",
    sourcesInBase := false,  
    //Compiler
    scalaSource in Compile := baseDirectory.value / "src",
    mainClass in Compile := Some("arch2.gui.GameGUI"),
    //Test
    scalaSource in Test := baseDirectory.value / "testsrc",
    parallelExecution in Test := false,
    //Dependencies
    libraryDependencies += Dependencies.sprayJSON,
    libraryDependencies ++= Seq( //Log4J needs a few more
      "org.apache.logging.log4j" % "log4j-1.2-api" % "2.2",
      "org.apache.logging.log4j" % "log4j-api" % "2.2",
      "org.apache.logging.log4j" % "log4j-core" % "2.2",
      "org.apache.logging.log4j" % "log4j-iostreams" % "2.2"
    ),
    libraryDependencies += Dependencies.scalaSwing,
    libraryDependencies += Dependencies.scalaTest,
    //Run configuration
    fork in run := true,
    managedClasspath in (Compile, run) += baseDirectory.value / "resources",
    fullClasspath in (Runtime) += baseDirectory.value / "resources",
    //Artifacts
    publishArtifact in (Compile, packageBin) := true,
    publishArtifact in (Compile, packageDoc) := false,
    //Assemblies
    mainClass in assembly := Some("arch2.gui.GameGUI"),
    assemblyOutputPath in assembly := new File("./connect4rl.jar"),
    test in assembly := {} //Skip tests
  )


