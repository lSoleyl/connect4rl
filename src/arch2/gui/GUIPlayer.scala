package arch2.gui

import arch2.base._
import arch2.config._
import arch2.field._
import utils.Log 

/** A player wrapper which handles redrawing the field at the right moments.
 *  Additionally it provides a delay to artificially slow down the game to
 *  make it observable.  
 */
class GUIPlayer(p:Player, delayMS:Long) extends Player with Log {
  var board:GameBoard = null
  
  /** Game initialization
   */
  override def newGame(config:Seq[Any] = Nil) { 
    log.trace(s"configuring new Game for $p")
    config.foreach(_ match {
      case GameBoardConfig(b) => board = b
      case _ =>
    })
    log.trace("configuring wrapped Player")
    p.newGame(config) 
    log.trace("configuration done")
  }
  
  /** GUI-Player will sleep for an appropriate time and then let the player decide.
   */
  def turn(field:FieldState, possibleActions:List[Int], color:Stone):Int = {
    if (board.destroyed) throw new GameboardClosedException
    board.drawField
    Thread.sleep(delayMS)
    p.turn(field, possibleActions, color)
  }
  
  
  /** Callbacks for losing/winning
   */
  override def won  {
    board.drawField
    p.won 
  }
  override def lost {
    board.drawField
    p.lost 
  }
  override def tied {
    board.drawField
    p.tied 
  }
   
  
  val wrappedPlayer = p
  
  override def toString = p.toString + " (+GUI)"
  
  override val loader = p.loader 
}

class GameboardClosedException extends Exception("Gameboard was closed")