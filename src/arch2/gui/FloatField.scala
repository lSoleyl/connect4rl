package arch2.gui

import scala.swing._
import scala.swing.event.KeyTyped

class FloatField(columns:Int = 5) extends TextField(columns) {
  listenTo(keys)
  reactions += {
    case e:KeyTyped => {
      if (e.char == '.') {
        if (text.contains(".")) 
          e.consume 
      } else if (e.char == '-') {
        if (text.contains("-") || caret.position != 0)
          e.consume
      } else if(!e.char.isDigit) e.consume
    }
  }
  
  /** Getter and setter for field content (int only)
   */
  def value = text.toDouble
  def value_=(d:Double) { text = d.toString }
  

}