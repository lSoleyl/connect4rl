package arch2.gui

import scala.swing._
import utils.Factors
import scala.swing.event.KeyTyped
import utils.ui.PanelWrapper._
import utils.Log
import scala.concurrent._
import arch2.config.RewardConfig
import scala.concurrent.duration.Duration

private class ConfigWindow(val data:ConfigurationData) extends Frame with Log {
  title = "Application configuration"
  private val p = promise[Option[ConfigurationData]]
  val future = p.future
    
  val isBatchRun = new CheckBox
  isBatchRun.tooltip = "Run multiple games in a batch (no gameboard is drawn). Not available for human players"
  
  val alpha = new FloatField
  val gamma = new FloatField
  val epsilon = new FloatField
  val lambda = new FloatField
  
  alpha.tooltip = "Typical values: 0.1 - 0.2 for static learn rate / 0.8 - 1.0 for dynamic learn rate"
  gamma.tooltip = "Typical value: 0.8"
  epsilon.tooltip = "Typical values: 0.001 - 0.05"
  lambda.tooltip = "Not used yet"
  
  val dynamicAlpha = new CheckBox
  val e_2 = new NumberField(10)
  e_2.tooltip = "<html>Reasonable value for 10M total episodes: 200 000. Leads to &alpha;(1M) = 0.3 &alpha;<sub>0</sub>  and &alpha;(10M) ~ 0</html>"
  
  val winReward = new FloatField
  val tieReward = new FloatField
  val lossReward = new FloatField
  
  val agentDir = new TextField(10) //TODO use a file dialog
  agentDir.tooltip = "Location, where agents' experience should be saved to/loaded from. Restart application to load agents from new location"
  
  contents = new BoxPanel(Orientation.Vertical) {       
    contents += new Label("Configuration") { font = font.deriveFont(18f) }.flow
    
    contents += new ConfigGrid(12) {
      entry("Batch run?" -> new FlowPanel { contents ++= List(isBatchRun, new ActionButton("configure", configureBatchWindow)) })
      entry("<html>Initial learn rate &alpha;<sub>0</sub></html>" -> alpha.flow)
      entry("Dynamic alpha?" -> new FlowPanel { contents ++= List(dynamicAlpha, new ActionButton("?", alphaHelp)) })
      entry("<html>Halftime episode e<sub>2</sub>&nbsp;</html>" -> e_2.flow)
      entry("<html>Decay rate &gamma;</html>" -> gamma.flow)
      entry("<html>Exploration rate &epsilon;</html>" -> epsilon.flow)
      entry("<html>Trace decay &lambda;</html>" -> lambda.flow)
      entry("Rewards:" -> new Label(""))
      entry("Win reward: " -> winReward.flow)
      entry("Tie reward: " -> tieReward.flow)
      entry("Loss reward: " -> lossReward.flow)
      entry("Agent load directory: " -> agentDir.flow)
    }
    
    contents += new FlowPanel {
      contents += new ActionButton("OK", returnResult)
      contents += new ActionButton("Abort", abort)
    }
  }
  
  /** Closing the config window equals an abort
   */
  override def closeOperation = abort
  
  /** Fullfill the promise and then close the window
   */
  def returnResult {                
    p success Some(ConfigurationData(isBatchRun.selected, alpha.value, 
                                     dynamicAlpha.selected, e_2.value, gamma.value, epsilon.value, lambda.value, 
                                     RewardConfig(winReward.value, tieReward.value, lossReward.value), agentDir.text))
    Swing.onEDT { this.close }  
  }
  
  
  def alphaHelp {
    (new AlphaInfoWindow).open
  }
  
  
  def configureBatchWindow {
    logDebug("Opening batch configuration window")
    BatchConfigWindow.openWait
  }
  
  /** This method will preset the fields according to the configuration data which has been read and then
   *  open the window itself. In contrast to the default open method this one doesn't have to be calld inside the swing worker thread
   */
  override def open {
    isBatchRun.selected = data.batchRun
        
    alpha.value = data.alpha
    dynamicAlpha.selected = data.dynamicAlpha
    e_2.value = data.e_2
    gamma.value = data.gamma
    epsilon.value = data.epsilon
    lambda.value = data.lambda
    
    winReward.value = data.rewards.won
    tieReward.value = data.rewards.tied
    lossReward.value = data.rewards.lost
    
    agentDir.text = data.agentDir
    
    Swing.onEDT {
      super.open
    }
  }
  
  def abort {
    log.info("Aborted configuration")
    p success None
    Swing.onEDT { this.close }
  }
  
  
  class IllegalConfigurationException(msg:String) extends Exception(msg)
}

object ConfigWindow {
  
  /** Call this method to create a new configuration window, open it and get a future
   *  containing the changed configuration values.
   *  
   *  @param config the current configuration
   *  
   *  @return a future containing the changed configuration
   */
  def open(config:ConfigurationData):Future[Option[ConfigurationData]] = {
    val cfgWindow = new ConfigWindow(config)
    cfgWindow.open
    cfgWindow.future
  }
  
  /** Blocking alternative to above call
   */
  def openWait(config:ConfigurationData):Option[ConfigurationData] = Await.result(open(config), Duration.Inf)
}