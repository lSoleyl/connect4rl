package arch2.data.experience.base

import scala.collection.mutable.Map
import arch2.base.predicates._


/** Methods, which are independent of the type arguments are defined in this base trait
 */
trait ExperienceBase {
  /** This optional method should return the number of states, the agent has learned.
   */
  def stateCount:Option[Int] = None
  
  /** This method lets the experience define a custom action which can be issued from the agent list window
   */
  def customAction { }
}

/** The actual base trait for all experience implementations.
 *  This interface contains all methods, which are used by any agent, because this is the common interface, all agents can use.
 *  
 *  To ease the development of new Experience classes, all methods have dummy implementations, which throw an Exception if they are called.
 */
trait Experience[S,A,V] extends ExperienceBase {
  type State = S
  type Action = A
  type Value = V
  
  case class SA(s:State,a:Action)
  case class Entry(sa:SA, v:Value)
  case class ExtractorMatch[X](matchResult:X, state:State)
  
  object Entry {
    def apply(s:State, a:Action, v:Value) = new Entry(SA(s,a),v)
  }
  
  /** The default Q-Value for newly initialized entries. (When a new state is entered)
   */
  val default:Value
  
  /** This is used by all agents to determine, which action to use in a certain state.
   *  The experience should look up the Q-Values associated with the allowedActions in the given state
   *  and return the best(according to isBetter) Entry(state,action,value)
   *  
   *  @param state the state, in which the agent currently is
   *  @param allowedActions a list of available actions in the current state
   *  @param isBetter(a,b) a function, which returns true if a is better than b
   *    
   *  @return an Q-Entry, which represents the best action in the given state along with it's Q-Value
   */
  def bestActionEntry(state:State, allowedActions:List[Action], isBetter:(Value,Value)=>Boolean):Entry = ???
  
  
  /** Used by PredicateUnifier and PredicateMatcher traits to find the state which
   *  unifies with or matches the a given current state. The experience should iterate all it's 
   *  states and apply the matcher to each state until it returns the first result, which is != None.
   *  This result should be packed into an ExtractorMatch-Object together with the matching state and returned
   *  as result.
   *  
   *  @param matcher a function which takes a state and returns an optional matchresult of generic type.
   *  
   *  @return An extractor match or None if no state matched
   */
  def extractFirstMatch[X](matcher:State=>Option[X]):Option[ExtractorMatch[X]] = ???
  
  
  /** Is used by some algorithms to check whether a given state has already been entered into the experience.
   *  
   *  @param state the state to check
   */
  def containsState(state:State):Boolean = ???
  
  /** Enter a new state into the experience data and initialize all actions with the default value.
   *  Will be called by an agent if he encounteres a new state, which isn't aready part of the experience.
   *  
   *  @param state the state to enter
   *  @param actions a list of possible actions in that state
   */
  def enterState(state:State, actions:List[Action]):Unit = ???
  
  /** This method is used by most agents to look up the Q-Value to a (State,Action) tuple.
   *  If the state action tuple is not known, it should return the default value.
   *  
   *  @param key the state action tuple
   */
  def apply(key:SA):Value = ???
  
  /** This method is used by most agents to change the Q-Value of a (State,Action) tuple.
   *  If the state action tuple is not known, it should create a new entry with the given value. 
   *  
   *  @param key the state action tuple
   *  @param value the new Q-Value for the state action tuple
   */
  def update(key:SA, value:Value):Unit = ??? 
  
  class MissingStateException(state:State) extends Exception { override def toString = "MissingStateException(" + state + ")" }
}

/** This trait implements the experiece trait, using a simple Map from state action tuple to Q-Value.
 *  This is used by QLearningAI.
 */
trait MapExperience[S,A,V] extends Experience[S,A,V] {
  protected val experience:Map[SA,V] = Map()  
    
  override def bestActionEntry(state:State, allowedActions:List[Action], isBetter:(Value,Value)=>Boolean):Entry = {
    allowedActions.map(action => (Entry(state, action, apply(SA(state,action))))).reduce((entry1,entry2) => if (isBetter(entry1.v,entry2.v)) entry1 else entry2)
  }
  
  override def enterState(state:State, actions:List[Action]) { actions.foreach(action => experience(SA(state,action)) = default) }
  
  override def apply(key:SA):Value = experience.getOrElse(key, default)
  override def update(key:SA, value:Value) { experience(key) = value }
}


