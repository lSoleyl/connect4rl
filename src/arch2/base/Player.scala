package arch2.base

import arch2.field._
import arch2.data.AILoader

trait Player {  
  /** The main method to select the best action out of a list of actions.
   *  Every call to this method must fullfill following assumptions:
   *    * the action list is never empty
   *    * all supplied actions are valid actions
   *    
   *  Then the method is bound to return an action from this list
   *  
   *  @param state the current state the agent is in
   *  @param actions the list of possible actions in the current state
   *  @param color the color of the stones the agent is playing
   *  
   *  @return the selected action
   */
  def turn(state:FieldState, actions:List[Int], color:Stone):Int
  
  /** A hook method to react to the enemy's moves
   *  
   *  @param state the state of the enemy players
   *  @param actions the list of available actions
   *  @param selectedAction the chosen action
   */
  def enemyTurn(state:FieldState, actions:List[Int], selectedAction:Int) {}
  
  //Informative methods which are called upon win,loss and draw
  def won {}
  def lost {}
  def tied {}
  
  /** This method will be called whenever a new game starts.
   *  It can be used to configure the AI or the game itself
   *  
   *  @param config a sequence of configuration objects for the upcomming game
   */
  def newGame(config:Seq[Any]) {}
  
  /** Override this method to set a loader, from which the player/agent got loaded.
   *  This function makes the back reference to the loader from the agent.
   *  
   *  @return the AILoader which was used to load/create this agent
   */
  val loader:AILoader
  
  override def toString = this.getClass().getSimpleName()
  
  /** This method handles retrieving a describing string description dependent of the second player.
   *  If both have equal toStrings then their loader names should be used.
   * 
   * @param p2 the other player to get the name for
   * 
   * @return List(s1,s2) a List with two names
   */
  def uniqueNames(p2:Player) = {
    if (loader.displayName != p2.loader.displayName)
      List(loader.displayName, p2.loader.displayName)
    else
      List(this.toString, p2.toString)
  }
  
  private val id = Player.getID
  
  override def equals(o:Any):Boolean = {
    if (!o.isInstanceOf[Player])
      false
    else {
      o.asInstanceOf[Player].id == id
    }
  }
}

object Player {
  private var id:Long = 0
  
  //Give every player a unique id
  private def getID = {
    id += 1
    id
  }
}