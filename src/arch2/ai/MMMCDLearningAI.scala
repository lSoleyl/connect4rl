package arch2.ai

import arch2.data.experience._
import arch2.data._
import arch2.base.StringState
import arch2.algorithm.MinMaxMC
import arch2.field.FieldState

/** MinMaxMC LearningAI who also learns form the opponents moves
 */
case class MMMCDLearningAI(experience:StringExperience with HeaderV1, l:AILoader) extends AgentAI(new MinMaxMC(experience), l) with StringState {
  val mmmc = agent.asInstanceOf[MinMaxMC[String,Int]]
  
  override def enemyTurn(field:FieldState, actions:List[Int], selectedAction:Int) {
    val state = convertState(field)
    mmmc.enemyTurn(state, selectedAction, actions)
  }
  
  override def enemyFinalReward(reward:Double) {
    mmmc.enemyFinalReward(reward)
  }
} 


object MMMCDLearningAI {
  val Loader = GenericAILoader("MMMCDLearningAI", new StringExperience with HeaderV1, MMMCDLearningAI.apply)
}