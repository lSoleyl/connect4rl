package utils

object Percentage {
  implicit class PercentageInt(i:Int) {
    def of(n:Int):String = if (n != 0) f"${(i/n.toDouble)*100}%5.1f%%" else f"${0}%5.1f%%"
  }
}