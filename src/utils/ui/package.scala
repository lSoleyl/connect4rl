package utils

import scala.swing.Component

package object ui {
  implicit def richPanel(c:Component) = PanelWrapper.PanelWrapper(c)
  implicit def richCB[A](cb:scala.swing.ComboBox[A]) = ComboBox.RichComboBox(cb)
}