package utils

import scala.util.{Try, Success, Failure}

object Options {
  implicit class RichOption[A](opt:Option[A]) {
    def or(other: =>Option[A]):Option[A] = if (!opt.isEmpty) opt else other
    def default(default: =>A):A = if (!opt.isEmpty) opt.get else default 
    def mapOr[B](default: =>B)(mapping: A=>B):B = if (!opt.isEmpty) mapping(opt.get) else default
    def as[B]:Option[B] = {
      if (opt.isEmpty) 
        None 
      else
        Try { opt.get.asInstanceOf[B] } match {
          case Success(res) => Some(res)
          case Failure(_) => None
        }
    }
    def iff(action:A=>Unit):Unit = if (!opt.isEmpty) action(opt.get)
  }
  
  implicit def optionToBool[A](opt:Option[A]):Boolean = !opt.isEmpty
  
  implicit class OptionConverter[A](any:A) {
    def someIf(predicate: A=>Boolean):Option[A] = if (predicate(any)) Some(any) else None
    def noneIf(predicate: A=>Boolean):Option[A] = if (predicate(any)) None else Some(any)
  }
  
  implicit class Try2Option[A](obj:Try[A]) {
    def toOption:Option[A] = obj.map(Some(_)).getOrElse(None)
  }
  
  def max[T](a:Option[T], b:Option[T])(implicit order:Ordering[T]):Option[T] = {
    if ((a or b) == None) {
      None //No options contains a value
    } else if (a && b) {
      if (order.gteq(a.get, b.get)) a else b //Get greatest of both options
    } else {
      a or b //Get option, which is et
    }
  }
}