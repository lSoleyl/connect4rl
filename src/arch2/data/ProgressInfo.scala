package arch2.data

/** Base trait for displaying a progressbar while loading or writing
 *  big data.
 *  
 *  This can be either used with total increments/updates of the progress bar using 
 *  step() and update() or by initially setting the total amount of updates with setTotal()
 *  and then register each update with next() 
 */
trait ProgressInfo {
  var p = 0
  private var stepSize:Int = 0
  private var current:Int = 0
  private var total:Int = 0
  def currentCount = current
  def totalCount = total
  
  /** Set the total amount of steps and reset the progress bar
   */
  def setTotal(total:Int) {
    this.total = total
    stepSize = Math.max(total / 100, 1)
    current = 0
    p = 0
  }
  
  /** Move progressbar by exactly one step and redisplay if necessary
   */
  def next {
    current += 1
    if (current % stepSize == 0)
      step()
  }
  
  /** Move progressbar by N steps and redisplay it
   *  
   *  @param steps how many steps to add to current 
   */
  def nextN(steps:Int) {
    current += steps
    p = Math.min(current / stepSize, 100)
    display(p)
  }
  
  /** Move progressbar by N percent and redisplay the progress bar
   *  
   *  @param i how many percent should the progress bar move
   */
  def step(i:Int = 1) { 
    if (p < 100) 
      p = Math.min(p+i, 100)
    display(p) 
  }
  
  /** Display the given percent value
   * 
   * @param percent the value to display [0-100]
   */
  def update(percent:Int) { 
    if (percent >= 0 && percent <= 100) 
      p = percent
    display(p)
  }
  
  /** Abstract funciton to actually display the progress bar
   *  
   * @param percent the value to display
   */
  def display(percent:Int):Unit
}