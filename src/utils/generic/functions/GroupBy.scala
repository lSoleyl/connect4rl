package utils.generic.functions

trait GroupBy[M[_]] {
  def apply[A,B](ma:M[A], selector:A=>B):scala.collection.Map[B,M[A]]
}


object GroupBy {
  implicit object ListGroupBy extends GroupBy[List] {
    override def apply[A,B](list:List[A], selector:A=>B) = list.groupBy(selector)
  }
}