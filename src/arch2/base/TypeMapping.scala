package arch2.base

import arch2.base.predicates._

/** A construct for the scala compiler to deduce the generic
 *  parameters of AbstractAgentAI correctly without explicitly specifying them 
 */
sealed trait TypeMapping[CS,CA,AS,AA,V]

object TypeMapping {
  def identity[S,A,V](agent:Agent[S,A,V]) = new TypeMapping[S,A,S,A,V]{}
  
  val simplePredicateState = new TypeMapping[BoundPredicateState[Int], Int, UnboundPredicateState[Int], Variable[Int],Double]{}
}