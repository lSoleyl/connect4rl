package arch2.field

/** A very simple and fast implementation of a field for 4 wins.
 *  To make it useable it needs at least the FieldIndexMapper
 *  
 *  @remark it turned out that this implementation is about 14% slower than
 *          a field, using a 2D-Array without an index mapper.
 */
class PlainField(val columns:Int, val rows:Int) extends Field1DRW {
  val fields = columns*rows
  private val data = Array.fill[Stone](fields)(Stone.EMPTY)

  /** Method to access the data in the field
   */
  def apply(pos:Int):Stone = data(pos)
  
  
  /** Setter for the field
   */
  def update(pos:Int, color:Stone) { data(pos) = color }

  
  /** Clears the whole Field and sets every value to EMPTY
   */
  def clear { for(i <- 0 until fields) data(i) = Stone.EMPTY }
}