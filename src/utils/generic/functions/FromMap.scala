package utils.generic.functions

trait FromMap[M[_]] {
  def apply[A,B](map:scala.collection.Map[A,B]):M[(A,B)]
}

object FromMap {
  implicit object ListFromMap extends FromMap[List] {
    override def apply[A,B](map:scala.collection.Map[A,B]) = map.toList 
  }
}