package arch2.field.sequences

import arch2.field.Stone
import arch2.field.Field2DR

/** Column like view onto the field, used for string prepresentation of 
 *  the field's state. This class is inspired by vg.base.sequences.Row[A], but 
 *  is tailored towards the new field classes
 */
case class Column(field:Field2DR, index:Int)  extends Seq[Stone] {
  if (index >= field.columns || index < 0)
    throw new IllegalArgumentException(s"Can't instantiate a column at index $index for a field with ${field.columns} columns!")
  
  def apply(i:Int):Stone = field(index,i)
  def iterator:Iterator[Stone] = new ColumnIterator(this)
  def length = field.rows
  
  override def toString = "Column("+ mkString(",") + ")"
}

class ColumnIterator(col:Column) extends Iterator[Stone] {
   var i = 0
   def hasNext = i < col.length
   def next = {
     i += 1
     col(i-1)
   }
}