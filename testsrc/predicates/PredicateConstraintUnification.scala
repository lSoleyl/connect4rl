package predicates
import org.scalatest._
import arch2.base.predicates._
import utils.Options._
import data.Predicates
import data.Variables
import helper.Binding

class PredicatePredicateUnification extends FlatSpec with Matchers {
  import Variables._
  import Predicates._
  
  
  
  object ub {
    def unbound = new UnboundPredicateState(A(X) :: A(Y) :: A(Z) :: Nil)
    val C_104 = unbound.constrain(X, Constraint(-1, Y)).constrain(X, Constraint(3, Z)) // Should only unify with 104, 215, 326, ... (and other orders)
    
    //State in which one variable is set by multiple constraints should only unify like C_104
    val doubleConstrained =   unbound.constrain(X, Constraint(-1, Y)).   //Y = X - 1 
                                      constrain(X, Constraint( 3, Z)).   //Z = X + 3
                                      constrain(Y, Constraint( 4, Z))    //Z = Y + 4
                                      
    //A state which has conflicting constraints and should not unifiy with any other state
    // Test with 104 and 105
    val doubleConstrained_conflict = unbound.constrain(X, Constraint(-1, Y)). //Y = X - 1
                                             constrain(X, Constraint( 3, Z)). //Z = X + 3
                                             constrain(Y, Constraint( 5, Z))  //Z = Y + 5 => X + 4

    //A state with transitive constraints... should also only unify with the same states as C_104
    //Test with 107 and 115 to check whether the transitivity is completely applied
    val transitiveConstraint = unbound.constrain(X, Constraint(-1, Y)) //Y = X - 1
                                      .constrain(Y, Constraint( 4, Z)) //Z = Y + 4 => X + 3                          
    //TODO add test with cyclic constraints 
  }
  
  object b {
    def bound(x:Int,y:Int,z:Int) = A(x) :: A(y) :: A(z) :: Nil
    
    val AAA_104 = bound(1,0,4)
    val AAA_215 = bound(2,1,5)
    val AAA_204 = bound(2,0,4)
    val AAA_114 = bound(1,1,4)
    val AAA_410 = bound(4,1,0) //like 104 but reversed order
    
    val AAA_107 = bound(1,0,7)
    val AAA_115 = bound(1,1,5)
    
    val EMPTY = Nil
  }
  
  
  "Y = X - 1, Z = X - 3 " should "unify with 104 and 215" in {
    Binding(ub.C_104.unifyWith(b.AAA_104)).possible should be (true)
    Binding(ub.C_104.unifyWith(b.AAA_215)).possible should be (true)
  }
  
  it should "bind X=1 Y=0 Z=4 when unifying with 104" in {
    val binding = Binding(ub.C_104.unifyWith(b.AAA_104))
    binding(X) should be (Some(1))
    binding(Y) should be (Some(0))
    binding(Z) should be (Some(4))
  }
  
  //Prediates are ordered differently
  it should "also unify with 410" in {
    Binding(ub.C_104.unifyWith(b.AAA_410)).possible should be (true)
  }
  
  it should "still bind X=1 Y=0 Z=4 when unifying with 410" in {
    val binding = Binding(ub.C_104.unifyWith(b.AAA_410))
    binding(X) should be (Some(1))
    binding(Y) should be (Some(0))
    binding(Z) should be (Some(4))
  } 
  
  //Negative tests
  it should "not unify with an empty state" in {
    Binding(ub.C_104.unifyWith(b.EMPTY)).possible should be (false)
  }
  
  it should "not unify with states 204, 114, 107 and 115" in {
    Binding(ub.C_104.unifyWith(b.AAA_204)).possible should be (false)
    Binding(ub.C_104.unifyWith(b.AAA_114)).possible should be (false)
    Binding(ub.C_104.unifyWith(b.AAA_107)).possible should be (false)
    Binding(ub.C_104.unifyWith(b.AAA_115)).possible should be (false)
  }
  
  
  //Checking double constrained state
  "Y = X - 1, Z = X + 3, Z = Y + 4" should "unify with 104 and 215" in {
    Binding(ub.doubleConstrained.unifyWith(b.AAA_104)).possible should be (true)
    Binding(ub.doubleConstrained.unifyWith(b.AAA_215)).possible
  } 
  
  it should "bind X=1 Y=0 Z=4 when unifying with 104" in {
    val binding = Binding(ub.doubleConstrained.unifyWith(b.AAA_104))
    binding(X) should be (Some(1))
    binding(Y) should be (Some(0))
    binding(Z) should be (Some(4))    
  }
  
  it should "not bind with an empty state" in {
    Binding(ub.doubleConstrained.unifyWith(b.EMPTY)).possible should be (false)
  }
  
  it should "not bind with states 204, 114, 107 and 115" in {
    Binding(ub.doubleConstrained.unifyWith(b.AAA_204)).possible should be (false)
    Binding(ub.doubleConstrained.unifyWith(b.AAA_114)).possible should be (false)
    Binding(ub.doubleConstrained.unifyWith(b.AAA_107)).possible should be (false)
    Binding(ub.doubleConstrained.unifyWith(b.AAA_115)).possible should be (false)
  }
  
  it should "unify with 410" in {
    Binding(ub.doubleConstrained.unifyWith(b.AAA_410)).possible should be (true)
  }
  
  it should "bind X=1 Y=0 Z=4 when unifying with 410" in {
    val binding = Binding(ub.doubleConstrained.unifyWith(b.AAA_410))
    binding(X) should be (Some(1))
    binding(Y) should be (Some(0))
    binding(Z) should be (Some(4))
  }
  
  
  //Checking conflicting constraints
  "Y = X - 1, Z = X + 3, Z = Y + 5" should "not unify with anything (conflict)" in {
    Binding(ub.doubleConstrained_conflict.unifyWith(b.EMPTY)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_104)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_215)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_204)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_114)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_410)).possible should be (false)
    
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_107)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_115)).possible should be (false)
  }
  
  
  //Check transitive constraints
  "Y = X - 1, Z = Y + 4" should "unify with 104 and 205" in {
    Binding(ub.transitiveConstraint.unifyWith(b.AAA_104)).possible should be (true)
    Binding(ub.transitiveConstraint.unifyWith(b.AAA_215)).possible should be (true)
  } 
  
  it should "not unify with an empty state" in {
    Binding(ub.transitiveConstraint.unifyWith(b.EMPTY)).possible should be (false)
  }
  
  it should "not unify with anything else" in {
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_204)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_114)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_410)).possible should be (false)
    
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_107)).possible should be (false)
    Binding(ub.doubleConstrained_conflict.unifyWith(b.AAA_115)).possible should be (false)
  }
  
}