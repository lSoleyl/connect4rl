package arch2.gui

import scala.swing._
import utils.Log
import java.io._
import utils.TryWithResources
import javax.swing.filechooser.FileNameExtensionFilter
import scala.concurrent.Promise
import scala.util.Success
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import utils.ui.PanelWrapper._
import utils.ColumnWriter
import utils.Options._
import utils.Table._

class ResultEditor extends Frame with Log {
  title = "Result editor"
  
  val autoMergeButton = new ActionButton("automatic multimerge", autoMerge) 
    
  contents = new BoxPanel(Orientation.Vertical) {
    contents += new Label("<html><h2>Result editor</h2></html>")
    contents += autoMergeButton
  }
  
  
  def autoMerge {
    //Open file dialog for multiple files
    val fileChooser = new FileChooser(new File(GameGUI.batchConfig.multi.outputDir)) {
      title = "Select data sets to merge"
      fileFilter = new FileNameExtensionFilter("Data", "txt")
      fileSelectionMode = FileChooser.SelectionMode.FilesOnly
      multiSelectionEnabled = true
      showOpenDialog(autoMergeButton)
    }
    
    val files = fileChooser.selectedFiles.toList.filter(_.exists)
    
    if (files.length < 2) {
      reportError("Select at least two files to merge together")
      return
    } 
    
    val datasets = files.map(ResultData.fromFile)
    
    //Check column name of first column
    val byFirstColumn = datasets.groupBy(_.columnNames.head)
    if (byFirstColumn.keys.size != 1) {
      reportError("Selected files use different names for first column: " + byFirstColumn.keys.mkString(", "))
      return
    }
    
    //Check equality of first column
    val episodeList = datasets.head(0) //First column of fist dataset
    if (!datasets.forall(dataset => dataset(0) == episodeList)) {
      val wrongDatasets = datasets.filter(x => x(0) != episodeList).map(_.source)
      reportError("The first column of following files isn't equal to that of the first file: " + wrongDatasets.mkString(", "))
      return
    }
    

    //Check whether the data contains at least one unique agent per dataset
    val agentSets = datasets.map(_.agents)
    val commonAgents = agentSets.reduce((a,b) => a.intersect(b))
    logDebug(s"common agent set: $commonAgents")

    if (datasets.forall(_.agents.diff(commonAgents).isEmpty)) { //Only do something if all datasets have the same agent names
      val results = resolveAmbiguity(datasets) match {
        case None => None //User just closed window
        case Some((agent, true)) => renameAgent(datasets,agent)
        case Some((agent, false)) => Some(averageOnAgent(datasets,agent))
      }
     
      results iff { writeResultData }
    } else {
      writeResultData(mergeDatasets(datasets))  
    }
  }
  
  def writeResultData(result:ResultData) {
    //Get Filename:
    val filename = Dialog.showInput(message="Name of output file: ", title="Filename", initial="merged.txt")
    if (filename.isEmpty) {
      return
    }
    val filepath = GameGUI.batchConfig.multi.outputDir + File.separator + filename.get
    
    result.writeToFile(new File(filepath))
    log.info("Written merged result to "+ filepath)
  }
  
  
  def reportError(msg:String) {
    Swing.onEDT {
      Dialog.showMessage(message = msg, title = "error", messageType = Dialog.Message.Error)
    }
  }
  
  
  /** Will be called if all agents of the selected datasets have equal names and we have
   *  to determine a unique agent per dataset.
   *  
   *  @return (agent,true) if agent should be renamed (agent,false) if agent should be averaged, None if user closed window 
   */
  private def resolveAmbiguity(datasets:List[ResultData]) = {
    
    val agentNames = datasets.head.agents.toList
    
    val selectionWindow = new AgentSelectionDialog(agentNames)
    selectionWindow.open
    Await.result(selectionWindow.result, Duration.Inf)
  }
  
  private def averageOnAgent(datasets:List[ResultData], agent:String):ResultData = {
    val result = new ResultData("")
    result.appendColumn(datasets.head.columnNames.head, datasets.head(0)) //Copy first column from first dataset
    
    val attributes = datasets.head.attributes //List of attributes on which to average

    //Parse all values as double (since we are averaging, it's ok)
    val q = datasets.length.toDouble
    for(attribute <- attributes) {
      val valueTable = datasets.map(data => data(agent + "." + attribute))
      val averagedColumn = valueTable.foldLeftOuter(0d)((acc,v) => acc + (v.toDouble/q)) //We are averaging over the outer list
      val stringColumn = averagedColumn.map(d => String.format(java.util.Locale.US, "%4.3f", d.asInstanceOf[Object]))
      result.appendColumn(agent + "." + attribute, stringColumn)
    }
    
    result
  }
  
  
  private def mergeDatasets(datasets:List[ResultData]):ResultData = {
    val commonAgents = datasets.map(_.agents).reduce((a,b) => a.intersect(b)) //get new common agents set
    val result = new ResultData("")
    result.appendColumn(datasets.head.columnNames.head, datasets.head(0)) //Copy first column from first dataset
    for (dataset <- datasets) { commonAgents.foreach(dataset.deleteAgent) } //Remove all agents from the common agent set from all datasets
    
    //Fetch attributes and copy columns attribute wise into result dataset in the same order as in the first dataset
    for (dataset <- datasets) {
      val columns = dataset.columnNames.tail
      columns.foreach(column => result.appendColumn(column, dataset(column))) //Copy columns from all datasets into resultset
    }
    
    result
  }
  
  
  private def renameAgent(datasets:List[ResultData], agent:String):Option[ResultData] = {
    val dialog = new RenameDialog(datasets, agent)
    dialog.open
    
    if (!Await.result(dialog.result, Duration.Inf)) { //Just return result from dialog
      None
    } else {
      Some(mergeDatasets(datasets)) //Now we can merge the data
    }
  }
  
  
  class RenameDialog(datasets:List[ResultData], agent:String) extends Frame {
    title = "Rename agents"
      
    val data:List[(String, ResultData, TextField)] = datasets.map(data => (data.source, data, new TextField(agent, 20)))
    
    val pSuccess:Promise[Boolean] = Promise() 
    
    contents = new BoxPanel(Orientation.Vertical) {
      contents += new ConfigGrid(data.length) {
        data.foreach(t => entry(t._1 -> t._3))
      }
      
      contents += new ActionButton("Rename", rename)
    }
    
    def result = pSuccess.future
    
    private def rename {
      for((_, dataset, field) <- data) {
        dataset.renameAgent(agent, field.text)
      }

      pSuccess.complete(Success(true))
      Swing.onEDT { this.close }
    }
    
    override def closeOperation {
      pSuccess.complete(Success(false))
    }
  }
  
  //A dialog for selecting an agent from a list of agents
  class AgentSelectionDialog(agents:List[String]) extends Frame {
    title = "Rename agent"
 
    private val list = new ListView(agents) {  
      selection.intervalMode = ListView.IntervalMode.Single
      selectIndices(0) //Preselect first entry, so that one element is always selected      
    }
                                                //v- true = rename / false = average
    private val pResult:Promise[Option[(String,Boolean)]] = Promise()
    
    def result = pResult.future
    
    contents = new BoxPanel(Orientation.Vertical) {
      contents += new Label("All datasets share equal agent names, rename one to continue merging")
      contents += list.flow
      contents += new FlowPanel {
        contents += new ActionButton("rename", rename)
        contents += new ActionButton("average", average)
      }
    }
    
    private def average {
      pResult.complete(Success(Some((list.selection.items.head, false))))
      Swing.onEDT { this.close }
    }
    
    private def rename {
      pResult.complete(Success(Some((list.selection.items.head, true))))
      Swing.onEDT { this.close }
    }
    
    override def closeOperation {
      pResult.complete(Success(None))
    }
  }
}




class ResultData(val source:String) {
  import scala.collection.mutable._
  
  var columnNames:List[String] = Nil 
  val columns:Map[String,List[String]] = Map()
  
  def apply(name:String) = columns(name)
  def apply(i:Int) = columns(columnNames(i))
  def columnCount = columnNames.length
  
  def appendColumn(name:String, entries:List[String]) {
    if (columnNames.contains(name)) {
      deleteColumn(name)
    }
    columnNames = columnNames ++ List(name)
    columns(name) = entries
  }
  
  def deleteColumn(name:String) {
    columnNames = columnNames.filter(_ != name)
    columns.remove(name)
  }
  
  def deleteAgent(name:String) {
    val part = columnNames.partition(_.startsWith(name + ".")) //Find full column names
    part._1.foreach(columns.remove) //Remove the columns
    columnNames = part._2 
  }
  
  def renameAgent(fromAgent:String, toAgent:String) {
    val renames = columnNames.filter(_.startsWith(fromAgent + ".")).map(name => name -> name.replaceFirst(fromAgent, toAgent))
    
    for((from, to) <- renames) {
      columns(to) = columns(from)
      columns.remove(from)
      columnNames = columnNames.map(name => if (name == from) to else name) //Preserve column order
    }
  }
                                            //v- Split interprets the parameter as Regex
  def agents = columnNames.tail.map(_.split("\\.")(0)).toSet
  def attributes = columnNames.tail.map(_.split("\\.")(1)).toSet.toList
  override def toString = source
  
  
  def writeToFile(f:File) {
    val encoding = if (java.nio.charset.Charset.isSupported("Windows-1252")) "Windows-1252" else "UTF-8"    
    
    TryWithResources(new ColumnWriter(new FileOutputStream(f), ColumnWriter.Options(encoding = encoding))) { writer =>
      for(column <- columnNames) {
        writer.addColumn(column :: columns(column)) //Prepend column header to each dataset
      } 
    }.get
  }
}


object ResultData {
  def fromFile(file:File):ResultData = {
    import utils.io.ReadLines._
    import utils.Validation._
    
    val source = file.getName()
    val encoding = if (java.nio.charset.Charset.isSupported("Windows-1252")) "Windows-1252" else "UTF-8"
    
    TryWithResources(new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding))) { reader =>
      val data = new ResultData(source)
      
      val columnNames = reader.readLine().split(" ").validate(_.tail.forall(_.contains(".")), "Old result data format not supported. Invalid column headers: " + _.toList)
      val columns:Array[List[String]] = Array.fill(columnNames.length)(Nil)
            
      for(line <- reader.lineStream) { //Read columns linewise
        val parts = line.split(" ")
        
        if (parts.length > 0) {
          (0 until parts.length).foreach(c => columns(c) = parts(c) :: columns(c))
        }
      }
      
      //Write content into result object
      for(i <- 0 until columnNames.length) {          //v--- have been read in reverse order
        data.appendColumn(columnNames(i), columns(i).reverse)
      }
      
      data
    }.get
  }
}