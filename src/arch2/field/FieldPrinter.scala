package arch2.field

trait FieldPrinter extends Field2DRW {
  var step = 1
  var texField:List[String] = Nil
  
  FieldPrinter(this)
  
  abstract override def update(x:Int, y:Int, c:Stone) { 
    val color = if (c == Stone.RED) "red" else "yellow"
    texField = s"\\stoneNr{$color}{${x+1}}{${y+1}}{$step}" :: texField
    step += 1
    
    super.update(x,y,c)
  }
  
  abstract override def clear {
    printState
    step = 1
    texField = Nil
    
    super.clear
  }
  
  private def printState {
    val result = texField.reverse.mkString("\\begin{board}\n", "\n", "\n\\end{board}") 
    println("Printing board layout...")
    println(result)
    println("done.")
  }
}

object FieldPrinter {
  var lastField:FieldPrinter = null
  def apply(p:FieldPrinter) {
    if (lastField != null)
      lastField.printState
    lastField = p
  }
}