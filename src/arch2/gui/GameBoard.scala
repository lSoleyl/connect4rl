package arch2.gui

import scala.swing._
import scala.concurrent.Future
import arch2.field._

class GameBoard(field:FieldState, titleString:String) extends Frame {
  title = titleString
  val gamePanel = new FieldPanel(field)
  lazy val buttonPanel = new ButtonPanel(field.columns, this)
  var hasButtons = false
  var destroyed = false //This state variables is set to true, if the gameboard has been closed unexpectedly
  
  contents = gamePanel
  
  /** This method will be called by the GUIPlayers to redraw the
   *  field whenever something changes.
   */
  def drawField {
    Swing.onEDT(gamePanel.repaint)
  }
  
  /** Add buttons to gamefield if they have been requested
   *  by a human player
   */
  def requireButtons {
    if (!hasButtons) {
      hasButtons = true
      Swing.onEDT {
        contents = new BoxPanel(Orientation.Vertical) {
          contents += buttonPanel
          contents += gamePanel
        }
      }
    } 
  }
  
  def draw {
    Swing.onEDT {
      this.open
      this.centerOnScreen
    }
  }
  
  override def closeOperation {
    destroyed = true //Notify GUI-Players that game has been aborted beforehand
    buttonPanel.cancelSelection
  }
}