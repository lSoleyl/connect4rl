package arch2.game

import arch2.base._
import utils.Log
import utils.Options._
import spray.json._

/** This class provides variables and methods to count game statistics for a batch run
 *  without polluting the episode loop with mutable variables.
 */
class GameStatistics(val p1:Player, val p2:Player) extends Log {
  //Game outcomes
  var p1Won = 0
  var p2Won = 0
  var draw = 0
  
  //Total amount of turns in a game
  var p1WonTurns = 0  
  var p2WonTurns = 0
  var drawTurns = 0
  
  //Data regarding initial player
  var p1WonAsStarter = 0
  var p2WonAsStarter = 0
  var p1DrawAsStarter = 0
  
  var p1States:Option[Int] = None
  var p2States:Option[Int] = None
  
  def totalEpisodes = p1Won + p2Won + draw
  
  /** This method updates the internal stastical values according to the provided 
   *  game result of the last game.
   *  
   *  @param game the game result of the last game
   */
  def enter(game:GameResult) {
    game.winner match {
      case None => { //Game was a draw
        draw += 1
        drawTurns += game.turns
        if (game.startingPlayer == p1)
          p1DrawAsStarter += 1
      }
      
      case Some(player) => { //A player has won
        if (player eq p1) {  //Player 1 has won
          p1Won += 1
          p1WonTurns += game.turns
          if (game.startingPlayer == p1)
            p1WonAsStarter += 1
        } else {             //Player 2 has won
          p2Won += 1
          p2WonTurns += game.turns
          if (game.startingPlayer == p2)
            p2WonAsStarter += 1
        }
      }
    }
  }
  
  /** Returns the player information regarding the game
   * 
   * @param p the player to get the information for
   * 
   * @result a PlayerStatistics object which contains all relevant information about that game
   */
  def getFor(p:Player):PlayerStatistics = {
    val turnsp1 = if (p1Won != 0) p1WonTurns.asInstanceOf[Double]/p1Won else 0
    val turnsp2 = if (p2Won != 0) p2WonTurns.asInstanceOf[Double]/p2Won else 0
    val turnsd = if (draw != 0) drawTurns.asInstanceOf[Double]/draw else 0
    
    
    
    val episodes = p1Won + p2Won + draw
    
    p match {
      //    v- Backtick here to prevent a wildcard match to everything
      case `p1` => PlayerStatistics(p1, p1Won, draw, p2Won, p1WonAsStarter, p1DrawAsStarter, p2Won-p2WonAsStarter, turnsp1, turnsd, turnsp2, episodes, p1States)
      case `p2` => PlayerStatistics(p2, p2Won, draw, p1Won, p2WonAsStarter, draw-p1DrawAsStarter, p1Won-p1WonAsStarter, turnsp2, turnsd, turnsp1, episodes, p2States)
      
      case _ => log.error(s"Player $p wasn't part of this game!"); throw new IllegalArgumentException(s"$p wasn't part of this game!")
    }
  }
  
  
  def add(stats:GameStatistics) {
    if (!(p1 == stats.p1) || !(p2 == stats.p2 ))
      log.warn("Merging Gamestatistics from different players")
    
    
    p1Won += stats.p1Won
    p2Won += stats.p2Won
    draw += stats.draw
    
    p1WonTurns += stats.p1WonTurns
    p2WonTurns += stats.p2WonTurns
    drawTurns += stats.drawTurns
    
    p1WonAsStarter += stats.p1WonAsStarter
    p2WonAsStarter += stats.p2WonAsStarter
    p1DrawAsStarter += stats.p1DrawAsStarter
    
    p1States = max(p1States, stats.p1States)
    p2States = max(p2States, stats.p2States)
  }
  
  def toJSON:JsValue = {
    val optValue = (tpl:(String, Option[Int])) => if (tpl._2) Map(tpl._1 -> JsNumber(tpl._2.get)) else Map()
    
    JsObject(Map(
      "p1Won" -> JsNumber(p1Won),
      "p2Won" -> JsNumber(p2Won),
      "draw"  -> JsNumber(draw),
      "p1WonTurns" -> JsNumber(p1WonTurns),
      "p2WonTurns" -> JsNumber(p2WonTurns),
      "drawTurns"  -> JsNumber(drawTurns),
      "p1WonAsStarter" -> JsNumber(p1WonAsStarter),
      "p2WonAsStarter" -> JsNumber(p2WonAsStarter),
      "p1DrawAsStarter"-> JsNumber(p1DrawAsStarter))
      ++
      optValue("p1States" -> p1States)
      ++
      optValue("p2States" -> p2States)
    )
  }
  
  def serializer = GameStatistics.serializer(p1, p2)
}


object GameStatistics {
  import arch2.data.Serializer._
  
  def fromJSON(p1:Player, p2:Player, json:JsValue) = {
    val obj = json.asJsObject
    val member = (x:String) => obj.fields.get(x).as[JsNumber].get.value.toInt
    val memberOpt = (x:String) => obj.fields.get(x).as[JsNumber].map(_.value.toInt)
    
    val stats = new GameStatistics(p1,p2)
    
    stats.p1Won = member("p1Won")
    stats.p2Won = member("p2Won")
    stats.draw  = member("draw")
    
    stats.p1WonTurns = member("p1WonTurns")
    stats.p2WonTurns = member("p2WonTurns")
    stats.drawTurns = member("drawTurns")

    stats.p1WonAsStarter = member("p1WonAsStarter")
    stats.p2WonAsStarter = member("p2WonAsStarter")
    stats.p1DrawAsStarter = member("p1DrawAsStarter")
    
    stats.p1States = memberOpt("p1States")
    stats.p2States = memberOpt("p2States")
    
    stats
  }
  
  def serializer(p1:Player, p2:Player) = new arch2.data.Serializer[GameStatistics] {
    override def write(stats:Type)(implicit writer:ByteWriter) {
      import writer._
  
      writeInt(stats.p1Won)
      writeInt(stats.p2Won)
      writeInt(stats.draw)
      
      writeInt(stats.p1WonTurns)
      writeInt(stats.p2WonTurns)
      writeInt(stats.drawTurns)
      
      writeInt(stats.p1WonAsStarter)
      writeInt(stats.p2WonAsStarter)
      writeInt(stats.p1DrawAsStarter)
      
      serialize(stats.p1States)
      serialize(stats.p2States)
    }
    
    override def load()(implicit reader:ByteReader) = {
      import reader._
      
      val stats = new GameStatistics(p1,p2)
      stats.p1Won = readInt
      stats.p2Won = readInt
      stats.draw = readInt

      stats.p1WonTurns = readInt
      stats.p2WonTurns = readInt
      stats.drawTurns = readInt
      
      stats.p1WonAsStarter = readInt
      stats.p2WonAsStarter = readInt
      stats.p1DrawAsStarter = readInt
      
      stats.p1States = deserialize[Option[Int]]
      stats.p2States = deserialize[Option[Int]]
      
      stats
    }
  }
}