package arch2.data.experience

import java.io._
import arch2.data.ProgressInfo

/** A trait which adds support to directly load and save to files.
 *  Every class which supports StreamSerializable can be mixed in with this.
 */
trait FileSerializable extends StreamSerializable {
  def loadFromFile(file:File, progress:ProgressInfo) {
    super.loadFromStream(new FileInputStream(file), progress)
  }
  
  def writeToFile(file:File, progress:ProgressInfo) {
    super.writeToStream(new FileOutputStream(file), progress)
  }
  
  
  
  //This must already be implemented by the class this trait is mixed into
  abstract override def writeToStream(out:OutputStream, progress:ProgressInfo) = super.writeToStream(out, progress)
  abstract override def loadFromStream(in:InputStream, progress:ProgressInfo) = super.loadFromStream(in, progress)
}