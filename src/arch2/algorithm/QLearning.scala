package arch2.algorithm

import arch2.base.Agent
import arch2.data.experience.base.Experience
import arch2.config.LearningParameters
import arch2.data.experience.HeaderV1
import scala.util.Random

class QLearning[S,A](val experience:Experience[S,A,Double] with HeaderV1) extends Agent[S,A,Double] {  
  var alpha_0 = 0d
  var gamma = 0d
  var epsilon = 0d
  var dynamicAlpha = false
  var e_2 = 0
  var alpha = 0d
  
  var lastAction:Entry = null
  
  protected def isBetter(v1:Value, v2:Value) = v1 > v2
  
  def selectAction(state:S, reward:Double, possibleActions:List[A]):A = {
    val bestAction = bestActionEntry(state, possibleActions)
    
    if (lastAction != null) { //If this isn't the first state, then apply TD-Update
      val tdError = reward + gamma*bestAction.v - lastAction.v
      experience(lastAction.sa) = lastAction.v + alpha*tdError
    }
    
    lastAction = applyEpsilonGreedy(bestAction, possibleActions, epsilon)    
    lastAction.sa.a //Return the selected action
  }
  
  def finalReward(reward:Double) {
    experience(lastAction.sa) = lastAction.v + alpha*(reward - lastAction.v)
    
    lastAction = null
    experience.episodes += 1
    
    alpha = calcAlpha
  }
  
  /** Will be called once before every GUI game or before a batch run
   *  
   *  @param params the configuration which has been set
   */
  def initialize(params:LearningParameters) {
    alpha_0 = params.alpha
    gamma = params.gamma
    epsilon = params.epsilon
    dynamicAlpha = params.dynamicAlpha
    e_2 = params.e_2
    alpha = calcAlpha
  }
  
  private def calcAlpha = {
    if (dynamicAlpha)
      alpha_0 * Math.pow(0.5, experience.episodes/e_2.toDouble)
    else
      alpha_0
  }
}