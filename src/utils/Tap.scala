package utils

/** The Tap utility works similar to .tap in ruby.
 *  It can be called on every object and returns the object, it was called on.
 *  This can be used to do something on a return value, without actually modifying it (like logging something)
 *  
 *  This utility provides a special case for simple Tuples. To handle the tuple elements as seprate function
 *  arguments, or to only use on of the tuples values.
 */
object Tap {
  implicit class Tappable[T](content:T) {
    def tap(f: T =>Unit) = {
      f(content)
      content
    }
  }
  
  implicit class TappableTuple[A,B](content:(A,B)) {
    def ttap(f: (A,B) =>Unit) = {
      f(content._1, content._2)
      content
    }
    
    def ttap1(f: A =>Unit) = {
      f(content._1)
      content
    }
    
    def ttap2(f: B=>Unit) = {
      f(content._2)
      content
    }
  }
}