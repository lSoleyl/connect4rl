package arch2.ai

import arch2.data.experience._
import arch2.data._
import arch2.base.StringState
import arch2.algorithm.MinMaxMC

/** MinMaxMC LearningAI
 */
case class MMMCLearningAI(experience:StringExperience with HeaderV1, l:AILoader) extends AgentAI(new MinMaxMC(experience), l) with StringState 


object MMMCLearningAI {
  val Loader = GenericAILoader("MMMCLearningAI", new StringExperience with HeaderV1, MMMCLearningAI.apply)
}