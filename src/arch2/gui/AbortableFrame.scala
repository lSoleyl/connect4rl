package arch2.gui

import scala.swing.Frame
import arch2.game.Abortable

/** This method makes a frame into an abortable.
 *  An operation can thus be aborted by closing the frame.
 */
trait AbortableFrame extends Frame with Abortable {
  private var aborted = false
  
  override def isAborted = aborted
  
  override def closeOperation { aborted = true }
}