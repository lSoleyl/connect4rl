package utils.io

import java.io._
import utils.Byte._
import scala.Array.canBuildFrom

class ByteWriter(out:OutputStream) {
  val stream = out
  private val bytes = new Array[Byte](8)
  
  import ByteWriter._
  
  def writeInt(i:Int) {
    intBytes(i, bytes)
    out.write(bytes, 0, 4)
  }
  
  def writeLong(l:Long) {
    longBytes(l, bytes)
    out.write(bytes, 0, 8)
  }
  
  def writeString(s:String) {
    val b = s.toCharArray().map(_.asInstanceOf[Byte])
    writeInt(b.size)
    out.write(b)
  }
  
  def writeBool(b:Boolean) {
    writeByte(if (b) 1 else 0)
  }
  
  def writeByte(b:Byte) {
    bytes(0) = b
    out.write(bytes, 0, 1)
  }
  
  def writeDouble(d:Double) { //First converts the double into a long
    writeLong(java.lang.Double.doubleToRawLongBits(d))
  }
}

/** This companion object provides the basic conversion methods from datatypes to byte arrays
 */
object ByteWriter {
  def intBytes(i:Int, bytes:Array[Byte] = new Array[Byte](4)):Array[Byte] = {
    bytes(3) = i & 0xFF
    bytes(2) = (i >> 8) & 0xFF
    bytes(1) = (i >> 16) & 0xFF
    bytes(0) = (i >> 24) & 0xFF
    bytes
  }
  
  def longBytes(l:Long, bytes:Array[Byte] = new Array[Byte](8)):Array[Byte] = {
    bytes(7) = l & 0xFF
    bytes(6) = (l >> 8) & 0xFF
    bytes(5) = (l >> 16) & 0xFF
    bytes(4) = (l >> 24) & 0xFF
    bytes(3) = (l >> 32) & 0xFF
    bytes(2) = (l >> 40) & 0xFF
    bytes(1) = (l >> 48) & 0xFF
    bytes(0) = (l >> 56) & 0xFF
    bytes
  }
  
}