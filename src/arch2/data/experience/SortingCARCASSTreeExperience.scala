package arch2.data.experience

import base._
import arch2.base.predicates.Variable
import arch2.base.predicates.UnboundPredicateState
import scala.collection.mutable.Map
import arch2.base.predicates.BlockPredicate
import arch2.base.predicates.ChainingPredicate
import utils.io.ByteWriter
import arch2.data.ProgressInfo
import utils.io.ByteReader
import scala.collection.immutable.Queue
import arch2.base.predicates.BoundPredicateState
import arch2.data.Serializer
import utils.Options._
import utils.generic.MA._
import utils.Factors._

class SortingCARCASSTreeExperience(hasExpertKnowledge:Boolean = true) extends CARCASSTreeExperience(hasExpertKnowledge) {
  /** This constant determines, after how many totalMatches the node's children are reordered by their matchCount.
   *  This value has been empirically determined to provide the most significant speed up.
   */
  val REORDER_THRESHOLD = 50 k
  
  /** This derived node class keeps track of failed and succeded state matches for reordering
   */
  class SortableTreeNode(state:UnboundPredicateState[A], actionMap:Map[Variable[A],Double], children:Queue[TreeNode] = Queue()) extends TreeNode(state,actionMap, children) {
    var matchCount:Int = 0     //Number of times, this node(or subnode) was actually the searched node
    var totalMatches:Int = 0   //Number of total subnodes, which have been checked for a match since the last reordering
    
    /** Serialize the custom node fields too
     */
    override def serialize(implicit writer:utils.io.ByteWriter, ss:Serializer[UnboundPredicateState[A]], as:Serializer[List[(Variable[A],Double)]]) {
      import Serializer._
      super.serialize
      
      Serializer.serialize(matchCount)
      Serializer.serialize(totalMatches)
    }
  }
  
  //TODO override "insertState" in such a way that is searches the whole tree for the new state and references it if it already exists to prevent duplicate states.
  //TODO check whether this idea is compatible with the serialization
  
  /** Overwritten node creation to return SortableTreeNodes
   */
  protected override def createNode(state:UnboundPredicateState[A], actionMap:Map[Variable[A],Double], children:Queue[TreeNode] = Queue()):TreeNode = new SortableTreeNode(state, actionMap, children)
  
  /** Had to overwrite this method to take care of node reordering
   */
  override def extractFirstMatch[X](matcher:State=>Option[X]):Option[ExtractorMatch[X]] = extractMatch(matcher, experience)
  
  private def extractMatch[X](matcher:State=>Option[X], node:TreeNode):Option[ExtractorMatch[X]] = {
    val sortNode = node.asInstanceOf[SortableTreeNode] //Cast necessary due to inheritance complications
    
    if (sortNode.totalMatches >= REORDER_THRESHOLD) //Reorder children if necessary
      reorderNode(sortNode)
    
    val matchRes = matcher(sortNode.state)
    sortNode.totalMatches += 1 //Increment the count of total matches on this node
    if (matchRes) {
      sortNode.matchCount += 1 //Increment the number of times, this node was correctly matched
      node.children.findFirst(extractMatch(matcher, _)) or Some(ExtractorMatch(matchRes.get, node.state)) 
    } else {
      None
    }
  } 
  
  /** This helper function reorders the node's child nodes according to their matchCount and resets 
   *  the counter varibales afterwards
   */
  private def reorderNode(node:SortableTreeNode) {
    node.children = node.children.sortBy(child => -child.asInstanceOf[SortableTreeNode].matchCount) //order descending by matchCount
    node.totalMatches = 0 //Reset parent's total matches
    node.children.foreach(child => child.asInstanceOf[SortableTreeNode].matchCount = 0) //reset children's matchCount
  }
  
  
  /** This method is used to load a single tree node from a saved file
   */
  protected override def loadNode(implicit reader:ByteReader):TreeNode = {
    import arch2.data.Serializer._
    val node = super.loadNode.asInstanceOf[SortableTreeNode] //Will load a SortableNode with state and actionMap
    node.matchCount = load[Int]
    node.totalMatches = load[Int]
    node
  }
}