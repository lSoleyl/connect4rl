package arch2.data.experience

import base._
import arch2.base.predicates.Variable
import arch2.base.predicates.UnboundPredicateState
import scala.collection.mutable.Map
import arch2.base.predicates.BlockPredicate
import arch2.base.predicates.ChainingPredicate
import utils.io.ByteWriter
import arch2.data.ProgressInfo
import utils.io.ByteReader
import scala.collection.immutable.Queue
import arch2.base.predicates.BoundPredicateState

class CARCASSTreeExperience(hasExpertKnowledge:Boolean = true) extends PredicateTreeExperience[Int,Double] with DynamicStateAbstraction with StreamSerializable {
  val defaultVariable:Variable[Int] = Variable('?') //Variable for random action '?'
  val defaultVariableStartValue:Double = -1d
  val default:Double = 0d
  type A = Int
  
  protected var experience:TreeNode = createNode(new UnboundPredicateState(Nil), Map(defaultVariable -> defaultVariableStartValue)) // true -> ?
  
  //This variable holds the partially entered trace entry which gets completed on the next update
  private var nextTraceEntry:Double=>TEntry = null
  
  
  if (hasExpertKnowledge) {
    // Expert-Knowledge (first two states)
    enterState(new UnboundPredicateState(List(ChainingPredicate(4, Variable('X')))), experience.state)
    enterState(new UnboundPredicateState(List(BlockPredicate(4, Variable('X')))), experience.state)
  }
  
  def setTDEThreshold(tde:Double) { tdeThreshold = tde }
  
  override def insertState(state:State, derivedFrom:State):Boolean = enterState(state, derivedFrom)
  
  
  
  
  /** This method should get called by PredicateStateUnifier in selectAction()
   *  to notify the experience about the used abstraction.
   *  This information is needed to derive new abstract states.
   *  
   *  @param cs the concrete state
   *  @param as the derived abstract state
   *  @param aa the chosen abstract action
   *  @param ca the concrete action
   */
  def notifyExperience(cs:BoundPredicateState[A], as:UnboundPredicateState[A], aa:Variable[A], ca:A) {
    nextTraceEntry = TraceEntry(cs,as,aa,ca)
  }
  
  
  /** This class overrides the update method to insert the logging of trace entries.
   *  To do this, it needs the current Q-Value of the State,Action pair to calculate the
   *  TDE, which will entered into the trace entry.
   *  
   *  @throws MissingStateException if the state doesn't exist in the experience
   */
  override def update(key:SA, value:Value):Unit = {
    val entry = experience.find(key.s)
    if (entry.isEmpty)
      throw new MissingStateException(key.s)
    
    val actionMap = entry.get.actionMap
    val tde = value - actionMap(key.a)  //calculate TDE*alpha
    actionMap(key.a) = value            //update Q value

    //nextTraceEntry can't be zero if a value gets an update, since QLearning doesn't call update on the first state    
    addTrace(nextTraceEntry(tde)) //This might cause a new state to be derived
  }
  
  /** This method is used to load a single tree node from a saved file
   */
  protected def loadNode(implicit reader:ByteReader):TreeNode = {
    import arch2.data.Serializer._
    createNode(load[UnboundPredicateState[Int]], Map(load[List[(Variable[Int],Double)]]: _*))
  }
  
  override def writeToStream(out:OutputStream, progress:ProgressInfo) {
    implicit val writer = new ByteWriter(out)
    import writer._
    import arch2.data.Serializer._
    
    
    val length = stateCount.get
    val tempData = tempDataSize
    import tempData._
    log.info(s"Writing $length states")
    writeInt(length)
    writeInt(traceLength)
    writeInt(tdeMapSize)
    
    progress.setTotal(length + traceLength + tdeMapSize)
    
    log.info(s"Building nodeMap")
    val nodeMap:Map[TreeNode,Int] = Map()
    experience.foldLeft(0)((id:Int,node) => {
      nodeMap(node) = id
      id + 1
    })
    
    log.info(s"Writing ${nodeMap.size} nodeMap entries")
    //Serialize statemap  (Mapping ID <-> (state,actionMap))
    for((node,id) <- nodeMap) {
      serialize(id)
      node.serialize
      progress.next
    }
    
    log.info(s"Writing tree structure") //Mapping ID -> children.ids
    for((node,id) <- nodeMap) {
      serialize(id)
      serialize(node.children.map(nodeMap))      
    }
    
    
    log.info(s"Writing $traceLength trace entries and $tdeMapSize tdeMap entries")
    tempToStream(progress)
    
    log.info("done.")
  }
  
  override def loadFromStream(in:InputStream, progress:ProgressInfo) { 
    log.info(s"CARCASSExperience.loadFromStream")
    implicit val reader = new ByteReader(in)
    import reader._
    import arch2.data.Serializer._
    import utils.Times._
    
    
    val length = readInt
    val traceLength = readInt
    val mapSize = readInt
    log.info(s"Loading $length states")
    
    progress.setTotal(length+traceLength+mapSize)

    val nodeMap:Map[Int, TreeNode] = Map()
    length.times {
      nodeMap(load[Int]) = loadNode
      progress.next
    }
    
    log.info("Rebuilding tree structure")
    nodeMap.size.times {
      nodeMap(load[Int]).children = load[Queue[Int]].map(nodeMap)      
    }
    
    //set root (id:0)
    experience = nodeMap(0)

    log.info(s"Loading $traceLength traces and $mapSize tdeMap entries")
    tempFromStream(traceLength, mapSize, progress)
  }
  
  
  override def customAction {
    println("Printing nodes:")
    
    val childMap = scala.collection.mutable.Map[UnboundPredicateState[Int], Int]()
    
    experience.foreach(node => {
      if (!node.children.isEmpty)
        childMap(node.state) = node.children.length
    })
    
    childMap.toList.sortBy(x => -x._2).foreach(x => println(x._1 + "  - " + x._2))
    
    clearTempData
    println("\n\nCleared temporary data")    
  }
  
}