package arch2.base

import arch2.field._

/** This class will be used by the game to store the players current color in one object.
 *  The color isn't stored in the player itself anymore, because it isn't a real property
 *  of the player/AI
 */
case class ColoredPlayer(color:Stone, player:Player)

object ColoredPlayer {
  implicit def toPlayer(cp:ColoredPlayer):Player = cp.player
}