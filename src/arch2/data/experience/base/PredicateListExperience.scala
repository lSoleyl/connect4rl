package arch2.data.experience.base

import arch2.base.predicates._
import utils.Log
import scala.collection.mutable.Map
import utils.Options._


trait PredicateListExperience[A,V] extends Experience[UnboundPredicateState[A],Variable[A],V] with Log {
  import scala.collection.mutable.Map
  import utils.Options._
  
  type ActionMap = Map[Variable[A],V]
  type BoundVariables = List[BoundVariable[A]]
  
  val defaultVariable:Variable[A] //Variable for random action '?'
  val defaultVariableStartValue:V
  
  case class ListEntry(state:State, actionMap:ActionMap)
  case class MatchResult(entry:ListEntry, boundVariables:BoundVariables)  
  
  protected var experience:List[ListEntry] = List()
  
  override def extractFirstMatch[X](matcher:State=>Option[X]):Option[ExtractorMatch[X]] = {
    for(entry <- experience) {
      val matchData = matcher(entry.state)
      if (matchData)
        return Some(ExtractorMatch(matchData.get,entry.state))
    }
    return None
  }
  
  
  override def containsState(state:State) = !experience.find(_.state == state).isEmpty
  
  
  /** This method returns a new Listentry for the given state
   *  
   */
  def makeEntry(state:State):ListEntry = {
    val actionMap:ActionMap = Map()
    state.getBoundActions.foreach(actionMap(_) = default) //Populate Entry
    actionMap(defaultVariable) = defaultVariableStartValue
    
    ListEntry(state,actionMap)
  }
  
  /** With this method the caller can add a new entry into the list and gets a ListEntry returned, which 
   *  he then can manipulate without matching the state again.
   *  
   * @param state the state to add
   * 
   * @return the newly created listEntry
   */
  def enterState(state:State):ListEntry = {
    val entry = makeEntry(state)
    experience = entry :: experience
    entry
  }
  
  
  override def bestActionEntry(state:State, allowedActions:List[Action], isBetter:(Value,Value)=>Boolean):Entry = {
    val entry = experience.find(_.state == state)
    
    if (!entry)
      return Entry(state, allowedActions.head, default)
      
    val (action,value) = entry.get.actionMap.reduce((av1,av2) => if (isBetter(av1._2,av2._2)) av1 else av2)
    Entry(state, action, value)
  }
  
  
  override def apply(key:SA):Value = {
     for(entry <- experience) {
       if (entry.state == key.s)
         return entry.actionMap.getOrElse(key.a, default)
     }
     
     return default
  }
  
  override def update(key:SA, value:Value):Unit = {
    val entry = experience.find(_.state == key.s) default enterState(key.s)
    
    entry.actionMap(key.a) = value
  }
  
  override def enterState(state:State, actions:List[Action]) {
    val actionMap:ActionMap = Map()
    actions.foreach(actionMap(_) = default)
    actionMap(defaultVariable) = defaultVariableStartValue
    
    experience = ListEntry(state,actionMap) :: experience
  }
  
  
  override def toString = experience.toString
}