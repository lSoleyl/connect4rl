package arch2.base

import arch2.field._
import arch2.base.predicates.ChainingPredicate
import arch2.base.predicates.BlockPredicate
import arch2.base.predicates.BoundPredicateState

trait PredicateState extends Player {  
  /** The original turn method is implemented by converting the field state into a list of predicates
   *  
   *  @param state the original field state
   *  @param actions the list of valid actions
   *  @param color the current player's color
   *  
   *  @return the selected slot
   */
  override def turn(state:FieldState, actions:List[Int], color:Stone):Int = turn(convertState(state, actions, color), actions, color)

  
  /** Implement this method to implement the player's interface.
   *  The state is provide to the player through a list of predicates which are all true in the current state.
   *  
   *  @param state a list of predicates
   *  @param actions a list of possible actions
   *  @param color the color of the current player
   *  
   *  @return the selected column
   */
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone):Int
  
  
  /** This method takes a field state and converts it into an ORDERED predicate list.
   *  
   *  @param state the current field state to convert
   *  @param actions the list of available actions to save a bit time during the creation of the predicate list
   *  @param color the color of the current player. This is important, because it switches the predicates' meaning
   * 
   */
  protected def convertState(state:FieldState, actions:List[Int], color:Stone):BoundPredicateState[Int] = {
    val other = Stone.switch(color)
    
    
    val lines = actions.flatMap(action => (state.getLines(action, color) ++ state.getLines(action, other)).map(line => (action, line)))
    val predicates:BoundPredicateState[Int] = lines.filter(_._2.length >= 2).distinct.map(acLine => if (acLine._2.color == color) ChainingPredicate(acLine._2.length, acLine._1) else BlockPredicate(acLine._2.length, acLine._1))
    
    val partition = predicates.partition(_.chainLength > 2)
    if (!partition._1.isEmpty) 
      partition._1.sorted
    else //Add predicates of length 2 to the state description if it's empty
      predicates.sorted
  }
}