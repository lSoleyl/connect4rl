package utils

object Validation {
  implicit class Validate[A](obj:A) {
    def validate(valid:A=>Boolean, errorMsg: A=>String = _ + " is in an invalid state"):A = {
      if (valid(obj))
        obj
      else
        throw new ObjectValidationException(errorMsg(obj))
    }
  }

}

class ObjectValidationException(message:String) extends Exception(message)