package arch2.game

import arch2.base._
import arch2.gui._
import utils.Log
import scala.swing._
import utils.Options._
import java.io._
import utils.TryWithResources
import arch2.data.ExportData
import utils.io._
import arch2.data.Serializer._
import java.util.Date
import utils.Times._
import arch2.data.AILoader
import arch2.data.ProgressInfo
import scala.annotation.tailrec
import spray.json._
import scala.util.{Success, Failure, Try}

object MultiBatchRunner extends BatchRunner with Log {
  val JSON_FORMAT_STRING = "JSON-Format"
  
  def resume(from:InputStream):GameStatistics = {
    withResumeData(from) { data =>
      import data._
      run(p1, p2, config, batchConfig, episodeList, totalEpisodes, results) 
    }
  }
  
  
  /** Helper method which takes care of loading all information from a .resume file and then
   *  calls a function, which takes a ResumeData object as argument. 
   *  The function is called inside the acquired loaded locks so manipulation of the agents memory by other actions is prevented.
   *  
   *  @param from the source to read from
   *  @param fn the function to call with the ResumeData object
   *  
   *  @return the functions return value 
   */
  def withResumeData[T](from:InputStream)(fn:ResumeData=>T):T = {
    implicit val reader = new ByteReader(from)
    val s1 = reader.readString
    val jsonFormat = s1 == JSON_FORMAT_STRING //Determine, whether we are using the new JSON format
    val loadFn = if (jsonFormat) ResumeData.loadJSON _ else ResumeData.loadBinary _
    
    val List(l1,l2) = (if(jsonFormat) 2.collect(reader.readString) else List(s1,reader.readString)).map(AILoader.loaders)
    
    logDebug(s"Acquiring AILoader locks for '$l1' and '$l2'")
    
    utils.Acquire(l1,l2) { //Acquire loader locks
      logDebug("Acquired all locks")  
      
      
      val List(p1,p2) = List(l1,l2).map { loader =>
        val loadScreen = new LoadingScreen(loader)
        loadScreen.load(Some(from))
        loader.getPlayer
      }
      
  
      fn(loadFn(p1,p2)) 
    }
  }
  
  
  def start(p1:Player, p2:Player, config:ConfigurationData, batchConfig:BatchConfiguration):GameStatistics = {
    val (episodeList,totalEpisodes) = batchConfig.multi.stepType.getEpisodeList(batchConfig.multi.initialStep, batchConfig.multi.totalEpisodes)
    
    if (totalEpisodes != batchConfig.multi.totalEpisodes) { //Notify if we have to simulate less episodes
      log.info(s"Total amount of episodes lowered according to selected step type: ${batchConfig.multi.totalEpisodes} -> $totalEpisodes")
    }
    
    run(p1, p2, config, batchConfig, episodeList, totalEpisodes)
  }
  
  
  /** Wraps the conversion from the game statistics list into an ExportData object and exports it via MatlabExport to file.
   *  
   *  @param p1 the first player which participated in the game
   *  @param p2 the second player which participated in the game
   *  @param results the reversed game statistics list
   *  @param fileName the filename of the file to export to
   *  @param outputDir the directory to create the file in
   *  
   *  @return true if export was successful, false otherwise
   */
  def exportStats(p1:Player, p2:Player, results:List[GameStatistics], fileName:String, outputDir:String = "."):Boolean = {
    val exportData = ExportData(p1,p2) //Export data only needed here
    results.reverse.foreach(stats => exportData.addEntry(stats.totalEpisodes, stats.getFor(p1), stats.getFor(p2))) //Build up export data from total results    
    MatlabExport.exportToFile(exportData, outputDir, fileName)
  }
  
  /** This method actually runs the multi batch run. It also handles continuing from a previously interrupted batch run
   *  based on the passed arguments.
   */
  private def run(p1:Player, p2:Player, config:ConfigurationData, batchConfig:BatchConfiguration, episodeList:List[Int], totalEpisodes:Int, resultList:List[GameStatistics] = Nil):GameStatistics = {
    val field = Field
    val progress = new MultiBatchProgressScreen(totalEpisodes)
    progress.start
    
    
    var results:List[GameStatistics] = Nil    
    val remainingEpisodeList = skipEpisodes(episodeList, resultList, progress)
    
    for(epCount <- remainingEpisodeList) {
      if (!progress.isAborted) {
        val batchProg = progress.nextBatch(epCount)
        
        val result = Try { GameManager.batchRun(p1, p2, field, config, epCount, batchProg, progress) }
        
        if (result.isFailure) {
          progress.stop
          Swing.onEDT { progress.close }
          result.get //Rethrow caught exception
        }

        val stats = result.get
        results = stats :: results
      }      
    }
    
    results = combine(results, resultList) //Merge current results with results from previous run
    
    //Stop measurement thread and close progress screen
    progress.stop
    Swing.onEDT { progress.close }
    
    //Game got aborted by user... save data 
    if (progress.isAborted) { 
      log.info("Batch run aborted, saving data to resume it later...")
      
      val targetDirectory = new File(config.agentDir)
      if (!targetDirectory.exists() && !targetDirectory.mkdirs()) {
        log.error("Failed to create agent directory at: " + targetDirectory.getAbsolutePath() + " - interrupted batch run has not been saved!")
        return results.head
      }
        
     
      val fileName = dateString + ".resume"
      
      val filePath = targetDirectory.getAbsolutePath() + File.separator + fileName
      
      TryWithResources(new FileOutputStream(new File(filePath))) { fs =>
        implicit val writer:ByteWriter = new ByteWriter(fs)
        
        writer.writeString(JSON_FORMAT_STRING) //Use JSON format for new resume files
        
        //First save agents, because we need the players already instantiated to get a stat serializer
        writer.writeString(p1.loader.loaderName)
        writer.writeString(p2.loader.loaderName)
        
        for(player <- List(p1,p2)) {
          val prog = new LoadingScreen(player.loader)
          prog.save(writer.stream) //Display a progress screen for each player being saved
        }
        
        ResumeData(p1, p2, results, episodeList, totalEpisodes, config, batchConfig).saveJSON
        Swing.onEDT { Dialog.showMessage(message = s"Saved interrupted batch run to: '$filePath'", title = "info", messageType = Dialog.Message.Info) }
        
      }.recover { //Don't propagate exception up, to ensure that the result window gets shown 
        case ex:Throwable => log.error("Failed to write resume file for interrupted batch run.", ex)
      } 
    } else {
      import utils.Factors._
      val fileName = MatlabExport.suggestFileName(p1, p2, totalEpisodes.short)
      if (exportStats(p1,p2, results, fileName, batchConfig.multi.outputDir))
        exportConfig(config, batchConfig, fileName, batchConfig.multi.outputDir)
    }
    
    //return the results of the last run (much more helpful than overall results)
    results.head 
  }
  
  
  private def dateString = {
    val dt = new Date();
    f"${dt.getYear()+1900}-${dt.getMonth()+1}%02d-${dt.getDate()}%02d--${dt.getHours()}%02d-${dt.getMinutes()}%02d-${dt.getSeconds()}%02d"
  }
  
  /** Export the configuration, which was used for the batch run as JSON file  
   */
  private def exportConfig(config:ConfigurationData, batchConfig:BatchConfiguration, fileName:String, outputDirectory:String = ".") {
    val jsConfig = ConfigProtocol.configFormat.write(config.convert) //TODO implement a manual toJSON instead of wobbly formats
    val jsBatchConfig = batchConfig.toJSON
    val path = outputDirectory + File.separator + fileName + ".cfg"
    
    try { 
      utils.JSON.save(JsObject(Map("batch_configuration" -> jsBatchConfig, "gui_configuration" -> jsConfig)), path)
    } catch {
      case ex:Throwable => log.error("Failed to save batch run configuration!", ex)
    }
  }
  
  
  /** This method skips the progress bar to the last position when resuming a batch run from file 
   *  
   */
  private def skipEpisodes(episodeList:List[Int], results:List[GameStatistics], progress:MultiBatchProgressScreen):List[Int] = {
    
    @tailrec
    def skip(episodes:List[Int], results:List[GameStatistics]):List[Int] = {
      val p = progress.nextBatch(episodes.head)
      if (results.tail != Nil) { //A complete batch run
        p.nextN(episodes.head)   //Move complete episode
        skip(episodes.tail, results.tail)
      } else { //Last entry
        val epCount = results.head.totalEpisodes
        p.nextN(epCount)
        (episodes.head - epCount) :: episodes.tail //Return remaining episodes
      }
    }
    
    if (results == Nil)
      episodeList
    else
      skip(episodeList, results.reverse)
  }
  
  /** Append tail to head and merge head's last element with tail's first element
   */
  private def combine(head:List[GameStatistics], tail:List[GameStatistics]):List[GameStatistics] = {
    if (tail == Nil) { //Nothing to combine with
      head
    } else if (head.tail != Nil) { //Default case
      head.head :: combine(head.tail, tail)
    } else { //Last element of head
      tail.head.add(head.head) //Changes the element on which the method is called
      tail
    }
  }
  
  case class ResumeData(p1:Player, p2:Player, results:List[GameStatistics], episodeList:List[Int],  totalEpisodes:Int, config:ConfigurationData, batchConfig:BatchConfiguration) {
    def saveBinary(implicit writer:ByteWriter) {
      //Serialize batch run info
      implicit val statSerializer = GameStatistics.serializer(p1, p2)
      serialize(results) //Write game statistics
      serialize(episodeList) //Write episode list
      serialize(totalEpisodes)
      serialize(config)   
      serialize(batchConfig)
    }
    
    def saveJSON(implicit writer:ByteWriter) {
      val json = JsObject(Map(
        "totalEpisodes" -> JsNumber(totalEpisodes),
        "episodeList" -> JsArray(episodeList.map(JsNumber(_))),
        "results" -> JsArray(results.map(_.toJSON)),
        "config" -> config.toJSON,
        "batchConfig" -> batchConfig.toJSON
      ))
      writer.writeString(json.toString())
    }
  }
  
  object ResumeData {
    def loadBinary(p1:Player, p2:Player)(implicit reader:ByteReader) = {
      //Load batch run info
      implicit val statSerializer = GameStatistics.serializer(p1, p2)
      ResumeData(p1,p2, load[List[GameStatistics]], load[List[Int]], load[Int], load[ConfigurationData], load[BatchConfiguration])
    }
    
    def loadJSON(p1:Player, p2:Player)(implicit reader:ByteReader) = {
      val json = reader.readString.asJson
      val obj = json.asJsObject
      val member = (x:String) => obj.fields.get(x)
      
      //This will throw errors if fields are missing
      val totalEpisodes = member("totalEpisodes").as[JsNumber].get.value.toInt
      val episodeList = member("episodeList").as[JsArray].get.elements.map(_.asInstanceOf[JsNumber].value.toInt).toList
      val results = member("results").as[JsArray].get.elements.map(GameStatistics.fromJSON(p1, p2, _)).toList
      val config = ConfigurationData.fromJSON(member("config").get)
      val batchConfig = BatchConfiguration.fromJSON(member("batchConfig").get).get
      ResumeData(p1,p2,results,episodeList,totalEpisodes,config,batchConfig)
    }
  }
}


//Matlab export format
object MatlabExport extends Log {
  
  /** Export the collected batch run data into a file
   *  
   *  @return true if the export was successful, false otherwise
   */
  def exportToFile(data:ExportData, outputDirectory:String, fileName:String):Boolean = {
    val outDir = new File(outputDirectory)
    if (!outDir.exists() && !outDir.mkdirs()) {
      log.error("Failed to create result directory at: " + outDir.getAbsolutePath() + " - results have not been written!")
      return false
    }
      
    val outFile = new File(outputDirectory, fileName)
    log.info(s"Exporting results to ${outFile.getPath()}...")
    
    TryWithResources(new FileOutputStream(outFile)) {
      writeToStream(data, _)
    } match {
      case Success(_) => {
        log.info("Export done.")
        true
      }
      case Failure(ex) => {
        log.error("Failed to export batch run results. Probably no write access to file.", ex)
        false        
      }
    }
  }
  
  
  def suggestFileName(p1:Player, p2:Player, totalEpisodes:String) = {
    val List(str1,str2) = p1.uniqueNames(p2)
    s"${str1}-vs-${str2}-${totalEpisodes}.txt"
  }
  
  def writeToStream(data:ExportData, out:OutputStream) {
    import utils.ColumnWriter
    //We have to encode our results in Windows 1252 for Matlab <.< (fail)
    val encoding = if (java.nio.charset.Charset.isSupported("Windows-1252")) "Windows-1252" else "UTF-8"    
    
    TryWithResources(new ColumnWriter(out, ColumnWriter.Options(encoding = encoding))) { writer =>
      val episodes = data.getEpisodes
      val (p1Data, p2Data) = data.getDataPair
    
      
      writer.addColumn(ExportData.HeaderConstants.EPISODES :: episodes.map(Format.episodes))
      writer.addColumn(p1Data.getCompleteColumn(p1Data.columns.winRate)(Format.winRate))
      writer.addColumn(p2Data.getCompleteColumn(p2Data.columns.winRate)(Format.winRate))
      
      
      if (p1Data.columns.states.getEntries.forall(!_.isEmpty)) {
        writer.addColumn(p1Data.getCompleteColumn(p1Data.columns.states)(Format.states))
      }
      
      if (p2Data.columns.states.getEntries.forall(!_.isEmpty)) {
        writer.addColumn(p2Data.getCompleteColumn(p2Data.columns.states)(Format.states))
      }
    }
  }
  
  object Format {
    val episodes = (x:Int) => x.toString
    val states = (x:Option[Int]) => x.get.toString                  //v- String.format rounds values correctly
    val winRate = (x:Double) => String.format(java.util.Locale.US, "%4.3f", x.asInstanceOf[Object])
  }
}




