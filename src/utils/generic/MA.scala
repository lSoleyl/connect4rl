package utils.generic

import utils.generic.functions._

object MA {
  implicit class MAExtensions[M[_],A](ma:M[A]) {
    def context[B](f:A=>B)(implicit map:Map[M]):M[Context[A,B]] = map(ma, (v:A) => Context(v, f(v)))
    
    //FIXME: When retyping (type MyContainer[A] = List[Option[A]]), the type deduction fails and the predicate is expected to be of type A=>Boolean instead of Option[A]=>Boolean
    def partMap[T,F](predicate:A=>Boolean)(trueMap:A=>T)(falseMap:A=>F)(implicit partition:Partition[M], map:Map[M]):MappedPartition[M,T,F] = {
      val parts = partition(ma, predicate)
      MappedPartition(map(parts._1, trueMap), map(parts._2, falseMap))
    }
    
    def occurrenceMap()(implicit groupBy:GroupBy[M], length:Length[M]):scala.collection.Map[A,Int] = groupBy[A,A](ma, identity).mapValues(length(_))
    
    /** Creates a new OccurrenceList which contains unique entries ordered in descending order by occurences
     */
    def occurrenceList()(implicit groupBy:GroupBy[M], length:Length[M], fromMap:FromMap[M], sortBy:SortBy[M], map:Map[M]):M[A] = {
      map[(A,Int),A](sortBy[(A,Int),Int](fromMap(occurrenceMap), - _._2), _._1)
    }
    
    def findFirst[B](predicate:A=>Option[B])(implicit foreach:ForEach[M]):Option[B] = {
      foreach[A](ma, { x => 
        val opt = predicate(x)
        if (!opt.isEmpty)
          return opt
      })
      
      return None
    }
    
    def mapKeyToValues[K,V](fValue:A=>V)(fKey:A=>K = identity _)(implicit foreach:ForEach[M]):scala.collection.mutable.Map[K,List[V]] = {
      val result = scala.collection.mutable.Map[K,List[V]]()
      
      foreach[A](ma, { a =>
        val key = fKey(a)
        val value = fValue(a)
        
        result(key) = value :: result.getOrElse(key, Nil)
      })
      
      result
    }
    
    def valueList[K,V](fValue:A=>V)(fKey:A=>K = identity _)
                      (implicit foreach:ForEach[M], numeric:Numeric[V], fromMap:FromMap[M], sortBy:SortBy[M], map:Map[M]) = {
      map[(K,V),K](sortBy[(K,V),V](fromMap[K,V](mapKeyToValues(fValue)(fKey).mapValues(_.sum)), _._2), _._1)
    }
  }

}