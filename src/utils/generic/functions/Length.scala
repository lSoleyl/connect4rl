package utils.generic.functions

trait Length[M[_]] {
  def apply[A](ma:M[A]):Int
}

object Length {
  implicit object ListLength extends Length[List] {
    override def apply[A](list:List[A]) = list.length  
  }
}