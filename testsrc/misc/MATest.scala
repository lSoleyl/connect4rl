package misc

import org.scalatest._
import utils.generic.MA._
import utils.Options._

class MATest extends FlatSpec with Matchers {
  "findFirst(x>2)" should "return 3 for List(1,2,3,4,5)" in {
    List(1,2,3,4,5).findFirst(_.someIf(_ > 2)) should be (Some(3))
  }
  
  it should "return None for List(0,1,2)" in {
    List(0,1,2).findFirst(_.someIf(_ > 2)) should be (None)
  }
  
  it should "return None for Nil" in {
    val empty:List[Int] = Nil
    empty.findFirst(_.someIf(_ > 2)) should be (None)
  }
}