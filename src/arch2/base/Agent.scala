package arch2.base

import scala.util.Random

trait Agent[S,A,V] {
  type State = S
  type Action = A  
  type LearningParameters = arch2.config.LearningParameters
  type Experience = arch2.data.experience.base.Experience[S,A,V]
  type Value = V
  type Entry = experience.Entry
  type SA = experience.SA
  
  val experience:Experience
  
  protected def isBetter(v1:Value, v2:Value):Boolean
  
  /** This method returns best known action in the given state out of the possible actions
   *  
   *  @param state the current state the agent is in
   *  @param possibleActions a list of possible actions
   *  
   *  @return an entry of ((State,Action),Value) which contains the best action, the current state and the
   *          Q-Value of the best action 
   */
  protected def bestActionEntry(state:S, possibleActions:List[A]):Entry = {
    experience.bestActionEntry(state, possibleActions, isBetter)
  }
  
  /** This method selects an Action entry according to the epsilon greedy policy.
   *  
   *  @param bestAction the currently selected best actions
   *  @param possibleActions all possible actions in the current state
   *  @param epsilon the chance at which exploration should occurr
   *  
   *  @return if exploration occurred then a random action otherwise the bestAction
   */
  protected def applyEpsilonGreedy(bestActionEntry:Entry, possibleActions:List[A], epsilon:Double):Entry = {
    if (Random.nextDouble < epsilon) {
      val action = possibleActions(Random.nextInt(possibleActions.size))
      val state = bestActionEntry.sa.s
      val sa = new SA(state,action)
      new Entry(sa,experience(sa))
    } else {
      bestActionEntry
    }
  }
  
  //Abstract methods which every agent must implement
  def selectAction(state:S, reward:Double, possibleActions:List[A]):A //main decision method
  def finalReward(reward:Double)                                      //reward at end of episode
  def initialize(params: LearningParameters)
}