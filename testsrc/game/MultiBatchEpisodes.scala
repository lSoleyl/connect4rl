package game

import org.scalatest._
import arch2.gui.BatchConfiguration
import utils.Factors._

class MultiBatchEpisodes extends FlatSpec with Matchers {
  
  val log10 = BatchConfiguration.Step.LOG10.getEpisodeList _
  val linear = BatchConfiguration.Step.LINEAR.getEpisodeList _
  
  
  "linear(10,100)" should "return 100 total episodes" in {
    linear(10, 100)._2 should be (100)
  }
  
  it should "return episodes: 10 times 10" in {
    linear(10, 100)._1 should be (List.fill(10)(10))
  }
  
  "linear(100,1M)" should "return 1M total episodes" in {
    linear(100, 1 M)._2 should be (1 M)
  }
  
  it should "return an episode list with 10k entries" in {
    linear(100, 1 M)._1.length should be (10 k)
  }
  
  it should "only contain 100 as entry" in {
    linear(100, 1 M)._1.foreach(_ should be (100)) 
  }
  
  "linear(10,25)" should "return 20 as total episodes" in {
    linear(10, 25)._2 should be (20)
  }
  
  it should "return List(10,10)" in {
    linear(10, 25)._1 should be (List(10,10))
  }
  
  "log10(10,100)" should "return 100 total episodes" in {
    log10(10, 100)._2 should be (100)
  }
  
  it should "return episodes 10 times 10" in {
    log10(10, 100)._1 should be (List.fill(10)(10))
  }
  
  "log10(100,1M)" should "return 1M episodes in total" in {
    log10(100, 1 M)._2 should be (1 M)
  }
  
  it should "return an episode list with 37 entries" in {
    log10(100, 1 M)._1.length should be (37)
  }
  
  it should "return an episode list (100x10,1000x9, 10000x9, 100000x9)" in {
    val list = log10(100, 1 M)._1
    
    
    list.head should be (100)
    
    for(i <- (0 until 4)) { //i = 0,1,2,3
      val slice = list.tail.slice(i*9, (i+1)*9)
      slice should be (List.fill(9)(100 * Math.pow(10,i).toInt))
    }
  }
  
  
  "log10(10,10)" should "return 10 episodes in total" in {
    log10(10, 10)._2 should be (10)
  }
  
  it should "return List(10)" in {
    log10(10, 10)._1 should be (List(10))
  }
  
  "log10(10, 250)" should "return 200 episodes in total" in {
    log10(10, 250)._2 should be (200)
  }
  
  it should "return 11 batch runs" in {
    log10(10, 250)._1.length should be (11)
  }
  
  it should "return (10x10, 100x1)" in {
    log10(10, 250)._1 should be (List.fill(10)(10) ::: List(100))
  }
  
  "log10(10, 9) and log10(10,0)" should "return zero episodes in total" in {
    log10(10, 9)._2 should be (0)
    log10(10, 0)._2 should be (0)
  }
  
  it should "return an empty list" in {
    log10(10, 9)._1 should be (Nil)
    log10(10, 0)._1 should be (Nil)
  }
  
  "linear(0,10)" should "return (Nil,0), because (10 / 0) = undefined" in {
    linear(0,10) should be ((Nil,0))
  }
  
  "log10(0,10)" should "return (Nil,0), because (10 / 0) = undefined" in {
    log10(0,10) should be ((Nil,0))
  }
  
  
}