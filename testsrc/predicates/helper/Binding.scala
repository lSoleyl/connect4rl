package predicates.helper

import arch2.base.predicates.BoundVariable
import arch2.base.predicates.Variable
import utils.Options._

case class Binding[T](vars:Option[List[BoundVariable[T]]]) {
  val possible = !vars.isEmpty
  val boundVars = (vars default Nil)
  val length = boundVars.length
  
  
  def apply(variable:Variable[T]):Option[T] = (vars default Nil).find(_.variable == variable).map(_.value)
}