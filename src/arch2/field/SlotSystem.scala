package arch2.field

/** This trait adds a part of game logic to the field.
 *  If offers a method to "throw" stones into slots instead of placing
 *  them at a specific coordinate.
 */
trait SlotSystem extends Field2DRW {
  
  /** Method to actually throw a stone into the playfield
   *  If the slot isn't free, then this method will fail with an exception
   *  
   *  @param slot the slot to throw into
   *  @param color the stone to throw in
   *  
   *  @return the row at which the stone landed (for later checking of lines)
   */
  def putIntoSlot(slot:Int, color:Stone):Int = {
    val row = getLandingPos(slot) //Might throw an exception
    this(slot, row) = color
    row
  }
}