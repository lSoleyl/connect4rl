package predicates

import org.scalatest._
import arch2.base.predicates._
import utils.Options._
import data.Predicates
import data.Variables
import helper.Binding

class PredicateUnification extends FlatSpec with Matchers {
  import Variables._
  import Predicates._
  
  object ub {
    val EMPTY_STATE = new UnboundPredicateState[Int](Nil)
    val ONLY_A = new UnboundPredicateState(A(X) :: Nil)
    val A_X = ONLY_A
    
    val AB_XX = new UnboundPredicateState(A(X) :: B(X) :: Nil)
    
    val ABC_XYZ = new UnboundPredicateState(A(X) :: B(Y) :: C(Z) :: Nil)    
    //TODO add partially unbound states... maybe
  }
  
  object b {
    val EMPTY_STATE = Nil 
    val ONLY_A = A(3) :: Nil
    val ONLY_B = B(7) :: Nil
    val A_3 = ONLY_A
    val B_7 = ONLY_B
    
    val ABC_555 = A(5) :: B(5) :: C(5) :: Nil
    val ABC_123 = A(1) :: B(2) :: C(3) :: Nil
    val AAB_122 = A(1) :: A(2) :: B(2) :: Nil //Backtracking necessary when matching AB_XX
    val AAA_123 = A(1) :: A(2) :: A(3) :: Nil
    
  }
  
  
  //Precondition for all tests
  "All variables" should "be in an unbound state" in {
    List(X,Y,Z).foreach(_.bound should be (None))
  }
  
  "An empty state" should "unify with all other states" in {
    val s = ub.EMPTY_STATE
    Binding(s.unifyWith(b.EMPTY_STATE)).possible should be (true)
    Binding(s.unifyWith(b.ONLY_A)).possible should be (true)
    Binding(s.unifyWith(b.ABC_555)).possible should be (true)
  }
  
  it should "bind no variables when unifying with other states" in {
    val s = ub.EMPTY_STATE
    s.unifyWith(b.EMPTY_STATE) should be (Some(Nil))
    s.unifyWith(b.ONLY_A) should be (Some(Nil))
    s.unifyWith(b.ABC_555) should be (Some(Nil))
  }
  
  "A(X)" should "not unify with an empty state" in {
    Binding(ub.ONLY_A.unifyWith(b.EMPTY_STATE)).possible should be (false)
  }
  
  it should "not unifiy with B(7)" in {
    Binding(ub.ONLY_A.unifyWith(b.ONLY_B)).possible should be (false)
  }
  
  
  "Unifying A(X) with A(3)" should "be possible" in {
    Binding(ub.ONLY_A.unifyWith(b.ONLY_A)).possible should be (true)
  }
  
  it should "bind exactly one variable" in {
    Binding(ub.ONLY_A.unifyWith(b.ONLY_A)).length should be (1)
  }
  
  it should "bind X = 3" in {
    Binding(ub.ONLY_A.unifyWith(b.ONLY_A))(X) should be (Some(3))
  }
  
  
  "Unifying A(X) with A(5) B(5) C(5)" should "be possible" in {
    Binding(ub.ONLY_A.unifyWith(b.ABC_555)).possible should be (true)
  }
  
  it should "bind exactly one variable" in {
    Binding(ub.ONLY_A.unifyWith(b.ABC_555)).length should be (1)
  }
  
  it should "bind X = 5" in {
    Binding(ub.ONLY_A.unifyWith(b.ABC_555))(X) should be (Some(5))
  }
  
  
  "Unifying A(X) with A(1) A(2) A(3)" should "be possible" in {
    Binding(ub.ONLY_A.unifyWith(b.AAA_123)).possible should be (true)
  }
  
  it should "bind exactly one variable" in {
    Binding(ub.ONLY_A.unifyWith(b.AAA_123)).length should be (1)
  }
  
  //The following test assumes determinsitic binding of variables
  it should "bind X = 1" in {
    Binding(ub.ONLY_A.unifyWith(b.AAA_123))(X) should be (Some(1))
  }
  
  
  "A(X) B(X)" should "not unify with emty state" in {
    Binding(ub.AB_XX.unifyWith(b.EMPTY_STATE)).possible should be (false)
  }
  
  it should "not unify with A(3)" in {
    Binding(ub.AB_XX.unifyWith(b.A_3)).possible should be (false)
  }
  
  it should "not unify with A(1) A(2) A(3)" in {
    Binding(ub.AB_XX.unifyWith(b.AAA_123)).possible should be (false)
  }
  
  
  "Unifying A(X) B(X) with A(5) B(5) C(5)" should "be possible" in {
    Binding(ub.AB_XX.unifyWith(b.ABC_555)).possible should be (true)
  }
  
  it should "bind exactly one variable" in {
    Binding(ub.AB_XX.unifyWith(b.ABC_555)).length should be (1)
  }
  
  it should "bind X = 5" in {
    Binding(ub.AB_XX.unifyWith(b.ABC_555))(X) should be (Some(5))
  }
  
  //This test requires backtracking work correctly
  "Unifying A(X) B(X) with A(1) A(2) B(2)" should "be possible" in {
    Binding(ub.AB_XX.unifyWith(b.AAB_122)).possible should be (true)
  }
  
  it should "bind exactly one variable" in {
    Binding(ub.AB_XX.unifyWith(b.AAB_122)).length should be (1)
  }
  
  it should "bind X = 2" in {
    Binding(ub.AB_XX.unifyWith(b.AAB_122))(X) should be (Some(2))
  }
  
  //All-Different constraint
  "Unifying A(X) B(Y) C(Z) with A(5) B(5) C(5)" should "not be possible  (all-different constraint)" in {
    Binding(ub.ABC_XYZ.unifyWith(b.ABC_555)).possible should be (false)
  } 
  
  //This test is more a debugging test (if binding succeeds, the user can see what got bound)
  it should "not bind any variables" in {
    Binding(ub.ABC_XYZ.unifyWith(b.ABC_555)).boundVars should be (Nil)
  }
  
  "Unifying A(X) B(Y) C(Z) with A(1) B(2) C(3)" should "be possible" in {
    Binding(ub.ABC_XYZ.unifyWith(b.ABC_123)).possible should be (true)
  }
  
  it should "bind exactly three variables" in {
    Binding(ub.ABC_XYZ.unifyWith(b.ABC_123)).length should be (3)
  }
  
  it should "bind X = 1, Y = 2, Z = 3" in {
    val binding = Binding(ub.ABC_XYZ.unifyWith(b.ABC_123))
    
    binding(X) should be (Some(1))
    binding(Y) should be (Some(2))
    binding(Z) should be (Some(3))
  }
}