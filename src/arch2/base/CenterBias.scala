package arch2.base

/** A trait which provides a center bias for action selection.
 *  
 *  The motivation for this trait was to factor out the selection behavior out of the GreedyPredicateAI to be able
 *  to swap it out with a different one. This was done to make the results more reliable by making the GreedyPredicateAI into a 
 *  deterministic strategy.  
 */
trait CenterBias {
  def selectRandomActionWithCenterBias(actions:List[Int]):Int
}



trait RandomizedCenterBias extends CenterBias {
  import scala.util.Random
  
  /** This method selects a random action but prefers actions which are close to the 
   *  center column strongly. (Closest 2 actions have together a 75% chance of being selected)
   *  
   *  @param actions the list of available actions (should not be empty)
   *  
   *  @return one of these actions 
   */
  def selectRandomActionWithCenterBias(actions:List[Int]):Int = {
    val resorted = actions.sortBy(i => Math.abs(3 - i)).toArray //Order by distance to center
    
    val random = Random.nextDouble
    var currentUpperBound = 0d 
    var i = 0
    
    while(i < resorted.length) {
      currentUpperBound += Math.pow(0.5, i+1) // Probability function: P(i) = 0.5^(i+1)
      if (random < currentUpperBound)
        return resorted(i)
      
      i += 1
    }
    
    return resorted(resorted.length - 1)
  }
}

trait StrictCenterBias extends CenterBias {
  
  /** Just return the action which is closest to the center
   */
  def selectRandomActionWithCenterBias(actions:List[Int]):Int = actions.minBy(x => Math.abs(3 - x))
}