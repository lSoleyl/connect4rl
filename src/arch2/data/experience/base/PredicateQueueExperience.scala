package arch2.data.experience.base

import arch2.base.predicates._
import utils.Log
import scala.collection.mutable.{Map,Queue}
import utils.Options._

/** The motivation behind this class is the logical argument that the states which are visited most often
 *  are discovered early. Using a Queue instead of a List reduces match-calls enourmously. 
 *  Eg: The initial state is visited each episode. Prepending new states will cause the match-algorithm to
 *      check every previous state whether it matches. So matching the initial state would always take n match calls.
 */
trait PredicateQueueExperience[A,V] extends Experience[UnboundPredicateState[A],Variable[A],V] with Log {
  
  type ActionMap = Map[Variable[A],V]
  type BoundVariables = List[BoundVariable[A]]
  
  val defaultVariable:Variable[A] //Variable for random action '?'
  val defaultVariableStartValue:V
  
  case class ListEntry(state:State, actionMap:ActionMap, var matchCount:Long = 0, var failedMatches:Long = 0)
  case class MatchResult(entry:ListEntry, boundVariables:BoundVariables)  
  
  var failedCounter:Int = 0
  
  protected var experience:Queue[ListEntry] = Queue()
  
  override def extractFirstMatch[X](matcher:State=>Option[X]):Option[ExtractorMatch[X]] = {
    var failed:Long = 0
    
    if (failedCounter > 100000) {
      failedCounter = 0
      logDebug("Reordering experience by matchCount")
      experience = experience.sortBy(_.matchCount).reverse
    }
    
    for(entry <- experience) {
      val matchData = matcher(entry.state)
      if (matchData) {
        entry.matchCount += 1
        entry.failedMatches += failed
        return Some(ExtractorMatch(matchData.get,entry.state))
      }
      failedCounter += 1
      failed += 1
    }
    return None
  }
  
  
  override def containsState(state:State) = !experience.find(_.state == state).isEmpty
  
  /** With this method the caller can add a new entry into the list and gets a ListEntry returned, which 
   *  he then can manipulate without matching the state again.
   *  
   * @param state the state to add
   * 
   * @return the newly created listEntry
   */
  def enterState(state:State):ListEntry = {
    val actionMap:ActionMap = Map()
    state.getBoundActions.foreach(actionMap(_) = default) //Populate Entry
    actionMap(defaultVariable) = defaultVariableStartValue
    
    val entry = ListEntry(state,actionMap)
    experience.enqueue(entry)
    
    entry
  }
  
  
  override def enterState(state:State, actions:List[Action]) {
    val actionMap:ActionMap = Map()
    actions.foreach(actionMap(_) = default)
    actionMap(defaultVariable) = defaultVariableStartValue
    
    experience.enqueue(ListEntry(state,actionMap))
  }
  
  
  override def bestActionEntry(state:State, allowedActions:List[Action], isBetter:(Value,Value)=>Boolean):Entry = {
    val entry = experience.find(_.state == state)
    
    if (!entry)
      return Entry(state, allowedActions.head, default)
      
    logTrace("StateEntry: " + entry.get)
      
    val (action,value) = entry.get.actionMap.reduce((av1,av2) => if (isBetter(av1._2,av2._2)) av1 else av2)
    Entry(state, action ,value)
  }
  
  
  override def apply(key:SA):Value = {
    for(entry <- experience) {
      if (entry.state == key.s)
        return entry.actionMap.getOrElse(key.a, default)
    }
    
    return default
  }
  
  override def update(key:SA, value:Value) {
    val entry = experience.find(_.state == key.s) default enterState(key.s)
    
    entry.actionMap(key.a) = value
  }
  
  override def stateCount:Option[Int] = Some(experience.length)  
  override def toString = experience.toString
}