package arch2.gui

import scala.swing._
import java.awt.Color

class AlphaInfoWindow extends Frame {
  title = "Info"
  contents = new FlowPanel {
    background = Color.white
    contents += new Label("""<html>
      <center><h2>Dynamic learn rate</h2></center>
      <p>
        The dynamic learn rate option causes the learn rate to be decreased with increasing episodes.<br/>
        This is a requirement to theoretically ensure convergence to &pi;*<br/>
        This process can be configured by setting the variables &alpha;<sub>0</sub> and e<sub>2</sub>
      </p>
      <p>
        The learn rate &alpha; for an episode e is calculated as follows:<br/>
        &nbsp;&nbsp;&alpha;(e) = &alpha;<sub>0</sub> &bull; 0.5<sup>limit(e)</sup><br/>
        &nbsp;&nbsp;limit(e) = e : e<sub>2</sub>
      </p>
      <p>
        &alpha;<sub>0</sub> - Initial learn rate for episode 0<br/>
        e<sub>2</sub> - Halftime episode. The episode at which &alpha;(e) = &alpha;<sub>0</sub> &bull; 0.5
      </p>
    </html>""")
  }
}