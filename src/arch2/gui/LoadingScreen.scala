package arch2.gui

import java.io.File
import scala.swing._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import arch2.base.Player
import arch2.data.AILoader
import utils.Options._
import utils.Log
import java.io.OutputStream
import java.io.InputStream

class LoadingScreen(loader:AILoader) extends Frame with Log {
  val progress = new ProgressBar
  private var opened = false
  
  val text = new Label("Please wait while AI '" + loader + "' is being loaded...")
  
  contents = new BoxPanel(Orientation.Vertical) {
    contents += text
    contents += progress
  }
  
  override def open {
    if (!opened) {
      opened = true
      super.open
    }
  }
  
  /** Display the loading screen (only if necessary), load the player data
   *  and return the player 
   */
  def load(from:Option[InputStream] = None) = {
    text.text = "Please wait while AI '" + loader + "' is being loaded..."
    try {
      loader.loadFrom(from, new ProgressUpdater(progress, this)) //<- Don't specify a source... will either create empty agent, of load from bound source
    } catch {
      case e:Throwable => log.error(s"Failed to load $loader!", e)
    }
    
    Swing.onEDT(close)
  }
  
  
  def save(out:OutputStream) {
    text.text = "Please wait while AI '" + loader + "' is being saved..."
    loader.save(Some(out), new ProgressUpdater(progress, this))
    Swing.onEDT(close)
  }
  
  /** Save the agent to a certain file or the default location
   */
  def save(file:Option[File]) {
    text.text = "Please wait while AI '" + loader + "' is being saved..."
    
    if (!file) {
      loader.save(None, new ProgressUpdater(progress, this))
    } else {
      loader.saveToFile(file.get, new ProgressUpdater(progress, this))
    }
    Swing.onEDT(close)    
  }
}