# Connect4 RL

A simple connect four implemenation with different types of AI, which was written in Scala. This program is taylored towards observing the success rate and state count of learning agents over a long period of about 10 million episodes. This program has multiple features to support this:
 * Saving/Loading learned agents into/from files.
 * Simple batch runs (in contrast to UI runs, which are also supported).
 * Multi batch runs = 1 Batchrun which is made up of multiple simple batch runs.
 * Export of multi batch run results into a file format, which is human readable and can be read by Matlab.
 * Display of current simulation speed (episodes/sec) and the estimated remaining time for each batch run and a progress bar.
 * Pausing long running multi batch runs (by closing the window), what causes the run to save all necessary information (includes the agents, batch run state, and all game parameters) into a single file, from which the run can be resumed at any later point.
 * An agent window to display, which agents are currently loaded, how many episodes they have been trained and how many states (if any) the have learned.
 * Export of intermediate results from a yet incomplete multi batch run into the matlab format to see how the agents played so far.
 * A rather simple configuration GUI to access and change all used game parameters.

Nontheless this is a GUI application and can be used for interactive play. If the batch run is disabled (configuration) the game board will be drawn and the agents will take turns to throw stones into the slots (columns). (This is delayed by 500ms to be able to follow the game)
If a human player (Agents: `Player`, `PredicatePlayer`) plays, buttons will be displayed at the top, one for each free slot.


## Agents
Here a list of implemented agents.

### Human agents
 * `Player` - Nothing special, just select the slot to throw the stone into
 * `PredicatePlayer` - If launched from a console a Predicate description of the current state

### Heuristic agents
 * `RandomAI` - Just selects random slots to throw into
 * `SimpleAI` - Just selects the leftmost available slot to throw into
 * `GreedyPredicateAI` - Uses the same state description like `PredicatePlayer` and selects the slot, which is suggested by the first predicate. (If multiple predicates are similarly good, it chooses the center)

### Learning agents
 * `MCLearningAI` - A simple learning agent, using the Monte-Carlo learn algorithm.
 * `QLearningAI` - A simple learning agent, using the Q-Learning algorithm.
 * `PQLearingAI` - A QLearning agent, which uses the predicate state space as base.
 * `DCQLearningAI` - A more sophisticated version of `PQLearningAI` which aims to learn faster with less states and abstracting state details.
 * `SDCQLearningAI` - Same as `DCQLearningAI` but without any prior knowledge of the domain.
 * `DCQLearningAI2` - Same as `DCQLearningAI` but without the reduced predicate state.
 * `SDTQLearningAI` - Same as `DCQLearningAI` but uses a tree instead of a decision list for faster state lookup. (Only effects simulation speed)
 * `DTQLearningAI`  - Same as `SDCQLearningAI` but additionally reorders the tree's nodes. This also affects the learning algorithm but further improves the simulation speed.
 * `MMQLearningAI` - Uses the iterative/sampling approach of QLearning but the evaluation of paths/states approach of Min-Max. Learn a bit more efficient than `QLearningAI`.
 * `MMMCLearningAI` - The evaluations of paths/states approach from Min-Max combined with an history-update approach of Monte-Carlo. This agent outperforms most other agents in terms of play strength, but also learns a huge amount of states.
 * `MMMCDLearningAI` - This agent uses the information of one episode fully by additionally learning the enemies decisions. It learns slightly faster, but uses up much more states for this.
 

## Build
To build this project, you will need the Scala-Build-Tool, which can be downloaded [here](http://www.scala-sbt.org/).
This Project was developed and tested unter Scala 2.10.4 and might not work properly when build with a different version.

To create a runnable .jar file, start `sbt` in the project's directory and enter `assembly`.
Then you can launch the program via the provided .bat file or you can aswell start it by executing the `run` command in `sbt`. 

With `test` you can additionally run the unittests provided with this project.
    
## Misc

Should be used with a 64-Bit JRE and giving it 4GB or more memory is a good way to speed up long runs of learning agents and prevent the garbage collection from constantly interfering.