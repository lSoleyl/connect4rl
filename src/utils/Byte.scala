package utils

object Byte {
  implicit def toByte(i:Int):Byte = i.asInstanceOf[Byte]
  implicit def toByte(i:Long):Byte = i.asInstanceOf[Byte]
}