package arch2.data.experience

import java.io._
import utils.Log
import utils.Options._
import arch2.data.ProgressInfo
import utils.io.ByteWriter
import utils.io.ByteReader

/** This class provides an additional state variable which will be available if this trait
 *  is mixed in together with the experience class. 
 *  If the header isn't recognized then the header is unread again and passed the the trait's base class
 *  
 *  To add a new Header type, just extend HeaderV1 and abstract override loadFromStream/writeToStream and define a new version constant
 *  
 *  Header data structure:
 *  [Header-Constant(4)][Header-Version(4)][episodes(8)][agentType(???)][agentDescription(???)]
 * 
 */
trait HeaderV1 extends StreamSerializable with Log {
  var episodes:Long = 0
  var agentType:Option[String] = None
  var agentdescription:Option[String] = None
  
  abstract override def loadFromStream(in:InputStream, progress:ProgressInfo) {
    val pbIn = new PushbackInputStream(in, 16)
    if (!hasCorrectHeaderVersion(pbIn, HeaderV1.HEADER_VERSION)) {
      log.info("No match for V1 header in data stream")
      super.loadFromStream(pbIn, progress)
    } else { //Load header data
      val reader = new ByteReader(pbIn)
      episodes = reader.readLong
      agentType = reader.readString.noneIf(_.isEmpty)
      agentdescription = reader.readString.noneIf(_.isEmpty)
      
      super.loadFromStream(pbIn, progress) //Load stuff of base classes
    }
  }
  
  abstract override def writeToStream(out:OutputStream, progress:ProgressInfo) {
    val writer = new ByteWriter(out)
    writer.writeInt(HeaderV1.HEADER_CONSTANT)
    writer.writeInt(HeaderV1.HEADER_VERSION)
    writer.writeLong(episodes) 
    writer.writeString(agentType default "")
    writer.writeString(agentdescription default "")
    
    super.writeToStream(out, progress) //Let base classes write their stuff (experience)
  }
  
  /** This method checks whether a header is available and whether it's version is correct
   * 
   * @param input the input to read from
   * @param versionID the 4-Byte-Constant as header version
   */
  def hasCorrectHeaderVersion(input:PushbackInputStream, versionID:Int):Boolean = {
    if (hasHeader(input)) {
      val bytes = new Array[Byte](4)
      input.read(bytes)
      if (ByteReader.bytesToInt(bytes) != versionID) {
        input.unread(bytes)
        input.unread(ByteWriter.intBytes(HeaderV1.HEADER_CONSTANT)) //Also unread the header constant
        false      
      } else {
        true
      }
    } else {
      log.info("No header found! Missing Header constant.")
      false
    }
  }
  
  
  /** This method checks whether the learn data contains a header at all (initial bytes: 0xffffff)
   *  
   *  @param input the inputstream to read from 
   */
  private def hasHeader(input:PushbackInputStream):Boolean = {
    val bytes = new Array[Byte](4)
    input.read(bytes)
    if (ByteReader.bytesToInt(bytes) != HeaderV1.HEADER_CONSTANT) {
      input.unread(bytes)
      false
    } else { 
      true
    }
  }
  
}

object HeaderV1 {
  import utils.Byte._
  
  val HEADER_CONSTANT:Int = 0xFFFFFFFF
  val HEADER_VERSION:Int  = 0x00000001
}