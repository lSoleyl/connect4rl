package arch2.gui

import scala.swing._
import utils.Log
import utils.TimeFormat

/** Progress screen for batch runs
 *  
 *  @param episodes the total amount of episodes in this batchrun
 */
class BatchProgressScreen(episodes:Int, titleMessage:String = "Batch run in progress") extends Frame with Log with AbortableFrame {
  private var opened = false
  private val progBar = new ProgressBar
  val progress = new ProgressUpdater(progBar, this)
  progress.setTotal(episodes)
  title = titleMessage
  
  val speed = new Label("      0 episodes/sec") { preferredSize = new Dimension(200,20) }
  val timeLeft = new Label("Estimated time remaining: unknown.")
  var measuring = true

  contents = new BoxPanel(Orientation.Vertical) {
    contents += new FlowPanel { 
      contents += new Label("Running current batch")
      contents += speed
      contents += timeLeft
    }
    contents += progBar
  }
  
  override def open {
    if (!opened) {
      opened = true
      super.open
    }
  }
  
  
  def start {
    Swing.onEDT { this.open; this.centerOnScreen }
    
    measuring = true
    new Thread(this.getClass().getSimpleName() + "-Updater") {
      override def run {
        var lastCount = 0L
        while(measuring) {
          Thread.sleep(1000) //update every second
          val newCount = progress.currentCount
          val delta = Math.max(newCount - lastCount, 1)
          log.info(s"Current speed: $delta episodes/sec")
          
          val estimatedSeconds = (progress.totalCount - progress.currentCount) / delta
          
          
          Swing.onEDT { 
            speed.text = f" $delta%6d episodes/sec"
            timeLeft.text = "Estimated time remaining: " + TimeFormat.hhmmss(estimatedSeconds)
          }
          lastCount = newCount
        }
      }
    }.start()
  }
  
  def stop {
    measuring = false
    log.info("Stopped measuring game speed")
  }
}