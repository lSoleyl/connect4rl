package arch2.base

import arch2.field._
import arch2.base.predicates.ChainingPredicate
import arch2.base.predicates.BlockPredicate
import arch2.base.predicates.BoundPredicateState

/** The motivation behind this class was to keep the necessary changes, which had to be made to the 
 *  PredicateState for PQLearningAI, isolated from the original PredicateState implementation, which is
 *  in use by GreedyPredicateAI and already covered by a lot of tests.
 *  
 *  This trait extends the predicate state description far enough so that SPQLearning has a chance of defeating 
 *  GreedyPredicateAI in the same way, QLearningAI did it.
 *  
 *  It adds predicates of length 1 to the initial state (precomputed for performance) and to every state which consists of only
 *  predicates of length 2.
 *  
 *  Also predicates of length 2 are already filled in, the state has exactly one predicate of length >3. The argument here was that the agent has no "choice" in that state
 *  so that he can't really learn anything... (learning that the random action is best, would be fatal)
 */
trait ExtendedPredicateState extends PredicateState {    
  
  val columnList = (0 until arch2.game.Field.columns).toList 
  val initialState:BoundPredicateState[Int] = columnList.map(ChainingPredicate(1,_))
  
  /** This method takes a field state and converts it into an ORDERED predicate list.
   *  
   *  @param state the current field state to convert
   *  @param actions the list of available actions to save a bit time during the creation of the predicate list
   *  @param color the color of the current player. This is important, because it switches the predicates' meaning
   * 
   */
  protected override def convertState(state:FieldState, actions:List[Int], color:Stone):BoundPredicateState[Int] = {
    val other = Stone.switch(color)
    
    
    val lines = actions.flatMap(action => (state.getLines(action, color) ++ state.getLines(action, other)).map(line => (action, line)))
    val predicates:BoundPredicateState[Int] = lines.filter(_._2.length >= 2).distinct.map(acLine => if (acLine._2.color == color) ChainingPredicate(acLine._2.length, acLine._1) else BlockPredicate(acLine._2.length, acLine._1))
    
    if (predicates.isEmpty)
      return initialState
    
    val partition = predicates.partition(_.chainLength > 2)
        
    if (partition._1.length >= 3) //Add predicates of length 3 if the agent might not have possible actions to "learn"
      partition._1.sorted
    else {
      val unboundColumns = state.getFreeColumns diff predicates.map(_.column)
      predicates.sorted ++ unboundColumns.map(ChainingPredicate(1,_))
    }
  }
}