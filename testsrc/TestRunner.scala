import predicates._

object TestRunner {
  def main(args:Array[String]) {
    val suite = new PredicateMatching
    suite.execute
  }
}