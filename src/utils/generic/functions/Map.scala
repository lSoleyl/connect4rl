package utils.generic.functions

trait Map[M[_]] {
  def apply[A,B](ma:M[A], f:A=>B):M[B]
}


object Map {
  implicit object ListMap extends Map[List] {
    def apply[A,B](ma:List[A], f:A=>B):List[B] = ma.map(f)
  } 
  
  
}