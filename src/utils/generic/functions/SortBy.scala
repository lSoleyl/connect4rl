package utils.generic.functions

trait SortBy[M[_]] {
  def apply[A,B](ma:M[A], selector:A=>B)(implicit ordering:Ordering[B]):M[A]
}

object SortBy {
  implicit object ListSortBy extends SortBy[List] {
    override def apply[A,B](list:List[A], selector:A=>B)(implicit ordering:Ordering[B]) = list.sortBy(selector)
  }
}