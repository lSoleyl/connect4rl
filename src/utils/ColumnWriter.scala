package utils

import java.io.OutputStream
import java.io.IOException
import scala.annotation.tailrec
import Options._
import java.io.OutputStreamWriter


class ColumnWriter(out:OutputStream, options:ColumnWriter.Options = ColumnWriter.Options()) extends AutoCloseable {
  private val  writer = new OutputStreamWriter(out, options.encoding)
  private var isWritable = true
  var columns:List[List[String]] = Nil
  
  /** Add a column of string to the ColumnWriter. This doesn't cause any
   *  IO operation because the writer doesn't know how many column will be written in total.
   *  
   *  @param column the list of entries to write to the file
   */
  def addColumn(column:List[String]) {
    if (!isWritable) {
      throw new IOException("Column can't be added after data has been written to the underlying stream")
    }
    
    if (options.forceEqualColumns) {
      columns.headOption iff { firstColumn =>
        if (firstColumn.length != column.length)
          throw new Exception("Column lengths don't match. First column has " + firstColumn.length + " entries, new column has " + column.length + " entries!")
      } 
    }
    
    columns = column :: columns
  }
  
  /** Write all columns to the underlying stream
   *  After this operation the addColumn function can't be used anymore to add new columns 
   */
  def flush {
    isWritable = false
    writeLines(columns.reverse)
    writer.flush
  }
  
   
  /** Tailrecursive helper function to write all columns row wise into the underlying stream
   */
  @tailrec
  private def writeLines(cols:List[List[String]]) {
    if (cols.exists(_ != Nil)) { //Terminate condition (All columns are empty)
      val currentLine = cols.map(_.headOption default options.emptyValue)
      writer.write(currentLine.mkString(options.columnSeparator) + options.rowSeparator)
      writeLines(cols.map(column => if (column == Nil) Nil else column.tail))
    }
  }
  
  
  def close {
    if (isWritable)
      flush
    
    writer.close
  }
}



object ColumnWriter {
  case class Options(columnSeparator:String = " ",     //How should columns get separated 
                     rowSeparator:String = "\n",       //How should rows get separated
                     encoding:String = "UTF-8",        //What encoding to use
                     emptyValue:String= " ",           //If a column is shorter than others what values should it be padded with?
                     forceEqualColumns:Boolean = true) //This causes the writer to throw an exception if the column lengths don't match
}