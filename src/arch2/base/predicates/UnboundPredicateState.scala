package arch2.base.predicates

import utils.Options._
import arch2.data.Serializer

class UnboundPredicateState[T](val predicates:List[Predicate[Variable[T]]]) {
  val variables = predicates.map(_.argument).toSet.toList
  val ids = predicates.map(_.id).toSet
  import scala.collection.mutable.Map
  val constraints:Map[Variable[T],Constraints[T]] = Map() //TODO make this optional by mixing in a trait, so that PQLearning won't suffer a performance penalty because of this
  variables.foreach(constraints(_) = Constraints())
  
  type FreePredicate = Predicate[Variable[T]]
  type BoundPredicate = Predicate[T]
  
  /** Returns a list of Bound variables if this state matches the given state.
   *  Otherwise it returns None. 
   *  
   * @param state the state to match to
   * 
   * @return A list of bound variables, if the state descriptions match,
   *         otherwise None
   */ 
  def matchWith(state:BoundPredicateState[T]):Option[List[BoundVariable[T]]] = {
    if (state.length != predicates.length) 
      None //No direct matching possible
    else if(state.length == 0) //Match empty states together
      Some(Nil)
    else if(state.head.id != predicates.head.id) //This condition exploits predicate ordering must be checked after it is known that both states are not empty
      None
    else {
      tryMatch(predicates.head, predicates.tail, state)
    }
  }
  
  
  /** Adds a new constraint to the constraint list for the given variable.
   *  No Checks are performed whether the constraint creates a cyclic dependency or whether
   *  the variable even exists in the UPS.
   *  
   *  @param v the variable on which the constraint depends
   *  @param c the constraint itself  
   *  
   *  @return this for fluent usage
   */
  def constrain(v:Variable[T], c:Constraint[T]) = { 
    constraints(v) = c :: constraints(v) 
    this
  } 
  
  private def canBindTo(p:FreePredicate, bp:BoundPredicate) = p.id == bp.id && p.argument.isBindableTo(bp.argument)
  private def canBindToAllDifferent(p:FreePredicate, bp:BoundPredicate) = canBindTo(p,bp) && variables.forall(v => if (v != p.argument) v.bound.mapOr(true)(_.value != bp.argument) else true)
  
  /** Recursive helper method to match this state to a given bound state
   *  
   *  @param p the current predicate to find a binding for
   *  @param tail the remaining predicates to find a binding for
   *  @param state the state to match to
   *  
   *  @return if the binding was successfull, then it will return a list of bound variables, 
   *          otherwise None
   */
  private def tryMatch(p:FreePredicate, tail:List[FreePredicate], state:List[BoundPredicate]):Option[List[BoundVariable[T]]] = {
    val bindablePredicates = state.filter(canBindTo(p, _))
    
    for(candidate <- bindablePredicates) {
       if (p.argument.bind(candidate.argument)) {
         
         if (tail.isEmpty) { //Matching was successful
           val result = Some(variables.map(_.bound.get))
           p.argument.release //Release variable binding before returning
           return result //This call implicitly asserts that all variables are bound
         } else {
           val result = tryMatch(tail.head, tail.tail, state diff (candidate :: Nil))
           if (!result.isEmpty) { //This binding was successful
             p.argument.release
             return result
           } 
         }       
         
         p.argument.release
       }
    }
    
    return None
  }
  
  
  /** This method will try to unify the state with the passed state and return a list of BoundVariables.
   *  The BoundPredicateState passed to this method is treated as the knowledgebase for the unification.
   *  The unification unifies while assuming an all-different constraint for the variables, because 
   *  some states chould not be distinguished otherwise.
   *  
   *  @param state the bound state to unify with
   *  
   *  @return None if unification is not possible. Otherwise a List containing all bound variables  
   */
  def unifyWith(state:BoundPredicateState[T]):Option[List[BoundVariable[T]]] = {
    if (predicates.length == 0) //Empty unbound can unify with every other state
      return Some(Nil)
    
    //FIXME speedup possible, because state and id's can be sorted the same way... a filter with sorted sequences should be faster
    val filtered = state.filter(p => ids.contains(p.id)) //ignore irrelevant predicates
    
    if (predicates.length > filtered.length) //state's predicates must be a superset of this one's to unify
      None
    else
      tryUnify(predicates.head, predicates.tail, filtered)
  }
  
  /** Recursive helper method to unify this unbound state with the given bound state. 
   * 
   */
  private def tryUnify(p:FreePredicate, tail:List[FreePredicate], state:List[BoundPredicate]):Option[List[BoundVariable[T]]] = {
    val bindablePredicates = state.filter(canBindToAllDifferent(p, _))
    
    for(candidate <- bindablePredicates) {
       if (p.argument.bind(candidate.argument)) {
         val boundVariable = p.argument.bound.get
         val constraint = constraints(p.argument)
         if (constraint.evaluate(boundVariable)) { //All constraints were successfully evaluated
           
           if (tail.isEmpty) { //Matching was successful
             val result = Some(variables.map(_.bound.get))
             constraint.release //Release variables, bound by constraints before returning
             p.argument.release //Release variable binding before returning
             return result //This call implicitly asserts that all variables are bound
           } else {
             val result = tryUnify(tail.head, tail.tail, state)
             if (!result.isEmpty) { //This binding was successful
               constraint.release
               p.argument.release
               return result
             } 
           }
           
          constraint.release 
         }        
         
         p.argument.release
       }
    }
    
    return None
  }
  
  
  
  override def hashCode:Int = predicates.hashCode
  override def equals(x:Any) = {
    if (x.isInstanceOf[UnboundPredicateState[T]])
      predicates.equals(x.asInstanceOf[UnboundPredicateState[T]].predicates) && constraints.equals(x.asInstanceOf[UnboundPredicateState[T]].constraints)
    else
      false
  }
  
  def printConstraints:String = {
    import utils.Convert._
    val nonEmptyConstraints = constraints.mapValues(_.toList).toList.filter(vc => !vc._2.isEmpty)
    val stringList = nonEmptyConstraints.map(vc => vc._2.map(c => c.variable.toString + " = " + vc._1 + c.offsetString).mkString(", "))
    stringList.mkString(", ").changeIf(_ != "")(" | " + _) //Only prepend pipe character if the state actually contains constraints
  }
  
  /** This method retrieves all actions, which are bound by this predicate
   */
  def getBoundActions():List[Variable[T]] = predicates.map(_.argument).sortBy(_.id).distinct
  
  /** Same as above, but already creates an actionMap
   */
  def getActionMap[V](defaultValue:V) = Map(getBoundActions.map((_,defaultValue)) :_*)
  
  override def toString = if (predicates == Nil) "true" else (predicates.mkString(" ") + printConstraints)
}



object UnboundPredicateState {
  
  //Serializer for unbound predicate state
  implicit def serializer[T](implicit ps:Serializer[Predicate[Variable[T]]], cs:Serializer[(Variable[T],List[Constraint[T]])]):Serializer[UnboundPredicateState[T]] = new Serializer[UnboundPredicateState[T]]{
    override def write(ups:Type)(implicit writer:ByteWriter) {
      import writer._
      import ups._
    
      writeByte(predicates.length.toByte) //255 should be a reasonable limit for predicates in a state
      for (predicate <- predicates) {
        ps.write(predicate)
      }
     
      val cList = constraints.toList
      writeByte(cList.length.toByte)
      for((v,constraints) <- cList) {
        cs.write((v,constraints.toList))
      }
    }
    
    override def load()(implicit reader:ByteReader) = {
      import reader._
    
      val pCount = readByte
        
      val predicates = (0 until pCount).map(i => {
        ps.load
      }).toList
      
      val state = new UnboundPredicateState[T](predicates)
      
      val cCount = readByte
      
      (0 until cCount).foreach(i => {
        val (v,cl) = cs.load
        cl.foreach(c => state.constrain(v, c))
      })
      state
    }
  }
  
  /** This method plainly derives a "half-abstract" state from the given bound state by
   *  assigning variables to the bound values.
   */
  def deriveState[T](state:BoundPredicateState[T])(implicit varFactory:Int => Variable[T]):(UnboundPredicateState[T],List[BoundVariable[T]]) = {
    import scala.collection.mutable.Map
    
    val variables:Map[T,Variable[T]] = Map()
    var varid = 0
    
    val predicates = state.map(predicate => {
      if (!variables.contains(predicate.argument)) {
        variables(predicate.argument) = varFactory(varid)
        varid += 1
      }
      predicate.replaceArgument(variables(predicate.argument))
    })
    
    val boundVariables = variables.map(kv => BoundVariable(kv._2, kv._1)).toList
    (new UnboundPredicateState(predicates), boundVariables)
  }
}