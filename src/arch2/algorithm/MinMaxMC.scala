package arch2.algorithm

import arch2.data.experience._
import arch2.data.experience.base._
import arch2.base._
import scala.annotation.tailrec
import utils.Log

class MinMaxMC[S,A](val experience:Experience[S,A,Double] with HeaderV1) extends Agent[S,A,Double] with Log {  
  var episodeLog:List[(Entry,List[A])] = Nil
  var enemyEpisodeLog:List[(Entry,List[A])] = Nil
  
  protected def isBetter(v1:Value, v2:Value) = v1 > v2
  
  def selectAction(state:S, reward:Double, possibleActions:List[A]):A = {
    val bestAction = bestActionEntry(state, possibleActions)
    
    episodeLog = (bestAction,possibleActions) :: episodeLog    
    bestAction.sa.a //Return the selected action
  }
  
  def finalReward(reward:Double) {
    update(reward, episodeLog)
    episodeLog = Nil
    
    experience.episodes += 1
  }
  
  //Copy-interface to copy the enemy's playstyle
  def enemyTurn(state:S, action:A, actions:List[A]) {
    val sa = experience.SA(state,action)
    val v = experience(sa)
    val entry = experience.Entry(sa,v)
    
    enemyEpisodeLog = (entry, actions) :: enemyEpisodeLog
  }
  
  def enemyFinalReward(reward:Double) {
    update(reward, enemyEpisodeLog)
    enemyEpisodeLog = Nil
  }
  
  @tailrec
  private def update(nextQ:Double, history:List[(Entry,List[A])]) {
    //Only update q value if reward was worse than anticipated or previous q value was zero and the reward is > 0
    val (entry,actions) = history.head
    val oldQValue = entry.v
    if (nextQ < oldQValue || (oldQValue == 0d && nextQ > 0)) { //Value gets changed
      experience(entry.sa) = nextQ
    }
    
    //calculate effect on V value of this update
    val newQValue = bestActionEntry(entry.sa.s, actions).v
    
    //Update predecessors if any (this cannot be optimized by cutting, because this would lead to stopping learning in certain conditions)
    if (history.tail != Nil) {
      update(newQValue, history.tail) //update predecessor states
    } 
  }
  
  /** Will be called once before every GUI game or before a batch run
   *  
   *  @param params the configuration which has been set
   */
  def initialize(params:LearningParameters) { episodeLog = Nil; enemyEpisodeLog = Nil }
}