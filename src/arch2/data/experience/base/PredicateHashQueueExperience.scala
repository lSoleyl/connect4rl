package arch2.data.experience.base

import scala.collection.mutable.{Queue, Map}
import arch2.base.predicates._
import utils.Log
import utils.Options._

/** The idea behind implementing this class was the observation that a state is only bindable to another state if both
 *  states start (thanks to the ordering) with the same predicate. 
 *  Therefore to reduce the number of states which have to be checked for each match, all states are grouped by their first predicate id.
 *  Since most of the time is spend in the matching method, this should give a good speedup
 *  
 *  This turned out to only slow down the learning process and instead of a speed gain, the agent learned ~20% slower
 */
trait PredicateHashQueueExperience[A,V] extends Experience[UnboundPredicateState[A],Variable[A],V] with Log {
  
  type ActionMap = Map[Variable[A],V]
  type BoundVariables = List[BoundVariable[A]]
  
  val defaultVariable:Variable[A] //Variable for random action '?'
  val defaultVariableStartValue:V
  
  case class ListEntry(state:State, actionMap:ActionMap)
  case class MatchResult(entry:ListEntry, boundVariables:BoundVariables)  
  
  protected var experience:Map[String,Queue[ListEntry]] = Map()
  
  override def containsState(state:State) = {
    val queue = experience.get(state.predicates.head.id)
    if (!queue)
      false
    else
      queue.get.exists(_.state == state)
  }
  
  /** With this method the caller can add a new entry into the list and gets a ListEntry returned, which 
   *  he then can manipulate without matching the state again.
   *  
   * @param state the state to add
   * 
   * @return the newly created listEntry
   */
  def enterState(state:State):ListEntry = {
    val actionMap:ActionMap = Map()
    state.getBoundActions.foreach(actionMap(_) = default) //Populate Entry
    actionMap(defaultVariable) = defaultVariableStartValue
    
    val entry = ListEntry(state,actionMap)
    
    //Might crash when empty state is passed (which shouldn't occurr)
    val id = state.predicates.head.id
    experience.getOrElseUpdate(id, Queue()).enqueue(entry)
    
    entry
  }
  
  
  override def enterState(state:State, actions:List[Action]) {
    val actionMap:ActionMap = Map()
    actions.foreach(actionMap(_) = default)
    actionMap(defaultVariable) = defaultVariableStartValue
    
    val id = state.predicates.head.id
    experience.getOrElseUpdate(id, Queue()).enqueue(ListEntry(state,actionMap))
  }
  
  
  override def bestActionEntry(state:State, allowedActions:List[Action], isBetter:(Value,Value)=>Boolean):Entry = {
    val queue = experience.get(state.predicates.head.id)
    
    if (!queue)
      return Entry(state, allowedActions.head, default)
      
    val entry = queue.get.find(_.state == state)
    if (!entry)
      return Entry(state, allowedActions.head, default)
    
    logDebug("StateEntry: " + entry.get)
      
    val (action,value) = entry.get.actionMap.reduce((av1,av2) => if (isBetter(av1._2,av2._2)) av1 else av2)
    Entry(state, action, value)
  }
  
  
  def apply(key:(State,Action)):Value = {
    val queue = experience.getOrElse(key._1.predicates.head.id, Queue())
    
    for(entry <- queue) {
      if (entry.state == key._1)
        return entry.actionMap.getOrElse(key._2, default)
    }
    
    return default
  }
  
  def update(key:(State,Action), value:Value) {
    val queue = experience(key._1.predicates.head.id) //Make assumption that state exists on update
    
    val entry = queue.find(_.state == key._1) default enterState(key._1)
    
    entry.actionMap(key._2) = value
  }
  
  
  override def toString = experience.toString
}