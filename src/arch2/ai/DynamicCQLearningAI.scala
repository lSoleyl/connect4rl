package arch2.ai

import arch2.data.experience._
import arch2.base._
import arch2.data._
import arch2.algorithm.QLearning
import arch2.base.predicates._
import arch2.config.RewardConfig

/** This class implements the AI for DCQLearingAI
 */
case class DynamicCQLearningAI(val experience:CARCASSExperience with HeaderV1, l:AILoader) extends AbstractAgentAI(new QLearning(experience),l)(TypeMapping.simplePredicateState) with ExtendedPredicateState  with PredicateStateUnifier[Int,Double] {
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone) = selectAction(state, 0, actions)
  
  override def newGame(config:Seq[Any]) {
    config.foreach(_ match {
      case r:RewardConfig => {
        //Set threshold for abstraction
        experience.setTDEThreshold((r.won - r.lost) * 10)
      }
      case _ =>
    })
    
    super.newGame(config)
  }
  
  val defaultVariable = Variable('?')
  val variableFactory = Variable.get _
  
  override val notifyExperience = experience.notifyExperience _
}

object DynamicCQLearningAI {
  val Loader = GenericAILoader("DCQLearningAI", new CARCASSExperience with HeaderV1, DynamicCQLearningAI.apply, GenericAILoader.CANNOT_PLAY_AGAINST_ITSELF)
}

/** Simple Dynamic CARCASS QLearningAI
 *  
 *  Only difference to DCQLearningAI is that this agent doesn't know about the initial two states:
 *   - 4k(X) -> {X,?}
 *   - 4b(X) -> {X,?}
 */
object SDynamicCQLearningAI {                                         // v- no expert knowledge
  val Loader = GenericAILoader("SDCQLearningAI", new CARCASSExperience(false) with HeaderV1, DynamicCQLearningAI.apply, GenericAILoader.CANNOT_PLAY_AGAINST_ITSELF)
}