package field

import org.scalatest._
import arch2.field.Stone._
import arch2.field.Line

class FieldTest extends FlatSpec with Matchers {
  def f = arch2.game.Field
  
  val field = f
  
  "Newly created field" should "allow all columns" in {
    field.getFreeColumns.length should be (field.columns)
  } 
  
  it should "return 0 as landing position for every slot" in {
    field.getFreeColumns.foreach(field.getLandingPos(_) should be (0))
  }  
  
  /** Build following situation:
   * 
   *  R R R
   */
  val field2 = f
  field2.putIntoSlot(0, RED)
  field2.putIntoSlot(1, RED)
  field2.putIntoSlot(2, RED)
  
  "Single block situation" should "return one line for EMPTY at 3" in {
    field2.getLines(3, EMPTY).length should be (1)
  } 
  
  it should "return a line of length 3 for color red" in {
    field2.getLines(3, EMPTY) should be (List(Line(RED, 3)))
  }
  
  it should "return four lines for RED at 3" in {
    field2.getLines(3, RED).length should be (4)
  }
  
  it should "return one line of length 4 for RED at 3" in {
    field2.getLines(3, RED).filter(_.length > 1) should be (List(Line(RED,4)))
  }
  
  it should "return three lines of length 1 for RED at 3" in {
    field2.getLines(3, RED).filter(l => l.length == 1 && l.color == RED).length should be (3)
  }
  
  it should "return four lines for YELLOW at 3" in {
    field2.getLines(3, YELLOW).length should be (4)
  }
   
  it should "return only lines of length 1 for YELLOW at 3" in {
    field2.getLines(3, YELLOW).foreach(_ should be (Line(YELLOW,1)))
  }
  
  /** Build up following situation:
   *  
   *  R R Y
   *  R Y R
   *  
   *  ->state should be 3erblock(0) for yellow (NOT 3erblock(0) 3erblock(0)!!!)
   */  
  val field3 = f
  field3.putIntoSlot(0, RED)
  field3.putIntoSlot(0, RED)
  field3.putIntoSlot(1, YELLOW)
  field3.putIntoSlot(1, RED)
  field3.putIntoSlot(2, RED)
  field3.putIntoSlot(2, YELLOW)
  field3.putIntoSlot(2, RED)
  
  "Double block situation" should "return two lines for EMPTY at 0" in {
    field3.getLines(0, EMPTY).length should be (2)
  }
  
  it should "return two lines for red with length 2 at 0" in {
    field3.getLines(0, EMPTY).foreach(_ should be (Line(RED,2)))
  }
  
  it should "return four lines for red at 0" in {
    field3.getLines(0, RED).length should be (4)
  }
  
  it should "return two lines of length 4 for red at 0" in {
    field3.getLines(0, RED).filter(_ == Line(RED,3)).length should be (2)
  }
  
  it should "return two lines of length 1 for red at 0" in {
    field3.getLines(0, RED).filter(_ == Line(RED,1)).length should be (2)
  }
  
  it should "return four lines for yellow at 0" in {
    field3.getLines(0, YELLOW).length should be (4)
  }
  
  it should "return four lines of length 1 for yellow at 0" in {
    field3.getLines(0, YELLOW).foreach(_ should be (Line(YELLOW,1)))
  }
  
  
}