package predicates

import org.scalatest._

import arch2.base.predicates._

class StateDeriving  extends FlatSpec with Matchers {
  implicit val mapper = arch2.base.predicates.Variable.get _
  
  case class D[T]()(x:(UnboundPredicateState[T], List[BoundVariable[T]])) {
    val state = x._1
    val variables = x._2
    val predicates = state.predicates
    
    def apply(x:T):Variable[T] = variables.find(_.value == x).get.variable
  }
  
  
  import data.Predicates._
  
  
  object v {
    val A = Variable('A')
    val B = Variable('B')
    val C = Variable('C')
    val D = Variable('D')
  }
  
  def d(s:BoundPredicateState[Int]) = D()(UnboundPredicateState.deriveState(s))
  
  
  val EMPTY_STATE = Nil
  val K_1 = K(1) :: Nil
  val KL_11 = K(1) :: L(1) :: Nil
  val KL_12 = K(1) :: L(2) :: Nil
  val KLM_323 = K(3) :: L(2) :: M(3) :: Nil
  
  
  
  "Empty state" should "be derived into an empty state" in {
    d(EMPTY_STATE).predicates.length should be (0)
  }
  
  it should "bind no variables" in {
    d(EMPTY_STATE).variables.length should be (0)
  }
  
  "K(1)" should "be derived into one predicate" in {
    d(K_1).predicates.length should be (1)
  }
  
  it should "bind one variable" in {
    d(K_1).variables.length should be (1)
  }
  
  it should "bind A = 1" in {
    d(K_1)(1) should be (v.A)
  }
  
  it should "be K(A)" in {
    d(K_1).state.toString should be ("K(A)")
  }
  
  
  "K(1) L(1)" should "be derivde into two predicates" in {
    d(KL_11).predicates.length should be (2)
  }
  
  it should "bind one variable" in {
    d(KL_11).variables.length should be (1)
  }
  
  it should "bind A = 1" in {
    d(KL_11)(1) should be (v.A)
  } 
  
  it should "be K(A) L(A)" in {
    d(KL_11).state.toString should be ("K(A) L(A)")
  }
  
  
  "K(1) L(2)" should "be derived into two predicates" in {
    d(KL_12).predicates.length should be (2)
  } 
  
  it should "bind two variables" in {
    d(KL_12).variables.length should be (2)
  }
  
  it should "bind A = 1, B = 2" in {
    val b = d(KL_12)
    b(1) should be (v.A)
    b(2) should be (v.B)
  }
 
  it should "be K(A) L(B)" in {
    d(KL_12).state.toString should be ("K(A) L(B)")
  }
  
  
  "K(3) L(2) M(3)" should "be derived into three predicates" in {
    d(KLM_323).predicates.length should be (3)
  }
  
  it should "bind two variables" in {
    d(KLM_323).variables.length should be (2)
  }
  
  it should "bind A = 3, B = 2" in {
    val b = d(KLM_323)
    
    b(3) should be (v.A)
    b(2) should be (v.B)
  }  
  
  it should "be K(A) L(B) M(A)" in {
    d(KLM_323).state.toString should be ("K(A) L(B) M(A)")
  }
}