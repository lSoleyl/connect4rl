package arch2.data.experience

import arch2.data.ProgressInfo
import base.MapExperience
import utils.io.ByteWriter
import utils.io.ByteReader

/** MonteCarlo experience for string states. This class additionally keeps track of how often a state has been visited
 *  to average the value of a state. 
 */
class MCStringExperience extends StreamSerializable with MapExperience[String,Int,(Double,Int)] {
  override val default = (0d,0)
  
  override def writeToStream(out:OutputStream, progress:ProgressInfo) {
    val writer = new ByteWriter(out)
    
    writer.writeInt(experience.size) //Write Size
    progress.setTotal(experience.size)
    
    for((sa, (value,encounters)) <- experience) {
      writer.writeString(sa.s)
      writer.writeInt(sa.a)
      writer.writeDouble(value)
      writer.writeInt(encounters)  
      progress.next
    }
  }
  
  
  override def loadFromStream(in:InputStream, progress:ProgressInfo) {
    val reader = new ByteReader(in)
    
    experience.clear
    val entries = reader.readInt
    progress.setTotal(entries)
    var c = 0
    
    while(c < entries) {
      val state = reader.readString
      val action = reader.readInt
      val value = reader.readDouble
      val encounters = reader.readInt
      
      experience(SA(state,action)) = (value,encounters)      
      c += 1
      progress.next
    }
    
  }
}