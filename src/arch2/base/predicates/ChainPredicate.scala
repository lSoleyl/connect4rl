package arch2.base.predicates

import arch2.base.Order

sealed class ChainPredicate[T](val chainLength:Int, val column:T, name:String)(implicit val order:Order[T]) extends Predicate[T] with Ordered[ChainPredicate[T]] {
  def argument = column
  val id = name
  
  /** Predicates are ordered by 
   *   * chain length
   *   * type (chaining or block)
   *   * column
   */
  def compare(other:ChainPredicate[T]):Int = {
    if (this.id != other.id)
      return other.id.compareTo(id)
    else
      order(this.column, other.column) //this one is reversed
  }
  
  
  override def equals(other:Any) = {
    if (!other.isInstanceOf[ChainPredicate[T]]) {
      false
    } else {
      val o = other.asInstanceOf[ChainPredicate[T]]
      if (o eq this)
        true
      else
        this.chainLength == o.chainLength && this.id == o.id && this.column == o.column
    }    
  }
  
  override def hashCode:Int = chainLength + id.hashCode + column.hashCode
  
  def replaceArgument[A](arg:A)(implicit order:Order[A]) = new ChainPredicate(chainLength, arg, id)
  def mapArgument[A](f:T=>A)(implicit order:Order[A]) = new ChainPredicate(chainLength, f(argument), id)
}

//For more performance "k" is used instead of "erkette" and "b" instead of "erblock" for faster id compares
class ChainingPredicate(chainLength:Int , column:Int) extends ChainPredicate(chainLength, column, chainLength + "k")
class BlockPredicate(chainLength:Int, column:Int) extends ChainPredicate(chainLength, column, chainLength + "b")

object ChainingPredicate {
  def apply(chainLength:Int, column:Int) = new ChainingPredicate(Math.min(chainLength, 4), column) //Math.min, to treat all chains longer than 4 the same way, since that difference doesn't matter
  def apply[T](chainLength:Int, column:Variable[T]) = new ChainPredicate(Math.min(chainLength, 4), column, chainLength + "k")
}

object BlockPredicate {
  def apply(chainLength:Int, column:Int) = new BlockPredicate(Math.min(chainLength, 4), column)
  def apply[T](chainLength:Int, column:Variable[T]) = new ChainPredicate(Math.min(chainLength, 4), column, chainLength + "b")
}

