package arch2.base.predicates

sealed trait Constraints[T] {
  def ::(c:Constraint[T]):Constraints[T] = new ConstraintList(c, this)
  def evaluate(x:BoundVariable[T]):Boolean = true
  def release {}
  def equals(c:Constraints[T]):Boolean
  
  override def equals(o:Any):Boolean = {
    if (o.isInstanceOf[Constraints[T]]) 
      equals(o.asInstanceOf[Constraints[T]])
    else 
      false
  }
  
  def toList:List[Constraint[T]]
}

class ConstraintList[T](val c:Constraint[T], val tail:Constraints[T]) extends Constraints[T] {
  override def evaluate(x:BoundVariable[T]):Boolean = c.evaluate(x, tail)
  override def release {
    c.release
    tail.release
  }
  
  override def equals(other:Constraints[T]):Boolean = {
    if (!other.isInstanceOf[ConstraintList[T]])
      false 
    else {
      val cl = other.asInstanceOf[ConstraintList[T]]
      cl.c == this.c && cl.tail == this.tail
    }
  }
  
  override def toList = c :: tail.toList
}

case class Constraint[T](val offset:T, val variable:Variable[T])(implicit ring:arch2.math.MathTypes.Ring[T], ordering:Ordering[T]) {
  /** This method binds the chained constraints in an 
   *  all-or-nothing manner. Either all constraints are satisfied (result=true)
   *  or none of them and no variables get bound(result=false)
   *  
   *  @param x the bound variable to which this constraint is relatively evaluated
   *  
   *  @return true if all constraints were satisfied, false otherwise
   */
  def evaluate(x:BoundVariable[T], tail:Constraints[T]):Boolean = {
    val bindValue = ring.add(x.value, offset)
    if (variable.isBindableTo(bindValue)) {
      tail.evaluate(x) && variable.bind(bindValue)
    } else {
      false
    }
  }
  
  /** This method will unbind all bound variables.
   *  To ensure consistency, this should only get called if
   *  evaluate() was successful
   */
  def release {
    variable.release()
  }
  
  
  override def equals(other:Any):Boolean = {
    if (!other.isInstanceOf[Constraint[T]])
      false
    else {
      val c = other.asInstanceOf[Constraint[T]]
      c.offset == this.offset && c.variable == this.variable
    }
  }
  
  def offsetString:String = if (ordering.lt(offset, ring.zero)) offset.toString else ("+" + offset)
}

class NilConstraint[T] extends Constraints[T] {
  override def equals(c:Constraints[T]):Boolean = c.isInstanceOf[NilConstraint[T]]
  override def toList = Nil
}

object Constraints {
  def apply[T]():Constraints[T] = new NilConstraint
}