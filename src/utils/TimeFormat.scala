package utils

object TimeFormat {
  def hhmmss(seconds:Long):String = {
    val h = seconds / 3600
    val m = (seconds % 3600) / 60
    val s = seconds % 60
    
    f"$h%02d:$m%02d:$s%02d"
  }
}