package utils

import spray.json._
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.io.File
import java.io.OutputStreamWriter
import java.io.FileOutputStream

/** Small JSON utility to unify file access
 */
object JSON {
  /** Load JSON from file and return None if file does not exist.
   *  Some(json) if everything went ok, or throw an exception if
   *  a parser problem occurred
   */
  def load(filename:String):Option[JsValue] = {
    if (new File(filename).exists) {
      val data = new String(Files.readAllBytes(Paths.get(filename)),StandardCharsets.UTF_8) 
      Some(data.asJson)
    } else {
      None
    }
  }
  
  def save(json:JsValue, filename:String) {
    val data = json.prettyPrint
    TryWithResources(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"))(_.write(data)).get //Throw exception if any
  } 
}