package predicates

import org.scalatest._
import predicates.mock.MockPredicatePlayer
import arch2.field._
import arch2.field.Stone._

class PredicateStateTest extends FlatSpec with Matchers {
  def f = arch2.game.Field  
  val player = new MockPredicatePlayer
  
  def turn(field:arch2.game.Field, color:Stone = YELLOW) { player.turn(field, field.getFreeColumns, color) }

  val field1 = f
  
  "An empty field" should "allow all actions" in {
    turn(field1)
    player.lastState.actions.length should be (field1.columns)
  }
  
  it should "have an empty predicate list" in {
    turn(field1)
    player.lastState.state should be (Nil)
  }
  
  /** Build up following situation
   * 
   *  R R R
   * 
   *  -> State should be 4erblock(3) for yellow
   */
  val field2 = f
  field2.putIntoSlot(0, RED)
  field2.putIntoSlot(1, RED)
  field2.putIntoSlot(2, RED)
  
  
  "Field with one block possibility" should "allow all actions" in {
    turn(field2)
    player.lastState.actions.length should be (field2.columns)
  }
  
  it should "contain exactly one predicate" in {
    turn(field2)
    player.lastState.state.length should be (1)
  }
  
  it should "contain 4b(3)" in {
    turn(field2)
    player.lastState.state.mkString(" ") should be ("4b(3)")
  }
  
  
  /** Build up following situation:
   *
   *      R  
   *  R R Y
   *  R Y R
   *  
   *  ->state should be 3erblock(0) for yellow (NOT 3erblock(0) 3erblock(0)!!!)
   */  
  val field3 = f
  field3.putIntoSlot(0, RED)
  field3.putIntoSlot(0, RED)
  field3.putIntoSlot(1, YELLOW)
  field3.putIntoSlot(1, RED)
  field3.putIntoSlot(2, RED)
  field3.putIntoSlot(2, YELLOW)
  field3.putIntoSlot(2, RED)
  
  "Field with two block options in the same column" should "allow all actions" in {
    turn(field3)
    player.lastState.actions.length should be (field3.columns)
  }
  
  it should "contain exactly one predicate" in {
    turn(field3)
    player.lastState.state.length should be (1)
  }
  
  it should "be bound as 3b(0)" in {
    turn(field3)
    player.lastState.state.mkString(" ") should be("3b(0)")
  }
  
  it should "be bound as 3k(0) for RED" in {
    turn(field3, RED)
    player.lastState.state.mkString(" ") should be ("3k(0)")
  }
  
   /** Build up following situation:
   *
   *  R   R  
   *  R R Y
   *  R Y R
   *  
   *  ->state should be 4erblock(0) 3erblock(1)
   */  
  val field4 = f
  field4.putIntoSlot(0, RED)
  field4.putIntoSlot(0, RED)
  field4.putIntoSlot(1, YELLOW)
  field4.putIntoSlot(1, RED)
  field4.putIntoSlot(2, RED)
  field4.putIntoSlot(2, YELLOW)
  field4.putIntoSlot(2, RED)
  field4.putIntoSlot(0, RED)
  
  "State with two different block positions" should "contain two predicates" in {
    turn(field4)
    player.lastState.state.length should be (2)
  }
  
  it should "be bound as 4b(0) 3b(1)" in {
    turn(field4)
    player.lastState.state.mkString(" ") should be ("4b(0) 3b(1)")
  }
  
  it should "be bound as 4k(0) 3k(1) for RED" in {
    turn(field4, RED)
    player.lastState.state.mkString(" ") should be ("4k(0) 3k(1)")
  }
  
  val field5 = f
  field5.putIntoSlot(3, RED)
  field5.putIntoSlot(4, YELLOW)
  
  "Small state" should "contain six predicates" in {
    turn(field5)
    player.lastState.state.length should be (6)
  }
  
  it should "only contain predicates of length 2" in {
    turn(field5)
    player.lastState.state.foreach(_.chainLength should be (2))
  }
  
  it should "be bound as 2k(3) 2k(4) 2k(5) 2b(2) 2b(3) 2b(4)" in { //Test correct ordering here
    turn(field5)
    player.lastState.state.mkString(" ") should be ("2k(3) 2k(4) 2k(5) 2b(2) 2b(3) 2b(4)")
  }
  
  it should "be bound as 2k(2) 2k(3) 2k(4) 2b(3) 2b(4) 2b(5) for RED" in {
    turn(field5, RED) 
    player.lastState.state.mkString(" ") should be ("2k(2) 2k(3) 2k(4) 2b(3) 2b(4) 2b(5)")
  }
}