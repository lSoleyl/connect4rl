package utils.io

import java.io._
import scala.Array.canBuildFrom

class ByteReader(in:InputStream) {
  val stream = in
  private val bytes = new Array[Byte](8)
  
  import ByteReader._
  
  def readInt:Int = {
    if (in.read(bytes, 0, 4) != 4)
      throw new IOException("Inputstream end reached")
   
    bytesToInt(bytes)
  }
  
  def readLong:Long = {
    if (in.read(bytes, 0, 8) != 8)
      throw new IOException("Inputstream end reached")
   
    bytesToLong(bytes) 
  }
  
  def readString:String = {
    val length = readInt
    val buffer = new Array[Byte](length)
    if (in.read(buffer) != length)
      throw new IOException("Inputstream end reached")
    
    String.copyValueOf(buffer.map(_.asInstanceOf[Char]))
  }
  
  def readBool:Boolean = {
    return readByte != 0
  }
  
  def readByte:Byte = {
    if (in.read(bytes, 0, 1) != 1)
      throw new IOException("Inputstream end reached")
    bytes(0)
  }
  
  def readDouble:Double = {
    java.lang.Double.longBitsToDouble(readLong)
  }
}

/** This companion object provides functions to convert byte arrays into primitve values
 */
object ByteReader {
  def bytesToInt(bytes:Array[Byte]):Int = {
    var result = bytes(3) & 0xFF
    result += (bytes(2) & 0xFF) << 8 
    result += (bytes(1) & 0xFF) << 16
    result += (bytes(0) & 0xFF) << 24
    result
  } 
  
  def bytesToLong(bytes:Array[Byte]):Long = {
    var result:Long = bytes(7) & 0xFF
    result += (bytes(6) & 0xFF).asInstanceOf[Long] << 8 
    result += (bytes(5) & 0xFF).asInstanceOf[Long] << 16
    result += (bytes(4) & 0xFF).asInstanceOf[Long] << 24
    result += (bytes(3) & 0xFF).asInstanceOf[Long] << 32
    result += (bytes(2) & 0xFF).asInstanceOf[Long] << 40
    result += (bytes(1) & 0xFF).asInstanceOf[Long] << 48
    result += (bytes(0) & 0xFF).asInstanceOf[Long] << 56
    result
  }
}