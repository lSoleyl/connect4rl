package arch2.field

/** An abstract class which represents a line of stones of the same color
 */
case class Line(color:Stone, length:Int)