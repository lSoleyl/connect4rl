package arch2.gui

import scala.swing._
import arch2.base._
import utils.Options._

import scala.concurrent.Future
import scala.concurrent.Promise
import scala.util.Success
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration.Duration


/** This class represents the row of buttons on the field's top.
 *  It will draw itself with all buttons if created. This class then provides the 
 *  ActionSelectionInterface for human interaction with the game. 
 */
class ButtonPanel(fieldWidth:Int, parent:GameBoard) extends GridPanel(1, fieldWidth) with ActionSelectionInterface {
  val buttons = (0 until fieldWidth).map(new PanelButton("v", _, this)).toArray
  
  contents ++= buttons
  
  def selectActionFuture(possibleActions:List[Int]):Future[Int] = {
    val promise = new SynchronizedPromise[Int]
    possibleActions.foreach(buttons(_).activate(promise))
    promise.future
  }
  
  /** Implements the ActionSelectionInterface
   */
  def selectAction(actions:List[Int]):Int = Await.result(selectActionFuture(actions), Duration.Inf)
  
  
  /** This will be called if a player needs the button panel as interface
   *  for action selection
   */
  def require = parent.requireButtons
  
  /** Will be called by the gameboard if it has been closed while the game was in progress.
   *  This should unlock even human players and make it possible to interrupt the game cleanly
   *  This method will try to complete an open button promise (if any) with a default value
   */
  def cancelSelection {
    val button = buttons.find(b => b.enabled && b.promise != null)
    if (button) button.get.promise.complete(button.get.i)
  }
  
  def disableAllButtons { 
    Swing.onEDT {
      buttons.foreach(_.enabled = false)
    }
  }
}


class PanelButton(s:String, val i:Int, panel:ButtonPanel) extends Button(s) {
  var promise:SynchronizedPromise[Int] = null
  
  action = new Action(s) {
    def apply = {
      if (promise != null) {
        Future {
          promise.complete(i)
          panel.disableAllButtons
        }
      }
    }
  }

  
  def activate(promise:SynchronizedPromise[Int]) = {
    this.promise = promise
    Swing.onEDT { enabled = true }
  }
  
  
  enabled = false  
}

class SynchronizedPromise[T] {
  val p = Promise[T]
  def future:Future[T] = p.future
  
  def complete(result:T) {
    this.synchronized {
      if (!p.isCompleted)
        p.complete(Success(result))
    }
  }
}