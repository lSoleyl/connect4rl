package arch2.ai

import arch2.data._
import arch2.base.predicates._
import arch2.base.{ExtendedPredicateState, PredicateState}
import arch2.algorithm.QLearning
import arch2.base.TypeMapping
import arch2.data.experience._

case class PredicateQLearningAI(experience:SimplePredicateExperience with HeaderV1, l:AILoader) extends AbstractAgentAI(new QLearning(experience), l)(TypeMapping.simplePredicateState) with ExtendedPredicateState  with PredicateStateMatcher[Int,Double] {
  override def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone) = {
    logTrace("State: " + state)
    logTrace("Experience: " + experience)
    selectAction(state, 0, actions)
  }
  
  val defaultVariable = Variable('?')
  val variableFactory = Variable.get _
}


object PredicateQLearningAI {
  val Loader = GenericAILoader("PQLearningAI", new SimplePredicateExperience with HeaderV1, PredicateQLearningAI.apply, GenericAILoader.CANNOT_PLAY_AGAINST_ITSELF)
}