package arch2.math
import MathTypes._

case class IntVector2D(x:Int, y:Int) {
  type Type=IntVector2D
  
  def +(other:Type) = new Type(x + other.x, y + other.y)
  def *(f:Int) = new Type(x * f, y * f)
}

case class Vector2D[T](x:T, y:T)(implicit math:Ring[T]) {
  type Type=Vector2D[T]
  
  def +(other:Type) = new Type(math.add(x, other.x), math.add(y, other.y))
}