package utils

object TimeMeasureMS {
  def apply(ac: =>Unit):Long = {
    val t1 = System.currentTimeMillis
    ac
    System.currentTimeMillis - t1
  }
}

class TimeMeasureMS {
  var elapsed:Long = 0
  
  def apply[T](action: =>T):T = {
    val t1 = System.currentTimeMillis
    val result = action
    elapsed += (System.currentTimeMillis - t1)
    result    
  }
  
  def result = elapsed
}