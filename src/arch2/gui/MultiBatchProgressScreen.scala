package arch2.gui

import scala.swing._
import utils.Log
import arch2.data.ProgressInfo
import utils.TimeFormat

/** This class has a main progressbar which contains the total count of all episodes and a 
 *  subProgressBar for each single batch run.
 *  
 *  
 */
class MultiBatchProgressScreen(totalEpisodes:Int, titleMessage:String = "Multi batch run in progress") extends Frame with Log with AbortableFrame {
  title = titleMessage
  private var opened = false
  
  private val mainProgressBar = new ProgressBar
  private val subProgressBar = new ProgressBar
  
  private val mainProgress = new ProgressUpdater(mainProgressBar, this)
  private val subProgress = new ProgressUpdater(subProgressBar, this)
  mainProgress.setTotal(totalEpisodes)
  
  private val speed = new Label("      0 episodes/sec") { preferredSize = new Dimension(200,20) }
  private val timeLeft = new Label("Estimated time remaining: unknown.")
  private var measuring = true
  
  contents = new BoxPanel(Orientation.Vertical) {
    contents += new FlowPanel {
      contents += new Label("Running current multi batch")
      contents += speed
      contents += timeLeft
    }
    contents += mainProgressBar
    contents += subProgressBar
  }
  
  override def open {
    if (!opened) {
      opened = true
      super.open
    } 
  }
  
  
  /** Prepare next batch run by adding the total of the previous batch run to the main progress bar
   *  and resetting the sub progress bar to a new episode value. This is the only way to manipulate the 
   *  main progress bar.
   *  
   *  @param episodes the number of episodes for the next batch run
   *  
   *  @return a reference to the progress info
   */
  def nextBatch(episodes:Int):ProgressInfo = {
    if (subProgress.totalCount - subProgress.currentCount != episodes) { //Do nothing if we have a resumed batch run (special case) 
      mainProgress.nextN(subProgress.totalCount) //Move main progress bar
      subProgress.setTotal(episodes)
    }
    
    subProgress
  }
  
  def start {
    Swing.onEDT { this.open; this.centerOnScreen }
    
    measuring = true
    new Thread(this.getClass().getSimpleName() + "-Updater") {
      override def run {
        var avgDelta = 1
        var lastCount = 0
        while(measuring && !isAborted) {
          Thread.sleep(1000) //update every second
          val newCount = mainProgress.currentCount + subProgress.currentCount
          val delta = newCount - lastCount
          avgDelta = (0.9*avgDelta + 0.1*delta).toInt
          log.info(s"Current speed: $delta episodes/sec")
          
          
          val estimatedTime = if (avgDelta > 0) TimeFormat.hhmmss((mainProgress.totalCount - newCount) / avgDelta) else "infinite"
          
          Swing.onEDT { 
            speed.text = f" $avgDelta%6d episodes/sec"
            timeLeft.text = "Estimated time remaining: " + estimatedTime
          }
        
          lastCount = newCount
        }
      }
    }.start()
  }
  
  
  def stop {
    measuring = false
    log.info("Stopped measuring game speed")
  }
  
  
  
  

}