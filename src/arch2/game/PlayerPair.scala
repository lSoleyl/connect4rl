package arch2.game

import arch2.base._
import arch2.field.Stone._

/** This class represents a pair of players.
 *  This class should make switching beginning players much easier
 */
class PlayerPair(p1:Player, p2:Player) {
  var switched = false
  val pList = List((ColoredPlayer(YELLOW, p1), ColoredPlayer(RED, p2)), (ColoredPlayer(YELLOW, p2), ColoredPlayer(RED, p1)))
  
  /** Call this method to switch first and second player and return them as pair
   *  
   *  @return a pair of players
   */
  def switch:(ColoredPlayer,ColoredPlayer) = {
    switched = !switched
    if (switched) pList.head else pList.tail.head    
  }
  
  /** Just return the current order of players
   * 
   * @return a pair of players
   */
  def get:(ColoredPlayer, ColoredPlayer) = if (switched) pList.head else pList.tail.head
  
  override def toString = if (switched) s"PlayerPair($p1, $p2)" else s"PlayerPair($p2, $p1)"
}