package arch2.data

import arch2.base.Player
import arch2.game.PlayerStatistics
import utils.Log
import utils.Options._

/** This class represents the result data from a multi batch run, which should be somehow exported (eg. Matlab)
 *  
 */
class ExportData private() extends Log {
  import ExportData._
  
  //A list of episodes for which values got recorded. Always in reverse order
  private var episodes:List[Int] = Nil
  private var playerData:List[PlayerData] = Nil
    
  /** Private initialization method, which gets called form ExportData-object
   *  to initialize the player data list. This is called for a batch run
   *  
   *  @param players the players
   */
  private def init(p1:Player, p2:Player) =  {    
    playerData = p1.uniqueNames(p2).map(PlayerData(_))
    this
  }
  
  private def data(p:Player):Option[PlayerData] = playerData.find(d => d.name == p.toString || d.name == p.loader.displayName)
  
  /** Add an entry for a single batch run
   *  
   *  @param batchEpisodes of the current batch run
   *  @param stats the player statistics for every player
   */
  def addEntry(batchEpisodes: Int, stats:PlayerStatistics*) {
     val totalEpisodes = (episodes.headOption default 0) + batchEpisodes     
     
     episodes = totalEpisodes :: episodes
     
     for {
       stat <- stats
       pData <- data(stat.player) //for loop will simply skip players with no PlayerData in the list
     } {
       pData.columns.winRate += stat.winRate
       pData.columns.states += stat.states
     }     
  }
  
  def getDataPair:(PlayerData,PlayerData) = (playerData.head, playerData.tail.head)
  def getEpisodes = episodes.reverse
  
  case class PlayerData(name:String, columns:ColumnHeader = ColumnHeader()) {
    def fullColumnName[A](column:Column[A]) = name + "." + column.name //Agent.Column instead of ColumnAgent
    
    def getCompleteColumn[A](column:Column[A])(formatter: A=>String):List[String] = fullColumnName(column) :: column.getEntries.map(formatter)
  }
  
  case class ColumnHeader(winRate:Column[Double] = Column(HeaderConstants.WIN_RATE), 
                          states:Column[Option[Int]] = Column(HeaderConstants.STATE_COUNT))
  
  /** This class represents the entries of a single column, which like
   *  the episodes hold in a list in reverse order
   */
  case class Column[A](name:String, private var entries:List[A] = Nil) {
    def +=(x:A) { entries = x :: entries }
    def length = entries.length
    def getEntries = entries.reverse
  }
}

object ExportData {
  object HeaderConstants {
    val EPISODES = "Episoden"
    val WIN_RATE = "Gewinnrate"
    val STATE_COUNT = "Zustände"
  } 
  
  /** Constuctor function to collect data along a batch run
   *  
   *  @param p the players
   */
  def apply(p1:Player, p2:Player) = new ExportData().init(p1,p2)
  
}