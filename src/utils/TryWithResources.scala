package utils

import scala.util.Try

object TryWithResources {
  def apply[T, A <: AutoCloseable](resource: =>A)(code: A => T):Try[T] = {
    Try {
      val ress:A = resource //Evaluate by-name argument, which might throw
      val result = Try { code(ress) }
      ress.close()
      result.get //Transfer the Try's content into the outer Try
    }
  }
}