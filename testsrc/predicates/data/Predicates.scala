package predicates.data

import arch2.base.Order
import arch2.base.predicates.ChainPredicate

object Predicates {
  //Provide three different predicates for testing
  def A[T:Order](x:T) = new ChainPredicate[T](0, x, "A")
  def B[T:Order](x:T) = new ChainPredicate[T](0, x, "B")
  def C[T:Order](x:T) = new ChainPredicate[T](0, x, "C")
  
  def H[T:Order](x:T) = new ChainPredicate[T](0, x, "H")
  def I[T:Order](x:T) = new ChainPredicate[T](0, x, "I")
  def J[T:Order](x:T) = new ChainPredicate[T](0, x, "J")
  
  def K[T:Order](x:T) = new ChainPredicate[T](0, x, "K")
  def L[T:Order](x:T) = new ChainPredicate[T](0, x, "L")
  def M[T:Order](x:T) = new ChainPredicate[T](0, x, "M")
}