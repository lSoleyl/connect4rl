package arch2.data.experience

import arch2.base.predicates._
import arch2.data.ProgressInfo
import base.PredicateCachedExperience
import utils.Log
import utils.Times._
import utils.io._

/** This class represents the experience for the SP-QLearningAI. Every state consists of a list of predicates with 
 *  unbound variables. The number of actions equals the number of variables + 1(random_action)
 *  The random action is always a variable called '?'
 */
class SimplePredicateExperience extends StreamSerializable with PredicateCachedExperience[Int,Double] with Log {
  val default = 0d
  val defaultVariable = Variable('?')
  val defaultVariableStartValue = -1d
  
  def writeToStream(out:OutputStream, progress:ProgressInfo) {
    implicit val writer = new ByteWriter(out)
    import writer._
    import arch2.data.Serializer._
    
    val length = experience.length
    log.info(s"Writing $length states")
    writeInt(length)
    
    progress.setTotal(length)
    for(entry <- experience) {
      serialize(entry.state)      
      serialize(entry.actionMap.toList)
      
      progress.next
    }  
    
    writeInt(failedCounter)
  }
  
  def loadFromStream(in:InputStream, progress:ProgressInfo)  {
    import scala.collection.mutable.Map
    implicit val reader = new ByteReader(in)
    import reader._
    import arch2.data.Serializer._
    
    val entries = readInt
    log.info(s"Loading $entries states")
    
    
    progress.setTotal(entries)
    experience = scala.collection.mutable.Queue(entries.collect({
      progress.next
      val state = load[UnboundPredicateState[Int]]
      val actionMap = Map(load[List[(Variable[Int],Double)]]: _*)
      ListEntry(state, actionMap)
    }): _*)
    
    failedCounter = readInt
  }  
}