package utils

class StringTable(cols:Int, rows:Int, spacing:Boolean=false) {
  val ary = Array.fill(rows, cols)("")
  var row = 0
  
  //Add Row
  def append(line:Array[String]) {
    if (line.length > cols)
      throw new IllegalArgumentException("Not enough columns for entry")
    else if (row >= rows)
      throw new IllegalStateException("All rows are filled!")
    
    for (i <- 0 until line.length) {
      ary(row)(i) = line(i)
    }
    
    row += 1
  }
  
  def append(s:Any*) {
    append(s.map(_.toString).toArray)
  }
  
  private def columnWidth(col:Int) = (0 until rows).map(ary(_)(col).length).max + (if(spacing) 1 else 0)
  
  override def toString = "StringTable(" + cols + "," + rows + ")"
  
  def print {
     val result = new StringBuilder
     for(i <- 0 until rows) {
       for (j <- 0 until cols) {
         result append (ary(i)(j).padTo(columnWidth(j), ' ')) 
       }
       result append "\n"
     }
     System.out.print(result.toString)
  }
}