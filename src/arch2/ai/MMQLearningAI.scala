package arch2.ai

import arch2.data.experience._
import arch2.data._
import arch2.base.StringState
import arch2.algorithm.MinMaxQ

case class MMQLearningAI(experience:StringExperience with HeaderV1, l:AILoader) extends AgentAI(new MinMaxQ(experience), l) with StringState 


object MMQLearningAI {
  val Loader = GenericAILoader("MMQLearningAI", new StringExperience with HeaderV1, MMQLearningAI.apply)
}