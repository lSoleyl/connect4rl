package utils.generic

case class Context[A,B](value:A, context:B) {
  def _1 = value
  def _2 = context
}