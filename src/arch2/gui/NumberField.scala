package arch2.gui

import scala.swing._
import scala.swing.event.KeyTyped
import utils.Factors._
import utils.Factors

/** An input field class for numeric input
 */
class NumberField(maxDigits:Int) extends TextField(maxDigits*3/4) {
  listenTo(keys)
  reactions += {
    case e:KeyTyped => if (!e.char.isDigit || text.length >= maxDigits) e.consume
  }
  
  /** Getter and setter for field content (int only)
   */
  def value = Integer.parseInt(text)
  def value_=(i:Int) { text = i.toString }
}

/** An input field which consists of a number field and a factor selection
 *
 */
class BigNumberField extends FlowPanel {
  val number = new NumberField(4)
  val factor = new ComboBox(Factors.FACTOR_LIST) 
  
  contents += number
  contents += factor
  
  /** Getter and setter for field content
   */
  def value = number.value * factor.selection.item.numericValue
  def value_=(i:Int) {
    val (int, f) = i.tuple
    number.value = int
    factor.selection.item = f
  }
 
  def value_=(value:(Int,Factor)) {
    number.value = value._1
    factor.selection.item = value._2
  }
  
  def getFactor = factor.selection.item
  def getNumber = number.value
}