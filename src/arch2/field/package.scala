package arch2

import arch2.math._

package object field {
  //A list of traversal vectors for line completion in the game
  type Vector = arch2.math.Vector
  val VECTOR_CLOCK:List[Vector] = List((0,1), (1,1), (1,0), (1,-1), (0,-1), (-1,-1), (-1,0), (-1,1))
  
  //The same list, but vectors, which point in opposite directions are paired
  val PAIRED_VECTOR_CLOCK:List[(Vector,Vector)] = VECTOR_CLOCK.flatMap(v => {
    if (v.x + v.y > 0 || v.x == 1)
      Some((v, v*(-1)))
    else
      None
  })
  
  //An explicit list of columns for fast iteration
  val COLUMN_LIST = List(0,1,2,3,4,5,6)
}