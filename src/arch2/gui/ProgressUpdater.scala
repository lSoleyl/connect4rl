package arch2.gui

import scala.swing._
import scala.swing.Swing
import arch2.data.ProgressInfo

/** Progressbar implementation of the ProgressInfo trait
 */
class ProgressUpdater(widget:ProgressBar, loadingWindow:Frame) extends ProgressInfo {
  
  def display(percent:Int) { 
    Swing.onEDT {
      loadingWindow.open //Display the loading window if not already done
      widget.value = percent 
    }
  }
}