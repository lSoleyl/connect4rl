package utils

trait SyncObject {
  def acquire
  def release
}

object Acquire {
  def apply[T <: SyncObject with Ordered[T],A](locks: T*)(action: =>A):A = {
    //Remove duplicates and sort the synchronization objects
    val sorted = locks.toSet.toList.sorted
    sorted.foreach(_.acquire)     //acquire all necessary locks
    val result = action           //execute action
    sorted.foreach(_.release)     //release all acquired locks
    result
  }
}