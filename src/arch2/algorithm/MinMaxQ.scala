package arch2.algorithm

import arch2.data.experience._
import arch2.data.experience.base._
import arch2.base._

class MinMaxQ[S,A](val experience:Experience[S,A,Double] with HeaderV1) extends Agent[S,A,Double] {  
  var lastAction:Entry = null
  
  protected def isBetter(v1:Value, v2:Value) = v1 > v2
  
  def selectAction(state:S, reward:Double, possibleActions:List[A]):A = {
    val bestAction = bestActionEntry(state, possibleActions)
    
    if (lastAction != null) { //If this isn't the first state, then apply TD-Update
      update(bestAction.v)
    }
    
    lastAction = bestAction    
    lastAction.sa.a //Return the selected action
  }
  
  def finalReward(reward:Double) {
    update(reward)
    
    lastAction = null
    experience.episodes += 1
  }
  
  private def update(nextQ:Double) {
    //Only update q value if reward was worse than anticipated or previous q value was zero and the reward is > 0
    if (nextQ < lastAction.v || (lastAction.v == 0d && nextQ > 0)) {
      experience(lastAction.sa) = nextQ
    } 
  }
  
  /** Will be called once before every GUI game or before a batch run
   *  
   *  @param params the configuration which has been set
   */
  def initialize(params:LearningParameters) { lastAction = null }
}