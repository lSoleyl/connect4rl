package arch2.math

object MathTypes {
  trait Ring[T] {
    def add(a:T,b:T):T
    def multiply(a:T,b:T):T
    def subtract(a:T,b:T):T
    
    val zero:T
    val one:T
  } 
  
  object Ring {
    implicit object IntRing extends Ring[Int] {
      def add(a:Int, b:Int) = a+b
      def multiply(a:Int, b:Int) = a*b
      def subtract(a:Int, b:Int) = a-b
      
      val zero = 0
      val one = 1      
    }
  }
}