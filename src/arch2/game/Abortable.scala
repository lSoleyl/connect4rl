package arch2.game

/** This trait defines a type which can be used to determine whether a proces has
 *  been aborted or not.
 */
trait Abortable {
  def isAborted:Boolean
}

/** This object represents the default abortable which never aborts
 */
object DontAbort extends Abortable {
  override val isAborted = false
}