package utils.generic.functions

trait FoldLeft[M[_]] {
  def apply[A,B](ma:M[A], z:B, op:(B,A) => B):B
}

object FoldLeft {
  implicit object ListFoldLeft extends FoldLeft[List] {
    def apply[A,B](ma:List[A], z:B, op:(B,A) => B) = ma.foldLeft(z)(op)    
  }
}