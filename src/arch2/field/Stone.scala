package arch2.field

sealed trait Stone

object Stone {
  val EMPTY = new Stone  { override def toString = "EMPTY" }
  val RED = new Stone    { override def toString = "RED" }
  val YELLOW = new Stone { override def toString = "YELLOW" }
  
  val INVALID = new Stone { override def toString = "INVALID" }//Outside of the field
  
  def switch(x:Stone) = if (x == RED) YELLOW else if (x == YELLOW) RED else x
}