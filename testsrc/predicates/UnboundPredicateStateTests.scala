package predicates

import org.scalatest._
import arch2.base.predicates.UnboundPredicateState
import data.Variables._
import data.Predicates._

class UnboundPredicateStateTests extends FlatSpec with Matchers {
  
  val EMPTY_STATE = new UnboundPredicateState[Int](Nil)
  val ONLY_A = new UnboundPredicateState(A(X) :: Nil)
  val ABC_WITH_XYZ = new UnboundPredicateState(A(X) :: B(Y) :: C(Z) :: Nil)
  val ABC_WITH_XYX = new UnboundPredicateState(A(X) :: B(Y) :: C(X) :: Nil)
  val AAB_WITH_XYZ = new UnboundPredicateState(A(X) :: A(Y) :: B(Z) :: Nil)

   
  "Empty state" should "contain no predicates" in {
    EMPTY_STATE.predicates should be (Nil)
  }
  
  it should "contain no variables" in {
    EMPTY_STATE.variables should be (Nil)
  }
  
  
  "A(X)" should "contain exactly one predicate" in {
    ONLY_A.predicates.length should be (1)
  }
  
  it should "contain exactly one variable" in {
    ONLY_A.variables.length should be (1)
  }
  
  it should "contain the variable X" in {
    ONLY_A.variables.head should be (X)
  }
  
  it should "have no bound variables" in {
    ONLY_A.variables.foreach(_.bound should be (None))
  }
  
  
  "A(X) B(Y) C(Z)" should "contain exactly three predicates" in {
    ABC_WITH_XYZ.predicates.length should be (3)
  }
  
  it should "contain exactly three variables" in {
    ABC_WITH_XYZ.variables.length should be (3)
  }
  
  it should "contain the variables X, Y and Z" in {
    List(X,Y,Z).foreach(ABC_WITH_XYZ.variables.contains(_) should be (true))
  }
  
  it should "have no bound variables" in {
    ABC_WITH_XYZ.variables.foreach(_.bound should be (None))
  }
  
  
  "A(X) B(Y) C(X)" should "contain exactly three predicates" in {
    ABC_WITH_XYX.predicates.length should be (3)
  }
  
  it should "contain exactly two variables" in {
    ABC_WITH_XYX.variables.length should be (2)
  }
  
  it should "contain the variables X and Y" in {
    List(X,Y).foreach(ABC_WITH_XYX.variables.contains(_) should be (true))
  }
  
  it should "have no bound variables" in {
    ABC_WITH_XYX.variables.foreach(_.bound should be (None))
  }
  
  
  "A(X) A(Y) B(Z)" should "contain exactly three predicates" in {
    AAB_WITH_XYZ.predicates.length should be (3)
  }
  
  it should "contain exactly three variables" in {
    AAB_WITH_XYZ.variables.length should be (3)
  } 
  
  it should "contain the variables X,Y and Z" in {
    List(X,Y,Z).foreach(AAB_WITH_XYZ.variables.contains(_) should be (true))
  }
  
  it should "contain no bound variables" in {
    AAB_WITH_XYZ.variables.foreach(_.bound should be (None))
  }
}