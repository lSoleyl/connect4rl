package arch2.base

package object predicates {
  type Predicate[T] = Predicate1[T]
  type BoundPredicateState[T] = List[ChainPredicate[T]]
}