package arch2.base.predicates

case class BoundVariable[T](variable:Variable[T], value:T)