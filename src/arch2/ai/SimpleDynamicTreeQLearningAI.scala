package arch2.ai

import arch2.data.experience._
import arch2.base._
import arch2.data._
import arch2.algorithm.QLearning
import arch2.base.predicates._
import arch2.config.RewardConfig

/** This agent represents the CARCASS learning structure by a tree.
 *  This agent does not implement the reordering of the tree's nodes and is therefore 
 *  algorithmically equivalent to DynamicCQLearningAI, but achieves faster simulation speeds.
 *  
 *  This agent has been renamed, after implementing the reordering tree learner. (DTQL)
 */
case class SimpleDynamicTreeQLearningAI (val experience:CARCASSTreeExperience with HeaderV1, l:AILoader) extends AbstractAgentAI(new QLearning(experience),l)(TypeMapping.simplePredicateState) with ExtendedPredicateState  with PredicateStateUnifier[Int,Double] {
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone) = selectAction(state, 0, actions)
  
  override def newGame(config:Seq[Any]) {
    config.foreach(_ match {
      case r:RewardConfig => {
        //Set threshold for abstraction
        experience.setTDEThreshold((r.won - r.lost) * 10)
      }
      case _ =>
    })
    
    super.newGame(config)
  }
  
  val defaultVariable = Variable('?')
  val variableFactory = Variable.get _
  
  override val notifyExperience = experience.notifyExperience _
}

object SimpleDynamicTreeQLearningAI {
  val Loader = GenericAILoader("SDTQLearningAI", new CARCASSTreeExperience with HeaderV1, SimpleDynamicTreeQLearningAI.apply, GenericAILoader.CANNOT_PLAY_AGAINST_ITSELF)
}