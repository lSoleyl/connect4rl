package arch2.gui

import scala.swing._

/** This button call a given callback function whenever he is pressed. Before that he disables himself
 *  to prevent multiple execution. To reanable the button, the called function must call the callback function,
 *  which is passed to that function.
 *  This is a bit more complicated but not as Threadintensive as blocking Futures with Await 
*/
class CallbackButton[T](label:String, callbackFunction:(=>Unit)=>T, toolTip:String = null) extends Button {
  
  action = new Action(label) {
    def apply = {
      enabled = false
      callbackFunction(reenable)
    }
  }
  
  
  /** Execute the button's action and return the action's return value.
   *  The button is disabled during the action's execution and gets reenabled after the reenable callback got called.
   *  
   *  @return the actions return value
   */
  def execute:T = {
    Swing.onEDT { action.enabled = false }
    callbackFunction(reenable)
  }
  
  /** This callback should be called when the button should be reenabeled.
   *  Since button and action have seperate 'enabled' state variables, we must refer
   *  to the same one on every change, which is 'action.enabled'.
   *  
   *  This function is declared private, to prevent accidential reactivation of the button and thus breaking the state.
   */
  private def reenable { Swing.onEDT { action.enabled = true } }
  
  text = label
  tooltip = toolTip
}