package exp

import org.scalatest._
import arch2.data.experience._
import arch2.base.predicates.PredicateStateUnifier
import arch2.algorithm.QLearning
import arch2.data.experience.base.Experience
import arch2.base.predicates._
import arch2.math.IncrementingGenerator
import arch2.math.RandomGenerator
import arch2.base.Agent
import arch2.math.ZeroGenerator
import utils.Times._

/** A player, which always selects ? as his action
 *  The concrete action should be determined by the random generator alone
 */
class GenTestPlayer(gen:RandomGenerator) extends PredicateStateUnifier[Int,Double] {
  val agent = new Agent[UnboundPredicateState[Int], Variable[Int], Double] {
    val experience = new CARCASSExperience
    def finalReward(reward:Double) {}
    def initialize(params: LearningParameters) {}
    
    def selectAction(state:UnboundPredicateState[Int], reward:Double, possibleActions:List[Variable[Int]]):Variable[Int] = {
      return defaultVariable //Always choose '?' action, no matter what state we are in
    }
    
    protected def isBetter(v1:Value, v2:Value):Boolean = ???
  }
  val defaultVariable:Variable[Int] = Variable('?')
  val variableFactory = Variable.get _
  
  override val notifyExperience:(CS,AS,AA,CA)=>Unit = (cs,as,aa,ca) => {}
  
  override val random:RandomGenerator = gen
}

/** Actual tests for the use of random generators
 *  
 */
class TestRandomGenerator extends FlatSpec with Matchers {
  
  val inc  = new GenTestPlayer(new IncrementingGenerator)
  
  val actions = List(13,24,35,14,25,28, 0, -23) //These actions make no sense, but this shouldn't matter for this test
  
  "Using the incrementing generator" should "reproduce the list of actions" in {
    actions.foreach(currentAction => {
      inc.selectAction(Nil, 0d, actions) should be (currentAction)
    })
  }
  
  it should "reproduce the list of actions a second time" in {
    actions.foreach(currentAction => {
      inc.selectAction(Nil, 0d, actions) should be (currentAction)
    })
  }
  
  val zero = new GenTestPlayer(new ZeroGenerator)
  
  "Using the zero generator" should "always select the first action" in {
    30.times { 
      zero.selectAction(Nil, 0d, actions) should be (actions.head)
    }
  }
}