package utils

object Convert {
  implicit class Convertible[A](obj:A) {
    def convert[B](conversion:A => B):B = conversion(obj)
    def changeIf(predicate:A => Boolean)(conversion: A=>A):A = if (predicate(obj)) conversion(obj) else obj
  }
}