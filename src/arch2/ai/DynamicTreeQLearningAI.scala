package arch2.ai

import arch2.data.experience._
import arch2.base._
import arch2.data._
import arch2.algorithm.QLearning
import arch2.base.predicates._
import arch2.config.RewardConfig

/** This agent represents the CARCASS learning structure by a tree.
 *  It implements reordering of the tree's nodes to further increase the simulation performance.
 *  The reordering introduces the new issue of multiple states. (This will be fixed soon)
 */
case class DynamicTreeQLearningAI (val experience:SortingCARCASSTreeExperience with HeaderV1, l:AILoader) extends AbstractAgentAI(new QLearning(experience),l)(TypeMapping.simplePredicateState) with ExtendedPredicateState  with PredicateStateUnifier[Int,Double] {
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone) = selectAction(state, 0, actions)
  
  override def newGame(config:Seq[Any]) {
    config.foreach(_ match {
      case r:RewardConfig => {
        //Set threshold for abstraction
        experience.setTDEThreshold((r.won - r.lost) * 10)
      }
      case _ =>
    })
    
    super.newGame(config)
  }
  
  val defaultVariable = Variable('?')
  val variableFactory = Variable.get _
  
  override val notifyExperience = experience.notifyExperience _
}

object DynamicTreeQLearningAI {
  val Loader = GenericAILoader("DTQLearningAIv2", new SortingCARCASSTreeExperience with HeaderV1, DynamicTreeQLearningAI.apply, GenericAILoader.CANNOT_PLAY_AGAINST_ITSELF)
}