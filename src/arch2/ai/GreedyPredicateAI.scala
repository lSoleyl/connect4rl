package arch2.ai

import arch2.base._
import arch2.field.Stone
import scala.util.Random
import arch2.base.predicates.BoundPredicateState
import arch2.data.AILoader

/** This AI greedily exploits the predicate state description.
 *  It exploits the predicate ordering and the fact that actions in the center
 *  are better than actions on the edge of the field.
 *  
 *  This AI represents the best and most reasonable strategy, known to "me".
 *  The target of the predicate Q-Learning agent is to outperform this one.
 */
class GreedyPredicateAI(override val loader:AILoader) extends Player with PredicateState with StrictCenterBias {
  /** The Agent first filters out the most important predicates and then  
   *  randomly selects an action from these. Actions in the center of the 
   *  field are favoured. If this yields no action then a random action is selected
   *  from the given action list.
   *  
   *  @param state the current state as list of predicates
   *  @param actions the currently available actions
   *  @param color the color of the current player (irrelevant ot the strategy)
   * 
   *  @return the selected action
   */
  def turn(state:BoundPredicateState[Int], actions:List[Int], color:Stone):Int = {
    val bestActions = selectMostImportantPredicates(state).map(_.column)
    
    if (!bestActions.isEmpty)
      selectRandomActionWithCenterBias(bestActions)
    else
      selectRandomActionWithCenterBias(actions)
  }
  
  /** This method filters the most "important" predicates which are the first ones in the sorted
   *  predicate list. This method returns a list of predicates of the same type as the first predicate.
   *  In essence this means that all predicates of this returned list share the same class and the same
   *  chainLength.
   *  
   *   @param state the current state to filter
   *   
   *   @return the list of really relevant predicates
   */
  private def selectMostImportantPredicates(state:BoundPredicateState[Int]):BoundPredicateState[Int] = {
    if (state.isEmpty)
      Nil
    else {
      val first = state.head
      state.filter(p => p.id == first.id  && p.chainLength == first.chainLength)
    }
  }
}