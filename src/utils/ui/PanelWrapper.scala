package utils.ui

import scala.swing._

/** This utility allows to wrap an elment into a flow panel just by calling flow on it
 */
object PanelWrapper {
  implicit class PanelWrapper(c:Component) {
    def flow = new FlowPanel { contents += c }
    def box(align:Orientation.Value = Orientation.Horizontal) = new BoxPanel(align) { contents += c } 
  }
}