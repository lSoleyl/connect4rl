package arch2.game

import arch2.base._
import arch2.gui._
import scala.util.Try
import scala.swing._

object SimpleBatchRunner extends BatchRunner {
  def start(p1:Player, p2:Player, config:ConfigurationData, batchConfig:BatchConfiguration):GameStatistics =  {
    val field = Field
    val episodes = batchConfig.simple.episodes
    val progScreen = new BatchProgressScreen(episodes)
    
    progScreen.start
    
    val result = Try { GameManager.batchRun(p1, p2, field, config, episodes, progScreen.progress, progScreen)  }
    
    progScreen.stop
    Swing.onEDT { progScreen.close }
    
    result.get //This call either rethrows or returns the current result
  }
}