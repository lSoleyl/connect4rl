package arch2.game

import arch2.base.Player

/** A result object which describes the outcome of a single game
 */
case class GameResult(winner:Option[Player], startingPlayer:Player, turns:Int)