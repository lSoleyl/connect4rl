package arch2.data.experience.base 

/** This trait is motivated by the bad look up peformance of queues and lists.
 *  Each lookup is O(n/2) on the average. To compensate for this, this class exploits a certain
 *  behavior in the QLearning implementation. 
 *   - QLearning always first looks up the Q-Value of a state and then
 *   - updates the Q-Value of the previous lookup.
 *   
 *  So a cache of size 2 effectively prevents the second lookup when updating the Q-Value.
 */
trait PredicateCachedExperience[A,V] extends PredicateQueueExperience[A,V] {
  var c1:ListEntry = ListEntry(null,null)
  var c2:ListEntry = ListEntry(null,null)
  
  private def enterCache(x:ListEntry):ListEntry = {
    c1 = c2
    c2 = x
    x
  }
  
  
  override def apply(key:SA):Value = {
    for (entry <- experience) {
      if (entry.state == key.s) {
        return enterCache(entry).actionMap.getOrElse(key.a, default)
      }
    }
    
    return default
  }
  
  override def update(key:SA, value:Value) {
    if (c1.state == key.s) {
      c1.actionMap(key.a) = value
    } else if (c2.state == key.s) {
      c2.actionMap(key.a) = value
    } else {
      super.update(key,value)
    }
  }
}