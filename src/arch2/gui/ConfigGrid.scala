package arch2.gui

import scala.swing._

/** This class is a very small wrapper around the Grid panel with columns set to 2 and
 *  a shorter way of adding new entries.
 */
class ConfigGrid(lines:Int) extends GridPanel(lines, 2) {
  def entry(entry:(String, Component)) { 
    contents += new Label(entry._1)
    contents += entry._2
  }
}
