package arch2.base

/** This trait is used to compare all types of objects.
 *  It is provided as type class to prevent unnecessary conversions for
 *  base types
 */
trait Order[T] {
  type X = T
  
  //Return a negative number if a comes before b, otherwise a positive
  //If both are equal then return 0
  def apply(a:X, b:X):Int
}

object Order { //When searching for implict values for Order[T] scala looks inside the compainion object of Order[T]
  import arch2.base.predicates.Variable
    
  implicit object IntOrder extends Order[Int] { def apply(a:Int, b:Int) = a-b }
  implicit def VarOrder[T]:Order[Variable[T]] = new Order[Variable[T]] { def apply(a:X, b:X) = a.id - b.id }
}