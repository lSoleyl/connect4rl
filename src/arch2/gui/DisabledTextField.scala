package arch2.gui

import scala.swing._
import java.awt.Color._

class DisabledTextField extends TextField {
  enabled = false
  peer.setDisabledTextColor(BLACK)
}