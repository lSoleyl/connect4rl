package predicates

import org.scalatest._
import arch2.data.experience.base.PredicateTreeExperience
import arch2.base.predicates.Variable
import arch2.base.predicates.UnboundPredicateState
import scala.collection.mutable.Map
import arch2.base.predicates.ChainingPredicate
import arch2.base.predicates.BlockPredicate

class PredicateTree extends FlatSpec with Matchers {
  val fourk = new UnboundPredicateState(List(ChainingPredicate(4, Variable('X'))))
  val fourb = new UnboundPredicateState(List(BlockPredicate(4, Variable('X'))))
  val emptyState = new UnboundPredicateState[Int](Nil)
  
  //Create test object
  val exp = new PredicateTreeExperience[Int,Double] {
     val defaultVariable:Variable[Int] = Variable('?') //Variable for random action '?'
     val defaultVariableStartValue:Double = -1d
     val default:Double = 0d
  
  
     protected var experience:TreeNode = new TreeNode(emptyState, Map())
     
     enterState(fourk, experience.state)
     enterState(fourb, experience.state)
     
     def exp = experience
  }
  
  "Default experience" should "contain empty state" in {
    exp.exp.find(emptyState) should be (Some(exp.exp))
    exp.exp.find(emptyState).get.state should be (emptyState)
  }
  
  it should "contain the state 4k(X)" in {
    val result = exp.exp.find(fourk)
    result.isEmpty should be (false)
    result.get.state should be (fourk)
  }
  
  it should "contain the state 4b(X)" in {
    val result = exp.exp.find(fourb)
    result.isEmpty should be (false)
    result.get.state should be (fourb)
  }
  
  
}