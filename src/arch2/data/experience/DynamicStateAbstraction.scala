package arch2.data.experience

import arch2.base.predicates.UnboundPredicateState
import arch2.base.predicates.ChainPredicate
import arch2.base.predicates.Variable
import scala.collection.mutable.Map
import utils.Log
import arch2.base.predicates.Constraint
import utils.io.ByteWriter
import utils.io.ByteReader
import arch2.data.ProgressInfo
import arch2.data.Serializer
import utils.Times._

/** This trait provides the ability to derive new states from observing traces over a period of time.
 *  To use this trait, the experience class has to:
 *   * set the tdeThreshold to a reasonable value eg. (r_max - r_min)*10
 *   * implement insertState(newState,derivedFrom) as specified in the methods comment
 *   * call addTrace, every time, the agent has the information tuple: ConcreteState, AbstractState, AbstractAction, ConcreteAction, TDError*alpha
 *   * To make the agent resumable, it must use tempDataSize, tempToStream, tempFromStream to save and load the data of this trait appropriately
 */
trait DynamicStateAbstraction extends Log {
  
  type CA = Int //TODO factor this out into a type parameter or something like that
  type AS = UnboundPredicateState[CA]
  type AA = Variable[CA]
  type TEntry = TraceEntry[List[ChainPredicate[CA]],AS,AA,CA]
  
  /** This method gets called whenever a new state has been derived and should be entered into
   *  the state space. It provides the state, which was derived and the state it was derived from.
   *  This is the only method, which must be implemented by the user of this trait.
   *  
   *  @param newState the state which was derived
   *  @param derivedForm the state it was derived from
   *  
   *  @return true if insertion is successful, false, if the state already exists 
   */
  def insertState(newState:AS, derivedFrom:AS):Boolean
  
  //If not set, the abstraction is completely disabled, this has to be set by the deriving class 
  protected var tdeThreshold:Double = Double.NaN 

  
  /** Call this method to add a new trace entry into the trace list
   * 
   */
  def addTrace(trace:TEntry) {
    traces = trace :: traces //Add entry to traces
    
    val key = (trace.abstractState, trace.abstractAction)
    val totalTde = Math.abs(trace.tde) + tdeMap.getOrElse(key, 0d)
    tdeMap(key) = totalTde
    
    if (totalTde > tdeThreshold)
      think(key)
    
    //Cut off traces of they are too long but don't use trace.length, since it's O(n)
    if (traceLength > 100000) {
      traces = Nil
      tdeMap.clear
    }
    
    if (traces != Nil) { //Traces didn't got cut
      traceLength += 1
    } else {
      traceLength = 0
    }
  }
  
  /** Small method to clear all temporary data
   */
  protected def clearTempData {
    tdeMap.clear
    traces = Nil
    traceLength = 0;
  }
  
  
  protected def tempDataSize = TempDataSize(traceLength,tdeMap.size)
  
  /** Write temp data to stream
   */
  protected def tempToStream(progress:ProgressInfo)(implicit writer:ByteWriter) {
    import arch2.data.Serializer._
    for(tEntry <- traces) {
      serialize(tEntry)      
      progress.next
    }
    
    for(((state,action),value) <- tdeMap) {
      serialize(state)
      serialize(action)      
      serialize(value)
      progress.next
    }
  }
  
  /** Load temp data from stream (traces are read in in reverse order, but it doesn't affect the abstraction algorithm)
   */
  protected def tempFromStream(nTraces:Int, nMapEntries:Int, progress:ProgressInfo)(implicit reader:ByteReader) {
    import arch2.data.Serializer._
    
    traceLength = nTraces
    traces = Nil
    traceLength.times {
      traces = load[TEntry] :: traces
      progress.next      
    }
    
    tdeMap.clear
    nMapEntries.times {
      val key = (load[AS],load[AA])
      tdeMap(key) = load[Double]
    }    
  }
  
  
  /** This method is trying to derive new states from the data gathered throughout the last episodes.
   *  
   *  @param sa the state action pair which exceeded the absolute td error value
   */
  private def think(sa:(AS,AA)) {    
    //First filter out traces, which are irrelevant for deriving a new state
    val importantTraces = traces.filter(entry => entry.abstractState == sa._1 && entry.abstractAction == sa._2)       //select traces for current SA
                                .filter(entry => entry.concreteState.exists(p => p.argument == entry.concreteAction)) //ensure, selected actions can be bound to a predicate argument
                                .sortBy(entry => -entry.tde)                                                          //Order descending in regard to tde
    
                          
    //This happens if all traces are traces where ? gets bound to a value, which isn't bound to a predicate
    if (importantTraces.isEmpty) {
      log.info(s"[ERR1] No state derivable for SA: $sa, resetting accumulated TDE")
      tdeMap(sa) = 0d
      return
    }                                    
    
    /* Now comes the most complicated part:
     *   1. Find the predicate, which binds the selected action and ensure that all traces contain that predicate
     *   2. Find all other predicates which might be relevant for the new state's description 
     */
                                      
    val X = Variable('X') //X gets bound to the actually chosen action
    
    import utils.generic.MA._
    
    val targetTraces = importantTraces.context(trace => trace.concreteState.partMap(p => p.argument == trace.concreteAction)
                                                                                   (_.replaceArgument(X))
                                                                                   (_.mapArgument(_ - trace.concreteAction)))
    
    //Order the target predicate sets by occurrence
    //val targetPredicateSets = targetTraces.map(_.context.t).occurrenceList  //<- By occurrence desc
    //val targetPredicateSets = targetTraces.valueList(- _.value.tde)(_.context.t) //<- by TDE desc
    val targetPredicateSets = targetTraces.valueList(x => - Math.abs(x.value.tde))(_.context.t) //<- By delta desc
    
    //Iterate over all possible target predicates 
    val derivedState = targetPredicateSets.findFirst { targetPredicateSet =>
    
      //Find the first targetpredicateset for which a new unique predicate can be derived
      val relevantTraces = targetTraces.filter(_.context.t == targetPredicateSet)
      
      //val constraintSets = relevantTraces.map(_.context.f).occurrenceList //<- by occurrence desc
      //val constraintSets = relevantTraces.valueList(- _.value.tde)(_.context.f) //<- by TDE desc
      val constraintSets = relevantTraces.valueList(x => -Math.abs(x.value.tde))(_.context.f) //<- By delta desc
                                       
        
      constraintSets.findFirst { constraintPredicates =>
        val variables = constraintPredicates.map(_.argument).toSet.zipWithIndex.map(kv => (kv._1, Variable(kv._2))).toMap 
        val state =  new UnboundPredicateState(targetPredicateSet ++ constraintPredicates.map(_.mapArgument(variables(_))))
        variables.foreach(dv => state.constrain(X, Constraint(dv._1, dv._2))) //Add constraints to the state  
        
        if (insertState(state,sa._1)) {
          Some(state)
        } else {
          None
        }
      }      
    }
      
    //This can only occurr if the state description isn't accurate enough to differentiate between two very different concrete states
    if (derivedState.isEmpty) {  
      log.info(s"[ERR2] Can't derive new state from $sa, because of description restrictions")
    } 
    
    //Reset the trace record and tde values 
    traces = Nil
    tdeMap.clear
  }
  
  
  private var traces:List[TEntry] = Nil
  private var traceLength:Int = 0     //Keep length of traces in a variable to prevent traversing the whole trace
  private val tdeMap:Map[(AS,AA),Double] = Map()
  
  
  
  
  
  case class TempDataSize(traceLength:Int, tdeMapSize:Int)
  
  
  
  case class TraceEntry[CS,AS,AA,CA](concreteState:CS, abstractState:AS, abstractAction:AA, concreteAction:CA)(val tde:Double) {
    override def toString = f"TraceEntry($concreteState, $abstractState, $abstractAction, $concreteAction, $tde%6.3f)" 
  }
  
  object TraceEntry {
    
    implicit def serializer[CS, AS, AA, CA](implicit cs:Serializer[CS], as:Serializer[AS], aa:Serializer[AA], ca:Serializer[CA]):Serializer[TraceEntry[CS,AS,AA,CA]] = new Serializer[TraceEntry[CS,AS,AA,CA]]{
      override def write(te:Type)(implicit writer:ByteWriter) {
        import te._
        cs.write(concreteState)
        as.write(abstractState)
        aa.write(abstractAction)
        ca.write(concreteAction)
        writer.writeDouble(tde)
      }
      
      override def load()(implicit reader:ByteReader):Type = TraceEntry(cs.load, as.load, aa.load, ca.load)(reader.readDouble)
    }
  }
}