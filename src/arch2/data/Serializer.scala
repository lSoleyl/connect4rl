package arch2.data

import arch2.base.predicates._
import arch2.base.Order

trait Serializer[T] {
  type Type = T
  type ByteWriter = utils.io.ByteWriter
  type ByteReader = utils.io.ByteReader
  
  def write(obj:T)(implicit writer:ByteWriter)
  def load()(implicit reader:ByteReader):T
}



object Serializer {
  //This class itself is actually a design flaw... Unboundpredicate state doesn't know ChainPredicate, but it holds only predicates of this type
  implicit def predicateSerializer[T](implicit ps:Serializer[ChainPredicate[T]]):Serializer[Predicate[T]] = new Serializer[Predicate[T]] {
    override def write(p:Type)(implicit writer:ByteWriter) { ps.write(p.asInstanceOf[ChainPredicate[T]]) }
    override def load()(implicit reader:ByteReader) = ps.load    
  } 
  
  
  implicit def chainPredicateSerializer[T](implicit as:Serializer[T], o:Order[T]):Serializer[ChainPredicate[T]] = new Serializer[ChainPredicate[T]] {
    override def write(p:Type)(implicit writer:ByteWriter) {      
      import writer._
      
      val pByte:Byte = ((if(p.id(1) == 'k') 1 else 0) | (p.chainLength << 1)).toByte
      writeByte(pByte)
      as.write(p.argument)
    }
    
    
    override def load()(implicit reader:ByteReader) = {
      import reader._
      
      val pData = readByte
      val chainLength = pData >> 1
      
      val argument = as.load
      
      val name = chainLength + (if((pData & 1) == 1) "k" else "b")  
      new ChainPredicate(chainLength, argument, name)
    }
    
  }
  
  implicit def queueSerializer[T](implicit es:Serializer[T]):Serializer[scala.collection.immutable.Queue[T]] = new Serializer[scala.collection.immutable.Queue[T]] {
    import utils.Times._
    import scala.collection.immutable.Queue
    
    override def write(queue:Type)(implicit writer:ByteWriter) {
      writer.writeInt(queue.length)
      queue.foreach(es.write)
    }
    
    override def load()(implicit reader:ByteReader) = Queue(reader.readInt.collect(es.load) :_*)
  } 
  
  implicit def listSerializer[T](implicit es:Serializer[T]):Serializer[List[T]] = new Serializer[List[T]] {
    import utils.Times._
    
    override def write(list:Type)(implicit writer:ByteWriter) {
      writer.writeInt(list.length)
      list.foreach(es.write)
    }
    
    override def load()(implicit reader:ByteReader) = reader.readInt.collect(es.load)
  }
  
  implicit def pairSerializer[A,B](implicit as:Serializer[A], bs:Serializer[B]):Serializer[(A,B)] = new Serializer[(A,B)] {
    override def write(pair:(A,B))(implicit writer:ByteWriter) {
      as.write(pair._1)
      bs.write(pair._2)
    }
    
    override def load()(implicit reader:ByteReader) = (as.load, bs.load)
  }
  
  implicit object IntVarSerializer extends Serializer[Variable[Int]] {
    override def write(v:Type)(implicit writer:ByteWriter) {
      writer.writeByte(v.id.toByte)
    }
    
    override def load()(implicit reader:ByteReader) = Variable(reader.readByte.toChar)
  }
  
  implicit object IntConstraintSerializer extends Serializer[Constraint[Int]] {
    override def write(c:Type)(implicit writer:ByteWriter) {
      writer.writeInt(c.offset)
      serialize(c.variable)
    }
    
    override def load()(implicit reader:ByteReader) = {
      val offset = reader.readInt
      val v = Serializer.load[Variable[Int]]
      
      Constraint(offset,v)
    }
  }
  
  implicit def optionSerializer[T:Serializer]:Serializer[Option[T]] = new Serializer[Option[T]] {
    override def write(o:Option[T])(implicit writer:ByteWriter) {
      if (o.isEmpty)
        writer.writeBool(false)
      else {
        writer.writeBool(true)
        serialize(o.get)
      } 
    }
    
    override def load()(implicit reader:ByteReader) = if (!reader.readBool) None else Some(Serializer.load[T])
  }
  
  implicit object DoubleSerializer extends Serializer[Double] {
    override def write(d:Type)(implicit writer:ByteWriter) {
      writer.writeDouble(d.asInstanceOf[Double]) 
    }
    
    override def load()(implicit reader:ByteReader) = reader.readDouble
  }
  
  implicit object IntSerializer extends Serializer[Int] {
    override def write(i:Int)(implicit writer:ByteWriter) {
      writer.writeInt(i)
    }
    
    override def load()(implicit reader:ByteReader) = reader.readInt 
  }
  
  
  def serialize[T](obj:T)(implicit writer:utils.io.ByteWriter, s:Serializer[T]) { s.write(obj) }
  def load[T](implicit reader:utils.io.ByteReader, s:Serializer[T]):T = s.load
  def deserialize[T](implicit reader:utils.io.ByteReader, s:Serializer[T]):T = s.load //Alias for load
}