package utils.generic


case class MappedPartition[M[_],A,B](val t:M[A], val f:M[B])