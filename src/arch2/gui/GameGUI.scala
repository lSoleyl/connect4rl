package arch2.gui

import scala.swing._
import utils.Log
import javax.swing.Icon
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import arch2.field._
import arch2.field.Stone._
import arch2.base._
import arch2.config._
import arch2.game._
import arch2.data.AILoader
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import arch2.game.GameManager
import utils.TimeMeasureMS
import scala.util.{Try, Success, Failure}
import java.io.File
import scala.swing.event._
import utils.ui.PanelWrapper._
import utils.Options._
import utils.Tap._

object GameGUI extends SimpleSwingApplication with Log {
  var config = Configuration.load
  var batchConfig = BatchConfiguration.load
    
  val player1 = new ComboBox(AILoader.loaderList(config.agentDir))
  val player2 = new ComboBox(AILoader.loaderList(config.agentDir))
  
  val GUI_PLAYER_DELAY = 50 //ms
  
  val gameButton = new ActionButton("start game", loadGame)
  val resumeButton = new CallbackButton("resume run", resumeRun)
  
  var task:()=>Unit = null
  
  def top = new MainFrame {
    title = "Connect 4 RL"
      
    contents = new BorderPanel {
      add(new GridPanel(2,3) {
        contents += new Label("Yellow")
        contents += new Label("")
        contents += new Label("Red")
        contents += player1
        contents += new Label(" vs. ")
        contents += player2        
      }, BorderPanel.Position.North)
      
      
      add(new BoxPanel(Orientation.Horizontal) {
        contents += new ActionButton("configuration", openConfig)
        contents += new CallbackButton("agent list", openAgentList, "view loaded agents")
        contents += resumeButton
        contents += new ActionButton("result editor", editResults)
        contents += new ActionButton("load agents", loadAgents, "load selected agents")
      }.flow, BorderPanel.Position.Center)
    
      add(gameButton, BorderPanel.Position.South)
      
    }
    
    if (task != null) task()
  }
  
  override def main(args:Array[String]) {
    parseArgs(args.toList)
    
    super.main(args)
  }
  
  private def clamp(min:Int, max:Int, value:Int) = Math.min(Math.max(min, value), max)
  
  def printHelp(unknownOption:Option[String] = None) {
    unknownOption iff { opt => 
      println(s"Unknown command line option: '$opt'")
    }
    
    println("Allowed options:")
    println("--play <player1> <player2>  - Start game between player1 and player2 when launching")
    println("--resume <selector>         - Resume a previously suspended run. Valid selectors are: " +
            "(latest|first)[(+|-)offset] eg. latest-1, first+3, latest, first, ...")
    println("--help                      - Prints help this message\n")
  }
  
  
  def parseArgs(args:List[String]) {
    args match {
      case Nil =>
      case "--play" :: p1 :: p2 :: tail => {
        task = { selectPlayers(p1,p2); gameButton.doClick }
        parseArgs(tail)
      }
      case "--resume" :: RunSelector(index) :: tail => {
        task = () => {
          resumeButton.execute iff { resumeWin =>
            Swing.onEDT { resumeWin.peer.toFront() }
            val entries = resumeWin.fileSelect.listData.size
            resumeWin.fileSelect.selectIndices(clamp(0,entries-1,(if (index < 0) entries+index else index)))
            resumeWin.resumeButton.doClick
          } 
        }
        parseArgs(tail)
      }
      case "--resume" :: x :: tail => {
        println("Unknown selector '" + x + "' for --resume option")
        printHelp()
      }
      case "--help" :: tail => printHelp()
      case x :: tail => printHelp(Some(x)) //Unknown option
    }
  }
  
  
  
  /** Launches the result editor
   */
  def editResults {
    new ResultEditor().open
  }
  
  
  def selectPlayers(p1:String, p2:String) {
    val loaderList = AILoader.loaderList(config.agentDir)
    
    //Search for matching loader entry
    val List(l1,l2) = List(p1,p2).map(pName => loaderList.find(_.toString == pName) or loaderList.find(_.toString.startsWith(pName))).map(_ default null)
    player1.selection.item = l1
    player2.selection.item = l2
  }
  
  /** Loads the selected agents
   */
  def loadAgents {
    val l1 = player1.selection.item
    val l2 = player2.selection.item
    
    if (l1 == null || l2 == null) {
      reportError("Select a player!")
    } else {       
      logDebug(s"Acquiring AILoader locks for '$l1' and '$l2'")
       
      utils.Acquire(l1,l2) { 
        logDebug("Acquired all locks")
        //First load players
        (Try {
          log.info("Loading agents..")
          val (p1,p2) = l1.loadPlayers(l2) { new LoadingScreen(_).load() } 
          log.info("agents loaded")
        }).recover({
          case ex => {
            if (l1.isLoaded && l2.isLoaded) //Exception thrown, because both players are equal
              log.info("agents loaded")
            else
              throw ex
          }
        }).recover({
          case ex => {
            log.error("loading agents failed", ex)
            reportError("failed to load agents, see log for details.")
          }
        })        
      }
    }
  }
  
  /** Searches for a resumable batch run and executes it.
   *  Buttons stays disabled, until callback is invoked (after batchrun is completed) 
   *  
   */
  def resumeRun(callback: =>Unit):Option[ResumeWindow] = {
    var directory = new File(config.agentDir)
    val files = if (directory.exists()) directory.listFiles.toList.map(_.getAbsolutePath).filter(_.endsWith(".resume")) else Nil
    
    if (!files.isEmpty) {
      Swing.onEDT { gameButton.enabled = false }
      Some(new ResumeWindow(files, { 
        Swing.onEDT { gameButton.enabled = true }
        callback
      }).tap(_.open))
    } else {
      reportError(s"Nothing found to resume (searching in: '${new File(config.agentDir).getAbsolutePath}')")
      callback
      None
    }
  }
  
  
  /** Opens Agent list and lets the button disabled until it gets
   *  enabled by invoking the callback, what will happen, when the AgentListWindow
   *  will be closed.
   */
  def openAgentList(callback: =>Unit) {
    new AgentListWindow(callback).open
  }
  
  /** Opens a configuration window and lets the button disabled (Await.result) as long as
   *  the configuration window is still open.
   */
  def openConfig {
    log.info("Configuration is being opened")
    
    ConfigWindow.openWait(config) match {
      case Some(cfg) => {
        Configuration.save(cfg)
        config = cfg
        log.info("Configuration changed and saved to disk")
      }
      case None => log.info("Configuration is left unchanged")
    }
  }
  
  def reportError(msg:String) {
    Swing.onEDT {
      Dialog.showMessage(message = msg, title = "error", messageType = Dialog.Message.Error)
    }
  }
  
  
  def loadGame {
    val l1 = player1.selection.item
    val l2 = player2.selection.item
    
    if (l1 == null || l2 == null) {
      reportError("Select a player!")
    } else if (l1.compare(l2) == 0 && !l1.canPlayAgainstItself) {
       //TODO this should be possible by instantiating another agent instance through the loader
      reportError("This agent cannot play against itself")
    } else {       
      logDebug(s"Acquiring AILoader locks for '$l1' and '$l2'")
       
      utils.Acquire(l1,l2) { //Acquire locks for the player to prevent them from playing while their experience is being saved
        logDebug("Acquired all locks")
        //First load players
        log.info("Loading players..")
        val (p1,p2) = l1.loadPlayers(l2) { new LoadingScreen(_).load() } 
        log.info("Players loaded")
        
        if (config.batchRun) { //start a batch run
          import arch2.ai.HumanPlayer
          //human players aren't supported in batch runs (for obvious reasons)
          if (p1.isInstanceOf[HumanPlayer] || p2.isInstanceOf[HumanPlayer]) {
            log.error("Selected a human player for a batch run")
            reportError("A human player isn't allowed in a batch run.")
          } else { // no human players
            val field = Field
            val measure = new TimeMeasureMS
            val batchRunner = if (batchConfig.useMultiBatch) MultiBatchRunner else SimpleBatchRunner
            
            val results = Try { measure { batchRunner.start(p1, p2, config, batchConfig) } }
            
            results match { //Handle errors
              case Failure(e) => log.error(s"An error occurred during batch run ${e.toString}", e)
              case Success(res) => {
                new ResultWindow(res)
                log.info(s"Batch run took ${measure.result} ms")
                log.info(f"Overall simuation speed was ${res.totalEpisodes / (measure.result/1000.0)}%.2f ep/sec")
              }
            }
          }
        } else { //GUI-Run
          val List(gp1,gp2) = List(p1,p2).map(new GUIPlayer(_, GUI_PLAYER_DELAY))
          
          val field = Field
          val board = new GameBoard(field, s"${p1} vs. ${p2}")
          log.info("Gameboard created")
          val cfg = List(GameBoardConfig(board), InterfaceConfiguration(board.buttonPanel), LearningParameters(config.alpha, config.gamma, config.epsilon, config.lambda, config.dynamicAlpha, config.e_2), config.rewards)
          
          gp1.newGame(cfg)
          gp2.newGame(cfg)
          log.info("Players have been prepared for game")
          
          board.draw
          
          val cp1 = ColoredPlayer(YELLOW, gp1)
          val cp2 = ColoredPlayer(RED, gp2)
          
          try {
            val result = GameManager.game(cp1, cp2, cp1, field)
            log.info("Result: " + result)
          } catch {
            case e:GameboardClosedException => log.info("Game was aborted by closing the window!")
            case e:Exception => log.error("Game aborted due to an exception!", e)
          }
        }
      }
      logDebug("Locks released")
    }
  }
}

/** This object provides an unapply function to parse the resume selector
 */
object RunSelector {
  def unapply(test:String):Option[Int/*offset (negative if from end)*/] =  {
    val fromEnd = test.startsWith("latest") || test.startsWith("last")
    val remainder = test.replaceFirst("last", "").replaceFirst("latest", "").replaceFirst("first","").replaceFirst("oldest","").
                         replaceFirst("\\+","").replaceFirst("-", "")
    
    if (remainder.isEmpty)
      Some(applyEnd(0,fromEnd))
    else {
      Try { (applyEnd(remainder.toInt, fromEnd)) }.toOption
    } 
  }
  
  def applyEnd(i:Int, fromEnd:Boolean) = if (fromEnd) (-i-1) else i  
}
