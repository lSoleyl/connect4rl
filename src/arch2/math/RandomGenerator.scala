package arch2.math

import scala.util.Random

/** This class is used to control the generation of random numbers for testing purposes
 */
class RandomGenerator {
  /** This method should return a random number in the interval from 
   *  0 (inclusive) to n (exclusive) 
   * 
   *  @param n the interval's size
   *  
   *  @return a random number from that interval
   */
  def nextInt(n:Int):Int = Random.nextInt(n)
}

object RandomGenerator {
  val defaultGenerator = new RandomGenerator
} 

/** A special random generator, which will always return zero
 */
class ZeroGenerator extends RandomGenerator {
  override def nextInt(n:Int):Int = 0  
}

/** This is a determinisitic random generator, which won't generate always the same output, but two instances of
 *  it are guaranteed to generate the same sequence.
 */
class IncrementingGenerator extends RandomGenerator {
  var state:Int = -1
  
  override def nextInt(n:Int):Int = {
    state += 1
    return state % n
  }
}