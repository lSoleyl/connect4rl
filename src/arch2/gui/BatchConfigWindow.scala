package arch2.gui

import scala.swing._
import utils.Log
import utils.ui._
import scala.concurrent._
import scala.concurrent.duration.Duration

private class BatchConfigWindow extends Frame with Log {
  title = "Batch configuration"
  private val p = promise[Unit]
  val future = p.future
  
  //Current config value
  val data = GameGUI.batchConfig
  
  
  val batchType = new ComboBox("Simple" :: "Multi" :: Nil)  
  
  object Simple {
    val episodes = new BigNumberField
  }
  
  object Multi {
    val outputDir = new TextField(10)
    val initialStep = new BigNumberField
    val totalEpisodes = new BigNumberField
    val stepType = new ComboBox(BatchConfiguration.STEPS.values.toList)
  }
  
  contents = new BoxPanel(Orientation.Vertical) {
    contents += new Label("Configuration") { font = font.deriveFont(18f) }.flow
    contents += new ConfigGrid(1) {
      entry("Batch type: " -> batchType)
    }
    
    
    contents += new Label("Simple Batch Configuration") { font = font.deriveFont(14f) }.flow
    contents += new ConfigGrid(1) {
      entry("Episodes" -> Simple.episodes)
    } 
   
    
    
    
    contents += new Label("Multi Batch Configuration") { font = font.deriveFont(14f) }.flow
    contents += new ConfigGrid(4) {
      entry("Output directory" -> Multi.outputDir.flow) //TODO this field is too small and should be a file picker
      entry("Initial step size" -> Multi.initialStep)
      entry("Total episodes" -> Multi.totalEpisodes)
      entry("Step type" -> Multi.stepType.flow)
    }

    contents += new FlowPanel {
      contents += new ActionButton("OK", save)
      contents += new ActionButton("Abort", abort)
    }
  }
  
  
  /** This method will preset the fields according to the configuration data which has been read and then
   *  open the window itself. In contrast to the default open method this one doesn't have to be calld inside the swing worker thread
   */
  override def open {
    batchType.selection.item = if (data.useMultiBatch) "Multi" else "Simple"
        
    Simple.episodes.value = data.simple.episodes
    
    Multi.outputDir.text = data.multi.outputDir
    Multi.initialStep.value = data.multi.initialStep
    Multi.totalEpisodes.value = data.multi.totalEpisodes
    Multi.stepType.value = data.multi.stepType
   
    
    Swing.onEDT {
      super.open
    }
  }
  
  /** Closing the config window equals an abort
   */
  override def closeOperation = abort
  
  
  /** This method is called if the current set values are correct and should be written back into the GameGUI
   */
  def save {
    
    data.useMultiBatch = batchType.selection.item == "Multi"
    
    data.simple.episodes = Simple.episodes.value
    
    data.multi.outputDir = Multi.outputDir.text
    data.multi.initialStep = Multi.initialStep.value
    data.multi.totalEpisodes = Multi.totalEpisodes.value
    data.multi.stepType = Multi.stepType.value
    
    BatchConfiguration.save(data)
    log.info("Batch configuration saved")
    
    p.success(())
    Swing.onEDT { this.close }
  }
  
  /** This method is called if "abort" is clicked or the window is closed
   */
  def abort {
    log.info("Batch configuration closed without changing it")
    p.success(())
    Swing.onEDT { this.close }
  }
}


object BatchConfigWindow {
  /** Call this method to create a new configuration window, open it and get a future
   *  containing the changed configuration values.
   *  
   *  @param config the current configuration
   *  
   *  @return a future containing the changed configuration
   */
  def open:Future[Unit] = {
    val cfgWindow = new BatchConfigWindow()
    cfgWindow.open
    cfgWindow.future
  }
  
  /** Blocking alternative to above call
   */
  def openWait = Await.result(open, Duration.Inf)
}