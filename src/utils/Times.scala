package utils

object Times {
  implicit class TimesObject[T <% Int](i:T) {
    def times(action: =>Unit) {
      var x = 0
      val limit:Int = i
      while (x < limit) {
        action
        
        x += 1
      }
    }
    
    def mapIndex[A](f:Int => A) = {
      val elements:Int = i
      List.tabulate(elements)((x:Int) => x).map(f)
    }
    
    def collect[A](f: =>A) = {
      val elements:Int = i
      (0 until elements).map((x:Int) => f).toList
    }
  }

}