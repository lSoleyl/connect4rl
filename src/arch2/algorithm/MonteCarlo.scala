package arch2.algorithm

import arch2.data.experience.base.Experience
import arch2.data.experience.HeaderV1
import arch2.base._
import scala.collection.mutable.Map
import scala.annotation.tailrec
import utils.Log

class MonteCarlo[S,A](val experience:Experience[S,A,(Double,Int)] with HeaderV1) extends Agent[S,A,(Double,Int)] with Log {
  var epsilon = 0d
    
  type StateEntry = Entry
  
  var states:List[StateEntry] = Nil
  var rewards:List[Double] = Nil
  
  protected def isBetter(v1:Value, v2:Value) = v1._1 > v2._1
  
  /** This method selects an action according to the epsilon greedy policy
   *  
   *  @param state the state the agent is currently in
   *  @param reward the reward for the last state transition
   *  @param possibleActions the currently allowed actions (must not be empty)
   *  
   *  @return an action from the 'possibleActions' list
   */
  override def selectAction(state:S, reward:Double, possibleActions:List[A]):A = {
    val bestAction = bestActionEntry(state, possibleActions)
    logTrace(s"best action: $bestAction")
    
    val selectedAction = applyEpsilonGreedy(bestAction, possibleActions, epsilon)
    
    //Remember entered states and received rewards
    states = selectedAction :: states
    rewards = reward :: rewards
    
    selectedAction.sa.a
  }
  
  /** Will be called whenever the game finishes
   *  
   *  @param reward the last reward
   */
  override def finalReward(reward:Double) {
    rewards = reward :: rewards
    
    applyMCUpdate(states, rewards)
    
    //Reset lists for next game
    states = Nil
    rewards = Nil
    experience.episodes += 1
  }
  
  /** This method applies the Monte-Carlo update according the given state- and rewards list.
   *  
   *  @param revStates a reversed list of encountered states
   *  @param revRewards a reversed list of encountered rewards
   *  @param currentReward the current accumulated reward (default:0)
   */
  @tailrec
  private def applyMCUpdate(revStates:List[StateEntry], revRewards:List[Double], currentReward:Double = 0) {
    if (!revStates.isEmpty) {
      val accReward = currentReward + revRewards.head
      val stateEntry = revStates.head
      
      val encounters = stateEntry.v._2
      val newValue = (stateEntry.v._1 * encounters + accReward) / (encounters + 1) 
      experience(stateEntry.sa) = (newValue,encounters+1)
      applyMCUpdate(revStates.tail, revRewards.tail, accReward)
    }
  }
  
  
  override def initialize(params:LearningParameters) {
    epsilon = params.epsilon
    
    states = Nil
    rewards = Nil
  }
}