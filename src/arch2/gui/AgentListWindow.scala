package arch2.gui

import arch2.data.AILoader
import utils.Options._
import utils.Log
import utils.ui.PanelWrapper._
import scala.swing._
import scala.swing.event.SelectionChanged
import javax.swing.JOptionPane
import javax.swing.ListSelectionModel
import java.io.File
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import javax.swing.filechooser.FileNameExtensionFilter


class AgentListWindow(closeCallback: =>Unit) extends Frame with Log {
  private var isOpen = true
  title = "Loaded Agents"
  val agentList = new ListView {
    preferredSize = new Dimension(300,250) //Somehow acceptable default value
    peer.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
  }
  val source = new DisabledTextField
  val episodes = new DisabledTextField
  val agentType = new DisabledTextField
  val displayName = new DisabledTextField
  val stateCount = new DisabledTextField
  val save = new ActionButton("save", saveAgent)
  val saveAs = new ActionButton("save as...", saveAgentAs)
  val customAction = new ActionButton("Custom Action", customAgentAction)
  
  contents = new BoxPanel(Orientation.Vertical) {
    contents += new Label("Agent list:").flow
    contents += agentList.flow
    contents += new Label("Agent data:").flow
    contents += new BoxPanel(Orientation.Horizontal) {
      
      contents += new BoxPanel(Orientation.Vertical) {
        contents += new Label("Loaded from: ")
        contents += new Label("Epsodes learned: ")
        contents += new Label("States: ")
        contents += new Label("Agent type: " )
        contents += new Label("Display name: " ) 
      }
      
      contents += new BoxPanel(Orientation.Vertical) {
        contents += source
        contents += episodes
        contents += stateCount
        contents += agentType
        contents += displayName
      }
    }
    
    contents += customAction.flow
    
    contents += new BoxPanel(Orientation.Horizontal) {
      contents += save
      contents += saveAs
    }
  }
  
  def reportError(msg:String) {
    Swing.onEDT {
      Dialog.showMessage(message = msg, title = "error", messageType = Dialog.Message.Error)
    }
  }
  
  def customAgentAction {
    if (!agentList.selection.items.isEmpty) {
      val loader = agentList.selection.items.head.asInstanceOf[AILoader]
      loader.customAction
    } else {
      reportError("no agent selected")
    }
  }
  
  def saveAgent {
    if (!agentList.selection.items.isEmpty) {
      Swing.onEDT { saveAs.enabled = false }
      
      val loader = agentList.selection.items.head.asInstanceOf[AILoader]
      logDebug(s"Acquiring lock for $loader to save the agent's experience")
      utils.Acquire(loader) {
        new LoadingScreen(loader).save(None)
      }
      logDebug(s"Lock released for $loader")
      
      Swing.onEDT { saveAs.enabled = true }
    }
  }
  
  def saveAgentAs {
    if (!agentList.selection.items.isEmpty) {
      Swing.onEDT { save.enabled = false }
      
      val loader = agentList.selection.items.head.asInstanceOf[AILoader]
      logDebug(s"Acquiring lock for $loader to save the agent's experience")
      utils.Acquire(loader) {
        
        val fileChooser = new FileChooser(new File(GameGUI.config.agentDir))
        fileChooser.title = "Save as..."
        fileChooser.fileFilter = new FileNameExtensionFilter("Experience", "exp2")
        fileChooser.showSaveDialog(save)
        val file = fileChooser.selectedFile
        if (file != null) {
          //Ensure correct file ending
          val saveFile = if (file.getAbsolutePath.endsWith(".exp2")) file else new File(file.getAbsolutePath + ".exp2")
          val description = JOptionPane.showInputDialog("Please enter a new agent description:")
        
          val header = loader.v1Header
          if (header)
            header.get.agentdescription = Some(description)
          
          try {
            new LoadingScreen(loader).save(Some(saveFile))
          } catch {
            case e:Throwable => log.error("Saving agent failed! Maybe missing write access.", e)
          }
        }
      }
      logDebug(s"Lock released for $loader")
      
      Swing.onEDT { save.enabled = true }
    }
  }
  
  /** Update the upper part of the window this will be called in 
   *  intervals of ~2sec from a seperate thread and the list will be created new from scratch each time.
   *  The selection will be reset to the same item even if the number of items changes.
   */
  def updateAgentList {
    val selectedIndex = agentList.selection.indices.fold(0)((a,b) => b)
    val newList = AILoader.loaderList(null).filter(x => x.isLoaded && x.isWritable).sorted
    
    Swing.onEDT { //Apply changes
      agentList.listData = newList.asInstanceOf[Seq[Nothing]]
      agentList.selectIndices(selectedIndex)
    }
  }
  
  private var lastInfoUpdateCompleted = true
  
  /** This will update the lower part of the window with agent specific data. The method will 
   *  be called every time the selection changes.
   */
  def updateAgentInfo {
    if (!agentList.selection.items.isEmpty) {
      val loader = agentList.selection.items.head.asInstanceOf[AILoader]
      source.text = loader.source.mapOr("Unknown source")(_.getAbsolutePath())
      displayName.text = loader.displayName
      val header = loader.v1Header
      if (header) {
        episodes.text = header.get.episodes.toString
        agentType.text = header.get.agentType default "Unknown"
      } else {
        episodes.text = ""
        agentType.text = "Unknown"
      }
      
      //The info display needs special handling since the info string might take some time to 
      //be computed. QLearningAI had problems with over 1.6 million states and it took several seconds
      if (lastInfoUpdateCompleted) { //Only schedule new update if last one has completed
        lastInfoUpdateCompleted = false
        Future {
          val infoText = loader.stateCount.map(_.toString) default "Not provided"
          Swing.onEDT {
            stateCount.text = infoText
            lastInfoUpdateCompleted = true
          }
        }
      }
    }
  }
  
  
  val x = (toolkit.getScreenSize().getWidth() - bounds.getWidth()).toInt
  logDebug(s"New Window created with screen size: ${toolkit.getScreenSize()}, moved window to ($x,0)")
  peer.setLocation(x, 0) 
  new Thread("AgentListUpdater") {
    override def run {
      while(isOpen) {
        Thread.sleep(AgentListWindow.AGENT_LIST_UPDATE_TIME)
        updateAgentList
      }
    }
  }.start()  
  
  listenTo(agentList.selection) //Register for selection event
  reactions.+= { 
    case ev:SelectionChanged => updateAgentInfo
  }
  
  
  override def closeOperation {
    log.info("Window closed")
    isOpen = false //EndThread
    closeCallback 
  }
}

object AgentListWindow {
  val AGENT_LIST_UPDATE_TIME = 2000
}