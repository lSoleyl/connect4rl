package arch2.data.experience

import arch2.data.ProgressInfo

/** Every data container, which wants to be loadable and
 *  writable must satisfy interface.
 *  
 *  Every method additionally has a progress parameter for progressbar support
 */
trait StreamSerializable {
  type InputStream = java.io.InputStream
  type OutputStream = java.io.OutputStream
  
  def writeToStream(out:OutputStream, progress:ProgressInfo)
  def loadFromStream(in:InputStream, progress:ProgressInfo)
}