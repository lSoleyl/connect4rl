package utils.ui

/** Small combobox utility 
 */
object ComboBox {
  implicit class RichComboBox[A](cb:scala.swing.ComboBox[A]) {
    def value = cb.selection.item
    
    def value_=(obj:A) {
      cb.selection.item = obj
    }
  }
}