package arch2.game


import arch2.config
import arch2.base.Player
import arch2.gui.ConfigurationData
import arch2.gui.BatchConfiguration

trait BatchRunner {
  def start(p1:Player, p2:Player, config:ConfigurationData, batchConfig:BatchConfiguration):GameStatistics
  
  
}

