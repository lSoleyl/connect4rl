package arch2.game

import utils.Factors._
import arch2.base.Player
import arch2.field.Stone._
import scala.annotation.tailrec
import utils.TimeMeasureMS
import arch2.base.ColoredPlayer
import arch2.gui.ConfigurationData
import arch2.data.ProgressInfo
import utils.Log
import arch2.config.LearningParameters

/** This class is responsible for initiating the game by
 *  instantiating the correct player objects, loading their
 *  learn data and then starting the game.
 * 
 */
object GameManager extends Log {
  
  
  /** This tail recursive definition is playing a whole game of 4 wins until either of the players has
   *  won or the game ends tied. The game result will be returned in a GameResult object.
   *  
   *  @param current the player who is the next in turn
   *  @param other the other player
   *  @param startingPlayer the player who started the game if this is the first turn, this should equal 'current'
   *  @param field the field on which the players should play their game. It must be cleared before calling this function
   *  @param turn the current turn. An internal variable, used for the GameResult
   *  
   *  @return a GameResult object which summarizes the game
   */
  @tailrec
  def game(current:ColoredPlayer, other:ColoredPlayer, startingPlayer:ColoredPlayer, field:Field, turn:Int = 1):GameResult = {
    val actions = field.getFreeColumns
    
    if (actions.isEmpty) {
      current.player.tied
      other.player.tied
      GameResult(None, startingPlayer, turn-1) //Draw
    } else {
      val slot = current.player.turn(field, actions, current.color)
      other.player.enemyTurn(field, actions, slot)
      val row = field.putIntoSlot(slot, current.color)
      if (field.getLines(slot, row, current.color).exists(_.length >= 4)) {
        logTrace(s"${current.player} won in slot $slot and row $row with color: ${current.color}")
        current.player.won
        other.player.lost
        GameResult(Some(current), startingPlayer, turn) //Current player has won
      } else {
        game(other, current, startingPlayer, field, turn+1)
      }
    }
  }
  
  
  
  /** This method starts a batch run with the given players and a new field.
   *  The amount of episodes to run is given in the config parameter (config.totalEpisodes). 
   *  The field is cleared after each episode and the players switch colors (and who begins).
   *  This way every player has started the game equally often.
   *  As result this method returns a GameStatistics object with accumulated game information which then can be
   *  analyzed and displayed to determine the success of the individual players.
   *  
   *  @param player1 the first player to use
   *  @param player2 the second player to use
   *  @param field the gamefield to use for the entire batch run
   *  @param config various configuration parameters to configure the batch duration and some agent parameters
   *  @param progress a ProgressInfo object (must not be null!) to display the current simulation progress
   *  @param abort an object which implements the Abortable trait and can be used to abort the batch run cleanly.
   *               If the batch run is aborted then the GameStatistics will contain data about all runs until the abort.   
   *  
   *  @return a GameStatistics object which summarizes all games in the batch run
   */
  def batchRun(player1:Player, player2:Player, field:Field, config:ConfigurationData, totalEpisodes:Long, progress:ProgressInfo, abort:Abortable = DontAbort):GameStatistics = {
    //Prepare batch run
    var episode = 1 //Episode count starts with 1
    val players = new PlayerPair(player2, player1)   //In reverse order, because of first switch
    val stats = new GameStatistics(player1, player2) //Object which holds all the information about the batch run
    log.info(s"Starting a batch run with ${totalEpisodes.short} episodes")
    
    val cfg = List(LearningParameters(config.alpha, config.gamma, config.epsilon, config.lambda, config.dynamicAlpha, config.e_2), config.rewards)
    player1.newGame(cfg)
    player2.newGame(cfg)
    
    while(episode <= totalEpisodes && !abort.isAborted) {
      val (p1,p2) = players.switch
      stats.enter(game(p1,p2,p1,field)) //Play game and enter result into statistics
      
      field.clear //Clear field for next run
      progress.next //Update progress counter
      episode += 1
    }
    
    if (episode < totalEpisodes)
      log.info(s"batch run got aborted in episode: $episode")
    
    stats.p1States = player1.loader.stateCount
    stats.p2States = player2.loader.stateCount
      
    stats
  }
}